<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class facebook_follow extends Model
{
    //
    public $timestamps = false;
    public function log(){
    	return $this->morphMany('App\log', 'logable');
    }
}
