<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class facebook_token extends Model
{
    public $timestamps = false;
    public function User()
    {
        return $this->hasOne('App\User', 'user_id', 'id');
    }
    public function log(){
    	return $this->morphMany('App\log', 'logable');
    }
}
