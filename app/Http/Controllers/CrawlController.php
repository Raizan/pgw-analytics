<?php
/*
PT. TRIKARYA TECHNOLOGIES
* FRANSISKUS GUSTI NGURAH DWIKA SETIAWAN * 
* I PUTU PRADNYANA *
* MUHAMMAD GHOZIE MANGGALA *
* REYHAN ARIEF *

Script for Crawl Streaming
function : 
__construct() : set initial value of global variable $data. Global variable can be called with $this method ($this->data)
index() : show interface of crawl stream
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use Auth;
use App\crawl_queue;
use App\object;
use App\log;
class CrawlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $data;
    public function __construct(){
        if (empty(Session::get('selectedTopic')))
            $this->data['topic'] = Session::get('selectedTopic');
        else 
            $this->data['topic'] = "noTopic";
        $this->data['latest_topic'] = crawl_queue::all();
        $this->data['latest_topic'] = $this->data['latest_topic']->sortBy('id')->first();
        $this->data['crawl_object'] = object::count();
    }
    public function index()
    {
        $data = $this->data;

        // Page title and breadcrumb
        $data['page_title'] = "Crawl Streaming";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Crawl";
        $data['breadcrumb'][1]['link'] = url('/dashboard/crawl');

        $log = new log;
        $log->action = 'view';
        $log->table = 'crawl_stream';
        $log->reference = '-';
        $log->user_id = Auth::user()->id;
        $log->save();
        return view('page.crawl.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
