<?php

/*
PT. TRIKARYA TECHNOLOGIES
* FRANSISKUS GUSTI NGURAH DWIKA SETIAWAN * 
* I PUTU PRADNYANA *
* MUHAMMAD GHOZIE MANGGALA *
* REYHAN ARIEF *

Script for view the analytic
function : 
__construct() : set initial value of global variable $data. Global variable can be called with $this method ($this->data)
index() : show interface of dashboard
selectTopic($topic) : select topic
influencer($topic = 'noTopic') :  show interface for view the influencer of a topic
topperson($topic = 'noTopic') : show interface for view the topperson of a topic
demography($topic = 'noTopic' : show interface for view the demography of a topic
sentiment($topic = 'noTopic') : show interface for view the sentiment of a topic
mapping($topic = 'noTopic') : show interface for view the mapping of a topic
compare() : show interface for compare the two topic
peak() : show peak time of every topic
*/

namespace App\Http\Controllers;
use Session;
use Illuminate\Http\Request;
use App\crawl_queue;
use App\object;
use App\log;
use App\Http\Requests;
use App\User as user;
use Auth;
class DashboardController extends Controller
{
    //
    protected $data;
    public function __construct(){
    	if (empty(Session::get('selectedTopic')))
            $this->data['topic'] = "noTopic";
        else 
            $this->data['topic'] = Session::get('selectedTopic');
        $this->data['latest_topic'] = crawl_queue::orderBy('id', 'dsec')->first();
        $this->data['crawl_object'] = object::count();
    }
    public function index(){
    	$data = $this->data;
        $data['content'] = crawl_queue::with('user', 'crawl_features')->orderBy('id', 'desc')->get();
        $data['objects'] = object::all();

        // Page title and breadcrumb
        $data['page_title'] = "Dashboard";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');

    	return view('page.dashboard.index', compact('data'));
    }
    public function streamportalfront(){
        $data = $this->data;
        $data['content'] = crawl_queue::with('user', 'crawl_features')->orderBy('id', 'desc')->get();
        $data['objects'] = object::all();

        // Page title and breadcrumb
        $data['page_title'] = "Dashboard";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/v2/index');

        return view('v2.page.dashboard.stream.modal', compact('data'));
    }

    public function main()
    {
	//dd(Session::get('toastr'));
        $data['latest_topic'] = crawl_queue::orderBy('id', 'dsec')->first();
        $data['crawl_object'] = object::count();
        
        $data['topics'] = crawl_queue::with('user', 'crawl_features')->orderBy('id', 'desc')->get();
	//dd($data['topics']);
        $data['objects'] = object::orderBy('id', 'desc')->get();
        $selectedTab = 'dashboard';
        return view('v2.page.dashboard.index', compact('data', 'selectedTab'));
    }

    public function expose(){
        $data['topics'] = crawl_queue::with('crawl_features')->orderBy('id', 'asc')->get();
        $data['objects'] = object::orderBy('id', 'desc')->get();
        return view('v2.page.dashboard.expose', ['selectedTab' => 'expose', 'data' => $data]);
    }

    public function influencer($topic = 'noTopic'){
    	$data = $this->data;
        
        // Page title and breadcrumb
        $data['page_title'] = "Influencer";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Influencer";
        $data['breadcrumb'][1]['link'] = url('/dashboard/influencer');

    	if (empty(Session::get('selectedTopic'))){
            $toastr = ['type' => 'error', 'message' => 'No Topic Selected'];
    		Session::set('toastr', $toastr);
    		return redirect('/');
    	}
        if (!empty($topic)){
            $data['topic'] = $topic;
            $log = new log;
            $log->action = 'view';
            $log->table = 'influencer';
            $log->reference = $topic;
            $log->user_id = Auth::user()->id;
            $log->save();
            return view('page.dashboard.influencer', compact('data'));
        }
    }
    public function topperson($topic = 'noTopic'){
    	$data = $this->data;

        // Page title and breadcrumb
        $data['page_title'] = "Top Person";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Top Person";
        $data['breadcrumb'][1]['link'] = url('/dashboard/top-person');

    	if (empty(Session::get('selectedTopic'))){
    		$toastr = ['type' => 'error', 'message' => 'No Topic Selected'];
            Session::set('toastr', $toastr);
    		return redirect('/')->with($data['page_title'], $data['breadcrumb']);
    	}
    	if (!empty($topic)){
    		$data['topic'] = $topic;
            $data['objects'] = object::all();
            $data['topic'] = $topic;
            $log = new log;
            $log->action = 'view';
            $log->table = 'top-person';
            $log->reference = $topic;
            $log->user_id = Auth::user()->id;
            $log->save();
    		return view('page.dashboard.topperson', compact('data'));
    	}
    }

    public function selectTopic($topic){
    	if (!empty($topic)){
    		Session::set('selectedTopic', $topic);
            Session::flash('toastr', ['type' => 'success', 'message' => 'Topic Selected: '.$topic]);
        }
    	return redirect('dashboard/influencer/'.$topic);
    }

    public function demography($topic = 'noTopic')
    {
        $data = $this->data;

        // Page title and breadcrumb
        $data['page_title'] = "Demography";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Demography";
        $data['breadcrumb'][1]['link'] = url('/dashboard/demography');

        if (empty(Session::get('selectedTopic'))){
            $toastr = ['type' => 'error', 'message' => 'No Topic Selected'];
            Session::set('toastr', $toastr);
            return redirect('/')->with($data['page_title'], $data['breadcrumb']);
        }
    	
        $data['topic'] = $topic;
        $log = new log;
        $log->action = 'view';
        $log->table = 'demography';
        $log->reference = $topic;
        $log->user_id = Auth::user()->id;
        $log->save();
    	return view('page.dashboard.demography', compact('data'));
    }

    public function sentiment($topic = 'noTopic'){
        $data = $this->data;

        // Page title and breadcrumb
        $data['page_title'] = "Sentiment";
        $data['breadcrumb'][0]['title'] = "Sentiment";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Sentiment";
        $data['breadcrumb'][1]['link'] = url('/dashboard/sentiment');

        if (empty(Session::get('selectedTopic'))){
            $toastr = ['type' => 'error', 'message' => 'No Topic Selected'];
            Session::set('toastr', $toastr);
            return redirect('/')->with($data['page_title'], $data['breadcrumb']);
        }
        $data['topic'] = $topic;
        $log = new log;
        $log->action = 'view';
        $log->table = 'sentiment';
        $log->reference = $topic;
        $log->user_id = Auth::user()->id;
        $log->save();
        return view('page.dashboard.sentiment', compact('data'));
    }

    public function mapping($topic = 'noTopic'){
        if (empty(Session::get('selectedTopic'))){
            $toastr = ['type' => 'error', 'message' => 'No Topic Selected'];
            Session::set('toastr', $toastr);
            return redirect('/')->with($data['page_title'], $data['breadcrumb']);
        }
        $data = $this->data;
        // Page title and breadcrumb
        $data['page_title'] = "Mapping";
        $data['breadcrumb'][0]['title'] = "Mapping";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Mapping";
        $data['breadcrumb'][1]['link'] = url('/dashboard/mapping');
        return view('page.dashboard.mapping', compact('data'));
    }

    public function compare(){
        $data = $this->data;
        // Page title and breadcrumb
        $data['page_title'] = "Compare";
        $data['breadcrumb'][0]['title'] = "Compare";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Compare";
        $data['breadcrumb'][1]['link'] = url('/dashboard/compare');
        $data['content'] = crawl_queue::orderBy('id', 'desc')->get();
        return view('page.dashboard.compare', compact('data'));
    }

    public function peak(){
        $data = $this->data;
        // Page title and breadcrumb
        $data['page_title'] = "Compare";
        $data['breadcrumb'][0]['title'] = "Compare";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Compare";
        $data['breadcrumb'][1]['link'] = url('/dashboard/compare');
        $data['content'] = crawl_queue::orderBy('id', 'desc')->get();
        return view('page.dashboard.peak', compact('data'));
    }

    
}
