<?php

/*
PT. TRIKARYA TECHNOLOGIES
* FRANSISKUS GUSTI NGURAH DWIKA SETIAWAN * 
* I PUTU PRADNYANA *
* MUHAMMAD GHOZIE MANGGALA *
* REYHAN ARIEF *

Script for User Management (Create User, Delete User), Current User Profile, and User Log Activity
function : 
__construct() : set initial value of global variable $data. Global variable can be called with $this method ($this->data)
index() : show interface to view all the users and its privileges
store() : store new user (from interface) to database, also write the activity in Log
destroy($id) : delete user with $id, also write the activity in Log
profile() : show interface to view current user profile
log() : show interface about all of user activity that wroten in Log.
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use Session;
use App\privilege;
use App\crawl_queue;
use App\object;
use App\User as user;
use App\facebook_token;
use App\log;
use App\twitter_token;
use View;
class UserController extends Controller
{

    protected $data; // variable that will be shown in front end

    public function __construct(){
        if (empty(Session::get('selectedTopic')))
            $this->data['topic'] = Session::get('selectedTopic');
        else 
            $this->data['topic'] = "noTopic";
        $this->data['latest_topic'] = crawl_queue::all();
        $this->data['latest_topic'] = $this->data['latest_topic']->sortBy('id')->first();
        $this->data['crawl_object'] = object::count();
    }
    public function index()
    {
        $data = $this->data; 

        // Page title and breadcrumb
        $data['page_title'] = "User List";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "User List";
        $data['breadcrumb'][1]['link'] = url('/dashboard/user');

        $data['privileges'] = privilege::all(); // 
        $data['content'] = user::with('privilege')->get();
        return view('page.user.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['privileges'] = privilege::all(); // 
        $data['users'] = user::with('privilege')->get();
        $view = (string)View::make('v2.page.configuration.user.create')->with($data);
        // return view('v2.page.configuration.user.create')->with($data);
        return response(['label' => 'Add new User Object', 'body' => $view, 'button'=>'disabled', 'continue' => ['href' => route('v2.configuration.store'), 'text' => 'Add']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'privilege' => 'required'
        ]);

        $user = new user;
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->privilege_id = $request->get('privilege');
        $user->save();

        $log = new log;
        $log->action = 'store';
        $log->table = 'user';
        $log->reference = $user->name;
        $log->user_id = Auth::user()->id;
        $log->save();
        $toastr = ['type' => 'success', 'message' => 'New User Added'];
        Session::set('toastr', $toastr);
        return redirect('v2/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = user::find($id);
        user::find($id)->delete();
        $log = new log;
        $log->action = 'destroy';
        $log->table = 'user';
        $log->reference = $user->name;
        $log->user_id = Auth::user()->id;
        $log->save();
        $toastr = ['type' => 'success', 'message' => 'User has been deleted'];
        Session::set('toastr', $toastr);
        return redirect('/v2/configuration');
    }

    public function profile()
    {
        $data = $this->data;
        
        // Page title and breadcrumb
        $data['page_title'] = "User Profile";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "User Profile";
        $data['breadcrumb'][1]['link'] = url('/dashboard/profile');

        // dd($data['content']);

        // Check if facebook token is expired
        // Get user id
        $user_id = Auth::user()->id;
        $data['content'] = user::find($user_id);
        $data['twitter'] = twitter_token::all()->first();
        
        // Find facebook token corresponding to user_id
        $facebook_token = facebook_token::where('user_id', $user_id)->first();
        // No entry
        if ($facebook_token == null) {
            $data['fb_connected'] = false;
        }
        else {
            $data['fb_connected'] = true;
            $current_time = time();
            $current_date = date("Y-m-d H:i:s", $current_time);
            $expire_date = $facebook_token->expire;

            // If expired
            if ($current_date > $expire_date) {
                $data['fb_expired'] = true;
            }
            else {
                $data['fb_expired'] = false;
            }
        }
        
        return view('page.user.profile', compact('data'));
    }
    public function profileV2()
    {
        $data = $this->data;
        
        // Page title and breadcrumb
        $data['page_title'] = "User Profile";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "User Profile";
        $data['breadcrumb'][1]['link'] = url('/dashboard/profile');

        // dd($data['content']);

        // Check if facebook token is expired
        // Get user id
        $user_id = Auth::user()->id;
        $data['content'] = user::find($user_id);
        $data['twitter'] = twitter_token::all()->first();
        
        // Find facebook token corresponding to user_id
        $facebook_token = facebook_token::where('user_id', $user_id)->first();
        // No entry
        if ($facebook_token == null) {
            $data['fb_connected'] = false;
        }
        else {
            $data['fb_connected'] = true;
            $current_time = time();
            $current_date = date("Y-m-d H:i:s", $current_time);
            $expire_date = $facebook_token->expire;

            // If expired
            if ($current_date > $expire_date) {
                $data['fb_expired'] = true;
            }
            else {
                $data['fb_expired'] = false;
            }
        }
        $view = (string) View::make('v2.header.profile')->with(['data' => $data]);
        return response(['label' => 'My Profile', 'body' => $view, 'button'=>'disabled', 'continue' => ['href' => route('v2.configuration.store'), 'text' => 'Add']]);
        // return view('v2.header.profile', compact('data'));
    }

    public function logusermodal(){
        // echo "hooo";
        $data = $this->data;

        // Page title and breadcrumb
        $data['page_title'] = "Log";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Log";
        $data['breadcrumb'][1]['link'] = url('/dashboard/log');

        $data['content'] = log::with('user')->orderBy('id', 'desc')->limit(60)->get();
        // dd($data['content']);
        $view = (string) View::make('v2.header.loguser')->with(['data' => $data]);
        return response(['label' => 'Log Users KSM Analytic', 'body' => $view, 'button'=>'disabled']);
        // dd($log);
    }

    public function log(){
        // echo "hooo";
        $data = $this->data;

        // Page title and breadcrumb
        $data['page_title'] = "Log";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Log";
        $data['breadcrumb'][1]['link'] = url('/dashboard/log');

        $data['content'] = log::with('user')->orderBy('id', 'desc')->limit(100)->get();
        // dd($data['content']);
        return view('page.user.log', compact('data'));
        // dd($log);
    }
}
