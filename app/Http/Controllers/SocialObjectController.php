<?php

/*
PT. TRIKARYA TECHNOLOGIES
* FRANSISKUS GUSTI NGURAH DWIKA SETIAWAN * 
* I PUTU PRADNYANA *
* MUHAMMAD GHOZIE MANGGALA *
* REYHAN ARIEF *

Script for Social Object Management (Create Social Object, Delete Social Object)
function : 
__construct() : set initial value of global variable $data. Global variable can be called with $this method ($this->data)
index() : show interface to view all the social object
store() : store new Social Object (from interface) to database, also write the activity in Log
destroy($id) : delete Social Object with $id, also write the activity in Log
*/

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\facebook_follow;
use App\crawl_queue;
use App\object;
use App\log;
use View;
class SocialObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $data;
    public function __construct(){
        if (empty(Session::get('selectedTopic')))
            $this->data['topic'] = Session::get('selectedTopic');
        else 
            $this->data['topic'] = "noTopic";
        $this->data['latest_topic'] = crawl_queue::all();
        $this->data['latest_topic'] = $this->data['latest_topic']->sortBy('id')->first();
        $this->data['crawl_object'] = object::count();
    }

    public function index()
    {
        //
        $data = $this->data;

        // Page title and breadcrumb
        $data['page_title'] = "Social Media Source";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Social";
        $data['breadcrumb'][1]['link'] = url('/dashboard/object/social');
        

        $data['content'] = facebook_follow::all();
        return view('page.socialobject.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = (string)View::make('v2.page.configuration.social.create');
        return response(['label' => 'Add new Social/Facebook Object', 'body' => $view, 'button'=>'disabled', 'continue' => ['href' => route('v2.configuration.store'), 'text' => 'Add']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'id' => 'required|string',
            'name' => 'required|string'
        ]);
        
        $facebook_follow = new facebook_follow;
        $facebook_follow->id = $request->id;
        $facebook_follow->name = $request->name;
        
        try {
            $facebook_follow->save();
            $log = new log;
            $log->action = 'store';
            $log->table = 'social object';
            $log->reference = $request->name;
            $log->user_id = Auth::user()->id;
            $log->save();
            $toastr = ['type' => 'success', 'message' => 'New Social Object Added'];
            Session::set('toastr', $toastr);
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('toastr', ['type' => 'error', 'message' => 'Facebook ID already exist']);
            return redirect('/dashboard/object/social');
        }
        return redirect('v2/configuration');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $facebook_follow = facebook_follow::find($id);
        facebook_follow::find($id)->delete();
        $log = new log;
        $log->action = 'destroy';
        $log->table = 'facebook_follow';
        $log->reference = $facebook_follow->id;
        $log->user_id = Auth::user()->id;
        $log->save();
        $toastr = ['type' => 'success', 'message' => 'Social Object \''. $facebook_follow->name . '\' Has Been Deleted'];
        Session::set('toastr', $toastr);
        return redirect('v2/configuration');
    }
}
