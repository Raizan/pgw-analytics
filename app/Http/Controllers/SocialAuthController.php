<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Http\Requests;
use Session;
use App\facebook_token;
use App\twitter_token;
use App\User;
use App\log;
class SocialAuthController extends Controller
{
    // Facebook
    public function fbConnect() {
       return Socialite::driver('facebook')->redirect(); 
    }

    public function fbCallback() {
        $providerUser = \Socialite::driver('facebook')->user();
        
        // Add expiresIn to current UNIX timestamp
        $expire_timestamp = time() + $providerUser->expiresIn;
        $expire_date = date("Y-m-d H:i:s", $expire_timestamp);
        
        // Bind token and expiresIn to user
        $session_array_key = array_keys(Session::all());
        $user_id_key = end($session_array_key);
        $user_id = Session::all()[$user_id_key];
        
        $user_facebook = facebook_token::where('user_id', $user_id)->first();
        $facebook_token = new facebook_token;

        // If there is no facebook token yet
        if ($user_facebook == null) {
            $facebook_token->user_id = $user_id;
            $facebook_token->token = $providerUser->token;
            $facebook_token->expire = $expire_date;
            $facebook_token->save();

            $user = User::find($user_id);
            $user->facebook = $facebook_token->id;
            $user->photo = $providerUser->avatar_original;
            $user->save();
        }
        else {
            // That facebook token is expired
            if ($this->fbIsExpired($providerUser->expiresIn)) {
                $user_facebook->token = $providerUser->token;
                $user_facebook->expire = $expire_date;
                $user_facebook->save();

                // Update photo
                $user = User::find($user_id);
                $user->photo = $providerUser->avatar_original;
                $user->save();

                $log = new log;
                $log->action = 'connect';
                $log->table = 'facebook';
                $log->reference = '';
                $log->user_id = Auth::user()->id;
                $log->save();
            }
        }
        return redirect()->to('/dashboard/profile');
    }

    public function fbIsExpired($expiresIn) {
        // Get current expire and compare to CURRENT_TIMESTAMP
        $current_time = time();
        $expire_timestamp = $current_time + $expiresIn;
        $expire_date = date("Y-m-d H:i:s", $expire_timestamp);
        
        $current_date = date("Y-m-d H:i:s", $current_time);
        
        // If expired
        if ($current_date > $expire_date) {
            return true;
        }
        else {
            return false;
        }
    }
    // End of Facebook

    public function updateTwitter(Request $request) {
        $this->validate($request, [
            'access_token' => 'required',
            'access_token_secret' => 'required',
            'consumer_key' => 'required',
            'consumer_secret' => 'required'
        ]);

        $find_token = twitter_token::where('access_token', $request->access_token)->first();
        if ($find_token == null) {
            $twitter_token = new twitter_token;
            $twitter_token->access_token = $request->access_token; 
            $twitter_token->access_token_secret = $request->access_token_secret;
            $twitter_token->consumer_key = $request->consumer_key;
            $twitter_token->consumer_secret = $request->consumer_secret;
            $twitter_token->save();
        }
        else {
            $find_token->access_token = $request->access_token; 
            $find_token->access_token_secret = $request->access_token_secret;
            $find_token->consumer_key = $request->consumer_key;
            $find_token->consumer_secret = $request->consumer_secret;
            $find_token->save();
        }
        return redirect()->to('/dashboard/profile'); 
    }
}
