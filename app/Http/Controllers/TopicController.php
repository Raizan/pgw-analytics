<?php
/*
PT. TRIKARYA TECHNOLOGIES
* FRANSISKUS GUSTI NGURAH DWIKA SETIAWAN * 
* I PUTU PRADNYANA *
* MUHAMMAD GHOZIE MANGGALA *
* REYHAN ARIEF *

Script for Topic -Crawl_Queue- Management (Create Topic, Delete Topic)
function : 
__construct() : set initial value of global variable $data. Global variable can be called with $this method ($this->data)
store() : store new topic -Crawl_Queue- (from interface) to database, also write the activity in Log
destroy($id) : delete topic -Crawl_Queue- with $id, also write the activity in Log
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\crawl_feature;
use App\crawl_queue;
use Auth;
use Session;
use App\log;
use App\object;
use View;
class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['objects'] = object::all();
        $view = (string)View::make('v2.page.topic.create')->with($data);
        // return view('v2.page.topic.create', $data);
        return response(['label' => 'Add new Topic', 'body' => $view, 'button'=>'disabled', 'continue' => ['href' => route('v2.configuration.store'), 'text' => 'Add']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'keyword' => 'required|string',
            'filter' => 'required'
        ]);
        $user_id = Auth::user()->id;
        $crawl_queue = new crawl_queue;
        $created_queue = crawl_queue::where('keyword', $request->keyword)->count();
        if ($created_queue > 0){
            Session::set('toastr', ['type' => 'error', 'message' => 'Duplicate keyword, keyword has been stored before']);
            return redirect('dashboard/index');
        }

        $crawl_queue->user_id = $user_id;
        $crawl_queue->keyword = $request->get('keyword');
        $crawl_queue->save();
        $log = new log;
        $log->action = 'store';
        $log->table = 'crawl_queue';
        $log->reference = $request->keyword;
        $log->user_id = Auth::user()->id;
        $log->save();

        foreach ($request->get('filter') as $key => $value) {
            $crawl_feature = new crawl_feature;
            $crawl_feature->queue_id = $crawl_queue->id;
            $crawl_feature->object_id = $key;
            $crawl_feature->save();
            $log = new log;
            $log->action = 'store';
            $log->table = 'feature';
            $log->reference = 'object_id: '.$key.'-queue_id:'.$crawl_queue->id;
            $log->user_id = Auth::user()->id;
            $log->save();
        }
        $toastr = ['type' => 'success', 'message' => 'Topic has been inserted'];
        Session::set('toastr', $toastr);
        return redirect('v2/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $keyword = crawl_queue::find($id);
        if(crawl_queue::find($id)->delete()){
            $log = new log;
            $log->action = 'destroy';
            $log->table = 'topic';
            $log->reference = $keyword->keyword;
            $log->user_id = Auth::user()->id;
            $log->save();
            $toastr = ['type' => 'success', 'message' => 'Topic \''.$keyword->keyword.'\' has been deleted'];
            Session::set('toastr', $toastr);
        }
        return redirect('v2/index');
    }
}
