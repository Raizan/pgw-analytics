<?php
/*
PT. TRIKARYA TECHNOLOGIES
* FRANSISKUS GUSTI NGURAH DWIKA SETIAWAN * 
* I PUTU PRADNYANA *
* MUHAMMAD GHOZIE MANGGALA *
* REYHAN ARIEF *

Script for Crawl Object Management (Create Crawl Object, Delete Crawl Object)
function : 
__construct() : set initial value of global variable $data. Global variable can be called with $this method ($this->data)
index() : show interface to view all the crawl objects
store() : store new crawl object (from interface) to database, also write the activity in Log
destroy($id) : delete crawl objects with $id, also write the activity in Log

*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Http\Requests;
use App\crawl_queue;
use App\object;
use App\log;
use App\facebook_follow;
use App\User;
use Auth;
use View;
use App\Custom\File;
use URL;
class ObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $data;
    public function __construct(){
        if (empty(Session::get('selectedTopic')))
            $this->data['topic'] = Session::get('selectedTopic');
        else 
            $this->data['topic'] = "noTopic";
        $this->data['latest_topic'] = crawl_queue::all();
        $this->data['latest_topic'] = $this->data['latest_topic']->sortBy('id')->first();
        $this->data['crawl_object'] = object::count();
    }

    public function index()
    {
        //
        $data = $this->data;

        // Page title and breadcrumb
        $data['page_title'] = "Source Object";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Object";
        $data['breadcrumb'][1]['link'] = url('/dashboard/object/all');
        
        $data['content'] = object::all();

        $data['objects'] = object::all();
        $data['social_objects'] = facebook_follow::all();
        $data['users'] = user::with('privilege')->get();
        $selectedTab = 'configuration';
        // return view('page.object.index', compact('data'));
        return view('v2.page.configuration.index', compact('data', 'selectedTab'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('page.topic.create');
        $city = ['jakarta' => 'Jakarta', 'surabaya' => 'Surabaya', 'semarang' => 'Semarang', 'yogyakarta' => 'Yogyakarta'];
        $view = (string)View::make('v2.page.configuration.object.create')->with(['city' => $city]);
        return response(['label' => 'Add new Crawl Object', 'body' => $view, 'button'=>'disabled', 'continue' => ['href' => route('v2.configuration.store'), 'text' => 'Add']]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required|string',
            'url' => 'required|string'
        ]);
        $photo = '';
        if ($request->hasFile('photo')){
            if ($request->file('photo')->isValid()){
                $url = time().'_'.File::validateName($request->file('photo')->getClientOriginalName());
                $path = base_path().'/public/userFile';
                $request->file('photo')->move($path, $url);
                $photo = $url;

                $log = new log;
                $log->action = 'upload';
                $log->table = 'file';
                $log->reference = $url;
                $log->user_id = Auth::user()->id;
                $log->save();
            }
        }

        $created_object = object::where('url', $request->get('url'))->count();
        if ($created_object > 0){
            Session::set('toastr', ['type' => 'error', 'message' => 'URL already exist']);
            return redirect('dashboard/object/all');
        }
        $object = new object;
        $object->name = $request->get('name');
        $object->url = $request->get('url');
        $object->location = $request->get('location');
        $object->logo = $photo;
        $object->save();

        $log = new log;
        $log->action = 'store';
        $log->table = 'object';
        $log->reference = $object->name;
        $log->user_id = Auth::user()->id;
        $log->save();
        
        $toastr = ['type' => 'success', 'message' => 'New Source Object has been addes'];
        Session::set('toastr', $toastr);
        return redirect('v2/configuration');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $object = object::find($id);
        object::find($id)->delete();
        $log = new log;
        $log->action = 'destroy';
        $log->table = 'object';
        $log->reference = $object->id;
        $log->user_id = Auth::user()->id;
        $log->save();
        $toastr = ['type' => 'success', 'message' => 'Object \''. $object->name . '\' has been deleted'];
        Session::set('toastr', $toastr);
        return redirect('v2/configuration');
    }
}
