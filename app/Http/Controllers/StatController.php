<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Custom\Requester;

class StatController extends Controller
{
    public function cpuLoad() {
        $stat1 = file('/proc/stat'); 
        sleep(1); 
        $stat2 = file('/proc/stat'); 
        $info1 = explode(" ", preg_replace("!cpu +!", "", $stat1[0])); 
        $info2 = explode(" ", preg_replace("!cpu +!", "", $stat2[0])); 
        $dif = array(); 
        $dif['user'] = $info2[0] - $info1[0]; 
        $dif['nice'] = $info2[1] - $info1[1]; 
        $dif['sys'] = $info2[2] - $info1[2]; 
        $dif['idle'] = $info2[3] - $info1[3]; 
        $total = array_sum($dif); 
        $cpu = array(); 
        foreach($dif as $x=>$y) 
            $cpu[$x] = round($y / $total * 100, 1);
        return $cpu['user'];
    }

    public function formatBytes($bytes, $precision = 2) { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 
        
        // $bytes /= pow(1024, $pow);
        $bytes /= (1 << (10 * $pow)); 

        return round($bytes, $precision) . ' ' . $units[$pow]; 
    }    

    public function storage() {
        $free = disk_free_space("/");
        $total = disk_total_space("/");

        $split_free = explode(" ", $this->formatBytes($free));
        $split_total = explode(" ", $this->formatBytes($total));

        $used_disk = $split_total[0] - $split_free[0];
        $unit = $split_free[1];
        return $used_disk." ".$unit;
    }

    public function test() {
        return view('page.test');
    }
    public function total_crawled(){
        $requester = new Requester;
        $url = "/total_crawled";
        
        $response = $requester->request(null, $url, 'get');
        return $response;
    }
}
