<?php
/*

PT. TRIKARYA TECHNOLOGIES
* FRANSISKUS GUSTI NGURAH DWIKA SETIAWAN * 
* I PUTU PRADNYANA *
* MUHAMMAD GHOZIE MANGGALA *
* REYHAN ARIEF *

Script for Bridging (Connect to Core-API)
function : 

bridge(Request $request, $method, $url) : re-arrange path from requested path into API-safe path
uriQueryBuilder($requests) : re-arrange request method into string
requester($data, $urlAdder, $method = 'post') : create call request to Core-API with given URL and request
location($lat, $lng) : Get 'City Name' from given $lat and $lng. This function will call OpenStreetMap Server

*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;

class DumpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    function requester($data, $urlAdder, $method = 'post'){
        $ip = $_ENV['RESTFUL_IP'];
        $port = $_ENV['RESTFUL_PORT'];
        $url = 'http://'.$ip.':'.$port.'/api'.$urlAdder;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (strtolower($method) == 'post'){
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
            curl_setopt($curl, CURLOPT_POST, true);
            $data = json_encode($data);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
	    else if (strtolower($method) == 'get') {
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPGET, true);
        }
        // return $url;
        $json_response = curl_exec($curl);
        // echo curl_error($curl);
        return $json_response;
    }

    public function bridge(Request $request, $method, $url){
    	$path = $this->uriQueryBuilder($request->query());
    	$url = '/'.$url;
        return $this->requester(null, $url.$path, $method);
    }

    function uriQueryBuilder($requests){
    	$query = '';
    	$iter = 0;
    	foreach ($requests as $key => $value) {
    		$char = '&';
    		if ($iter == 0)
    			$char = '?';
    		$query .= $char.$key.'='.str_replace(' ', '%20', $value);
    		$iter++;
    	}
    	return $query;
    }

    public function location($lat, $lng){
        $nominatim_ip = $_ENV['NOMINATIM_IP'];
        $nominatim_port = $_ENV['NOMINATIM_PORT'];
        $url = 'http://'.$nominatim_ip.':'.$nominatim_port.'reverse?format=json&lat='.$lat.'&lon='.$lng;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        return curl_exec($curl);
    }
}
