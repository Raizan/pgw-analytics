<?php

/*
PT. TRIKARYA TECHNOLOGIES
* FRANSISKUS GUSTI NGURAH DWIKA SETIAWAN * 
* I PUTU PRADNYANA *
* MUHAMMAD GHOZIE MANGGALA *
* REYHAN ARIEF *

Script for Download
function : 
__construct() : set initial value of global variable $data. Global variable can be called with $this method ($this->data)

view($type) : show list of topic -crawl_object- that ready to download
download($menu, $id, $type) : factory method for download
GetInfluencer($id, $crawl_queue) : Get Influencer data from Requester
GetTopPerson($id, $crawl_queue) : Get Top Persons data from Requester
GetDemography($id, $crawl_queue) : Get Demography data from Requester
GetSentiment($id, $crawl_queue) : Get Sentiment data from Requester
downloadCSV($menu, $id, $crawl_queue) : Download CSV File from given data
GetPosts($type, $crawl_queue) : Get Posts from Requester
downloadPDF($menu, $id, $crawl_queue) : Download PDF File from given data
*/

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\crawl_queue;
use App\log;
use App\object;
use App\Http\Requests;
use SoapBox\Formatter\Formatter;
use App\Custom\Requester;
use PDF;
use Auth;
use URL;

class DownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $data;
    public function __construct(){
        if (empty(Session::get('selectedTopic')))
            $this->data['topic'] = "noTopic";
        else 
            $this->data['topic'] = Session::get('selectedTopic');
        $this->data['latest_topic'] = crawl_queue::orderBy('id', 'desc')->first();
        $this->data['crawl_object'] = object::count();
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view($type){
        $data = $this->data;

        // Page title and breadcrumb
        $data['page_title'] = "Download";
        $data['breadcrumb'][0]['title'] = "Dashboard";
        $data['breadcrumb'][0]['link'] = url('/dashboard');
        $data['breadcrumb'][1]['title'] = "Download ".strtoupper($type);
        $data['breadcrumb'][1]['link'] = url('v2/dashboard/download')."/".$type;

        $data['download'] = $type;
        $data['content'] = crawl_queue::with('user', 'crawl_features')->get();
        $data['container'] = [
            ['name' => 'Influencer', 'color' => 'red', 'link' => 'influencers'],
            ['name' => 'Top Person', 'color' => 'yellow', 'link' => 'top_persons'],
            ['name' => 'Demography', 'color' => 'green', 'link' => 'demography'],
            ['name' => 'Sentiment', 'color' => 'blue', 'link' =>'sentiment' ]
        ];
        // return view('page.download.view', compact('data'));
                return view('v2.page.dashboard.download.view', compact('data'));

        if (strtolower($type) == 'csv')
            return $this->viewCSV($data);
        else if (strtolower($type) == 'pdf')
            return $this->viewPDF($data);
        else if (strtolower($type) == 'png')
            return $this->viewPNG($data);
        else 
            return 'No Selected Type Available';
    }

    public function download($menu, $id, $type){
        $crawl_queue = crawl_queue::find($id);
        if (empty($crawl_queue))
            return 'No Topic Selected';
        $crawl_queue = str_replace(' ', '%20', $crawl_queue->keyword);

        $log = new log;
        $log->action = 'download';
        $log->table = $menu;
        $log->reference = $crawl_queue. ' - '. $menu;
        $log->user_id = Auth::user()->id;
        $log->save();
        switch ($type) {
            case 'csv':
                return $this->downloadCSV($menu, $id, $crawl_queue);
                break;
            case 'pdf':
                return $this->downloadPDF($menu, $id, $crawl_queue);
                # code...
                break;
            case 'png':
                return $this->downloadPNG($menu, $id, $crawl_queue);
                # code...
                break;
            
            default:
                return 'No Type Selected';
                break;
        }
    }
    function GetInfluencer($id, $crawl_queue){
    	$requester = new Requester;
    	$url = "/influencers?topic=$crawl_queue";
    	$response = $requester->request(null, $url, 'get');
        $response = json_decode($response);
    	$data = [];
    	foreach ($response as $r) {
    		array_push($data, ['Display_Name' => $r->display_name, 'Screen_Name' => $r->screen_name, 'Post_Count' => $r->count]);
    	}

    	return $data;
    }
    function GetTopPerson($id, $crawl_queue){
    	$now = date('Y-m-d');
		$startDate = strtotime('-30 day', strtotime($now));
		$startDate = date('Y-m-d', $startDate);
		$endDate = strtotime('-1 day', strtotime($now));
		$endDate = date('Y-m-d', $endDate);

    	$url = "/top_persons?topic=$crawl_queue&date_start=$startDate&date_stop=$endDate";
    	
    	$requester = new Requester;
    	$response = $requester->request(null, $url, 'get');
        $response = json_decode($response);
    	$data = [];
    	foreach ($response as $r) {
    		if (count($r->persons) > 0){
    			foreach ($r->persons as $p) {
    				array_push($data, ['Date' => $r->date, 'Display_Name' => $p->display_name, 'Screen_Name' => $p->screen_name, 'Post_Count' => $p->post_count]);
    			}
    		} else {
    			array_push($data, ['Date' => $r->date, 'Display_Name' => '', 'Screen_Name' => '', 'Post_Count' => 0]);
    		}
    	}

    	return $data;
    }

    function GetDemography($id, $crawl_queue){
    	$url = "/demography?topic=$crawl_queue";
    	$requester = new Requester;
    	$response = $requester->request(null, $url, 'get');
        $response = json_decode($response);
    	$data = [];
    	foreach ($response as $r) {
    		array_push($data, ['latitude' => $r->coordinates[1], 'longitude' => $r->coordinates[0]]);
    	}

    	return $data;
    }
    function GetSentiment($id, $crawl_queue){
    	$url = "/sentiment?topic=$crawl_queue";
    	$requester = new Requester;
    	$response = $requester->request(null, $url, 'get');
        $response = json_decode($response);
    	return $response;
    }

    function downloadCSV($menu, $id, $crawl_queue){
    	$selected = false;
    	$formatter = [];
        switch ($menu) {
            case 'influencers':
            	$selected = true;
                $formatter = Formatter::make($this->GetInfluencer($id, $crawl_queue), Formatter::ARR);
                break;
            case 'top_persons':
            	$selected = true;
                $formatter = Formatter::make($this->GetTopPerson($id, $crawl_queue), Formatter::ARR);
                # code...
                break;
            case 'demography':
            	$selected = true;
                $formatter = Formatter::make($this->GetDemography($id, $crawl_queue), Formatter::ARR);
                # code...
                break;
            case 'sentiment':
            	$selected = true;
                $formatter = Formatter::make($this->GetSentiment($id, $crawl_queue), Formatter::ARR);
                # code...
                break;
            default:
                # code...
                break;
        }
        if ($selected){
	        $csv   = $formatter->toCsv();
	        header('Content-Disposition: attachment; filename="download_'.$menu.'_'.$id.'.csv"');
	        header("Cache-control: private");
	        header("Content-type: application/force-download");
	        header("Content-transfer-encoding: binary\n");

	        return $csv;
        }
    }
    function GetPosts($type, $crawl_queue){
		$requester = new Requester;
		$data['tweet'] = json_decode($requester->request(null, "/posts?topic=$crawl_queue&type=tweet&n=100", 'get'));
		$data['fb'] = json_decode($requester->request(null, "/posts?topic=$crawl_queue&type=fb&n=100", 'get'));
		$data['berita'] = json_decode($requester->request(null, "/posts?topic=$crawl_queue&type=berita&n=100", 'get'));
		return $data;
    }
    function downloadPDF($menu, $id, $crawl_queue){
    	$selected = false;
    	$data = [];
        switch ($menu) {
            case 'influencers':
            	$selected = true;
                $data['header'] = $this->GetInfluencer($id, $crawl_queue);
                $data['content'] = $this->GetPosts('influencers',$crawl_queue);
                break;
            case 'top_persons':
            	$selected = true;
                $data['header'] = $this->GetTopPerson($id, $crawl_queue);
                $data['content'] = $this->GetPosts('top_persons',$crawl_queue);
                # code...
                break;
            case 'demography':
            	$selected = true;
                $data['header'] = $this->GetDemography($id, $crawl_queue);
                $data['content'] = $this->GetPosts('influencers',$crawl_queue);
                # code...
                break;
            case 'sentiment':
            	$selected = true;
                $data['header'] = $this->GetSentiment($id, $crawl_queue);
                $data['content'] = $this->GetPosts('influencers',$crawl_queue);
                # code...
                break;
            default:
                # code...
                break;
        }
        $data['topic'] = $crawl_queue;
        $pdf = PDF::loadView('page.download.download.pdf', compact('data'))->setPaper('a4', 'landscape');
        return $pdf->download("download-$menu-$crawl_queue.pdf");
    }
    function downloadPNG($menu, $id, $crawl_queue){
        
    }
}
