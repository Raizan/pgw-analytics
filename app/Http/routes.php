<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Route::get('/bridge/latest_crawl', 'DumpController@latest_crawl');

Route::get('/bridge/{method}/{url}', 'DumpController@bridge');
Route::group(['middleware' => ['auth']], function(){

	// BRIDGE
	
	
	// LOCATION
	Route::get('/location/{lat}/{lng}', 'DumpController@location');
 	// DUMP
	Route::get('/dump/all', 'DumpController@all');
	Route::get('/dump/recent_post', 'DumpController@recent_post');
	Route::get('/dump/rdm', 'DumpController@randomLat');
	Route::get('/dump/stream', 'DumpController@stream');
	Route::post('/dump/influencer', 'DumpController@influencer'); // request: [topic]
	Route::post('/dump/person-detail', 'DumpController@persondetail'); 
	Route::post('/dump/topperson/', 'DumpController@topperson'); // request: [topic, dateStart, dateStop]
	Route::post('/dump/demograph/', 'DumpController@demograph'); // request: [topic, dateStart, dateStop]
	Route::post('/dump/sentiment/', 'DumpController@sentiment'); // request: [topic, dateStart, dateStop]
	Route::get('/dashboard/index', function(){return redirect('v2/index');});

	// DASBHOARD
	/*Route::get('/dashboard/index', 'DashboardController@index');
	Route::get('/dashboard/influencer/{topic}', 'DashboardController@influencer');
	Route::get('/dashboard/influencer', 'DashboardController@influencer');
	Route::get('/dashboard/top-person/{topic}', 'DashboardController@topperson');
	Route::get('/dashboard/top-person', 'DashboardController@topperson');
	Route::get('/dashboard/sentiment/{topic}', 'DashboardController@sentiment');
	Route::get('/dashboard/sentiment', 'DashboardController@sentiment');
	Route::get('/dashboard/demography', 'DashboardController@demography');
	Route::get('/dashboard/demography/{topic}', 'DashboardController@demography');
	Route::get('/dashboard/mapping', 'DashboardController@mapping');
	Route::get('/dashboard/mapping/{topic}', 'DashboardController@mapping');
	Route::get('/dashboard/compare', 'DashboardController@compare');
	Route::get('/selectTopic/{topic}', 'DashboardController@selectTopic');
	Route::get('/dashboard/peak', 'DashboardController@peak');*/


	// TOPIC
	/*Route::resource('/dashboard/topic', 'TopicController');
	Route::get('/dashboard/topic/delete/{id}', ['as' => 'dashboard.topic.delete', 'uses' => 'TopicController@destroy']);*/

	// CRAWL
	/*Route::resource('/dashboard/crawl', 'CrawlController');*/
	

	// USER
	/*Route::resource('/dashboard/user', 'UserController');
	Route::get('/dashboard/log', 'UserController@log');

	Route::get('/dashboard/delete/{id}', ['as' => 'dashboard.user.delete', 'uses' => 'UserController@destroy']);
	Route::get('/dashboard/profile', ['as' => 'dashboard.user.profile', 'uses' => 'UserController@profile']);

	Route::post('/dashboard/profile/twitter/update', ['as' => 'twitter.update', 'uses' => 'SocialAuthController@updateTwitter']);*/

	// OBJECT
	/*Route::resource('/dashboard/object/all', 'ObjectController');
	Route::get('/dashboard/object/all/delete/{id}', ['as' => 'dashboard.object.delete', 'uses' => 'ObjectController@destroy']);*/

	// SOCIAL OBJECT
	/*Route::resource('/dashboard/object/social', 'SocialObjectController');
	Route::get('/dashboard/object/social/delete/{id}', ['as' => 'dashboard.object.social.delete', 'uses' => 'SocialObjectController@destroy']);*/
	
	// DOWNLOAD
	// Route::get('/dashboard/download/{type}', 'DownloadController@view');
	Route::get('/download/{menu}/{id}/{type}', 'DownloadController@download');
 	/*
	MISSING ROUTE REDIRECTOR
	*/

	/*Route::get('/dashboard', function(){
		return redirect('/dashboard/index');
	});
	Route::get('/dashboard/object', function(){
		return redirect('dashboard/object/all');
	});*/
	Route::get('/login_token=random', function () {
	    return view('template.auth.login');
	}); 

	/*Route::get('/dashboard/download', function(){
		return redirect('dashboard');
	});*/
	
	// STAT
	Route::get('/stat/cpu_load', 'StatController@cpuLoad');
	Route::get('/stat/storage', 'StatController@storage');
	Route::get('/stat/totalcrawled', 'StatController@total_crawled');
	Route::get('/stat/test', 'StatController@test');

	// THIRD-PARTY
	Route::get('/facebook/connect', 'SocialAuthController@fbConnect');
	Route::get('/facebook/callback', 'SocialAuthController@fbCallback');
	Route::get('/facebook/is_expired', 'SocialAuthController@fbIsExpired');

	/*Route::get('/', function(){
		return redirect('/dashboard/index');
	});*/

	// UI.VERSION.2
	Route::get('/', function(){
		return redirect('/v2/index');
	});
	Route::get('/v2/', function(){
		return redirect('/v2/index');
	});

	Route::get('v2/index', 'DashboardController@main');
 	Route::get('v2/help', function(){
		return view('v2.page.dashboard.help', ['selectedTab' => 'help']);
	});

	// Route sementara EXpose
	Route::get('v2/expose', 'DashboardController@expose');

	Route::get('v2/statistic', function(){
		return view('v2.page.dashboard.statistic', ['selectedTab' => 'statistic', 'crawl_queue' => DB::table('crawl_queues')->orderBy('id', 'desc')->get()]);
	});
	Route::get('v2/stream', function(){
		return view('v2.page.dashboard.stream', ['selectedTab' => 'stream']);
	});
	Route::get('v2/analyze', function(){
		Session::flash('notification',['error' => 'Select topic you want to analyze']);
		return redirect('/');
	});
		Route::get('v2/download/{type}', 'DownloadController@view',['selectedTab' => 'stream']);

	Route::get('v2/download/{menu}/{id}/{type}', 'DownloadController@download',['selectedTab' => 'stream']);

	Route::get('v2/analyze/{keyword}', function($keyword){
		$existedKeyword = DB::table('crawl_queues')->where('keyword', str_replace('%20', ' ', $keyword))->get();
		$objects = DB::table('objects')->get();
		if (count($existedKeyword) > 0){
			return view('v2.page.analyze', ['selectedTab' => 'none', 'topic' => $keyword, 'objects' => $objects, 'current' => $existedKeyword[0]]);
		}
		else 
			return redirect('v2/index');
	});
	Route::resource('v2/configuration', 'ObjectController');
	Route::resource('v2/object', 'ObjectController');
	Route::resource('v2/social', 'SocialObjectController');
	Route::resource('v2/user', 'UserController');
	Route::resource('v2/topic', 'TopicController');
	Route::get('/v2/profile', 'UserController@profileV2');
	Route::get('/v2/loguser', 'UserController@logusermodal');
	Route::get('/v2/streamportalfront', 'DashboardController@streamportalfront');
	Route::get('/v2/configuration/social/delete/{id}', ['as' => 'v2.social.delete', 'uses' => 'SocialObjectController@destroy']);
	Route::get('/v2/configuration/object/delete/{id}', ['as' => 'v2.object.delete', 'uses' => 'ObjectController@destroy']);
	Route::get('/v2/configuration/user/delete/{id}', ['as' => 'v2.user.delete', 'uses' => 'UserController@destroy']);
	Route::post('/v2/profile/twitter/update', ['as' => 'v2.twitter.update', 'uses' => 'SocialAuthController@updateTwitter']);
});
Route::auth();
