<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class crawl_queue extends Model
{
    //
    public function user(){
		return $this->belongsTo('\App\User','user_id','id');
    }
    public function crawl_features()
    {
        return $this->hasMany('App\crawl_feature','queue_id','id');
    }
    public function top_persons() {
        return $this->hasMany('App\top_persons', 'queue_id', 'id');
    }
    public function top_influencer() {
        return $this->hasMany('App\top_influencer', 'queue_id', 'id');
    }
    public function related_words() {
        return $this->hasMany('App\related_words', 'queue_id', 'id');
    }
    public function log(){
        return $this->morphMany('App\log', 'logable');
    }
}
