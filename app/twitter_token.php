<?php 
namespace App;

use Illuminate\Database\Eloquent\Model;

class twitter_token extends Model
{
    public $timestamps = false;
    public function log(){
    	return $this->morphMany('App\log', 'logable');
    }
}

?>