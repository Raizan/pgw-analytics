<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class object extends Model
{
    //
    public function crawl_feature()
    {
        return $this->hasMany('App\crawl_feature','object_id','id');
    }
    public function log(){
    	return $this->morphMany('App\log', 'logable');
    }
}
