<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class top_persons extends Model
{
    //
    public function crawl_queue() {
        return $this->belongsTo('\App\crawl_queue','queue_id','id');
    }
}
