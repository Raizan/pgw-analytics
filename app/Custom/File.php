<?php

namespace App\Custom;

use Illuminate\Http\Request;
use App\Http\Requests;
class File
{
	public static function validateName($string){
        return str_replace(' ', '-', $string);
    }
}