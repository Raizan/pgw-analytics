<?php

namespace App\Custom;
// use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
class Requester
{
	public function request($data, $urlAdder, $method = 'post'){
		$ip = $_ENV['RESTFUL_IP'];
	    $port = $_ENV['RESTFUL_PORT'];
        $url = 'http://'.$ip.':'.$port.'/api'.$urlAdder;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (strtolower($method) == 'post'){
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
            curl_setopt($curl, CURLOPT_POST, true);
            $data = json_encode($data);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
	    else if (strtolower($method) == 'get') {
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPGET, true);
        }
        // return $url; // -> return url nya API -> ketika di open di Mozilla, ada hasilnya
        $json_response = curl_exec($curl); // fungsi buat execute CURL dengan URL -> Gak ada hasilnya
        return $json_response;
	}
}