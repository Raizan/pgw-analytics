<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class log extends Model
{
    //
    public function logable(){
    	return $this->morphTo();
    }

    public function user(){
		return $this->belongsTo('\App\User','user_id','id');
    }
}
