<?php
namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'facebook', 'twitter', 'photo', 'privilege'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 
    ];
    public function privilege(){
        return $this->belongsTo('\App\privilege','privilege_id','id');
    }
    public function facebook_token(){
        return $this->belongsTo('\App\facebook_token','facebook','id');
    }
    public function log(){
        return $this->morphMany('App\log', 'logable');
    }
}