<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class crawl_feature extends Model
{
    //
    public function object(){
		return $this->belongsTo('\App\object','object_id','id');
    }

    public function log(){
    	return $this->morphMany('App\log', 'logable');
    }
}
