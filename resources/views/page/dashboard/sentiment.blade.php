<?php
$now = date('Y-m-d');
$startDate = strtotime('-30 day', strtotime($now));
$startDate = date('Y-m-d', $startDate);
$endDate = strtotime('-1 day', strtotime($now));
$endDate = date('Y-m-d', $endDate);
?>
@extends('template.index')
@section('content')
   <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
     
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-user"></i>Sentiment: {{$data['topic']}}</div>
               <div class="pull-right">
                  <span>
                    <i class="icon-calendar"> Date range: </i>
                    <input type="text" name="daterange" value="01/01/2016 - 01/31/2016" style="color: white; border: none; padding-left: 10px; background-color: #E02222" />
                  </span>
                  <a href="#" class="btn blue btn-sm download" target="_blank" style="margin-top: -5px"><i class="icon-download"> Download</i></a>
               </div>
            </div>
            <div class="portlet-body">
              <b><center>Showing Graph for <i>Influencer</i> of <i>{{$data['topic']}}</i></center></b>
              <b><center id="count"></center></b>
             <div id="piechart" style="width:100%; height: 500px;"></div>
            </div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box purple">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Posted</div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="portal_log" table_view="post_view">
               <thead>
                  <tr>
                      <th>No</th>
                      <th>Topic</th>
                      <th>Timestamp</th>
                      <th>URL</th>
                      <th>Source</th>
                  </tr>
              </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="row ">
      <div class="col-md-8 col-sm-12">
         <div class="portlet box" style="background-color: #3b5998; border: solid 0.1px #3b5998">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Facebook</div>
            </div>
            <div class="portlet-body" style="background-color: #E9EBEE">
               <div class="task-content">
                  <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
                     <script src="//connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v2.5" async></script>
                     <style type="text/css">
                        ul{
                           list-style-type: none;
                           padding-left: 0px;
                        }
                        #facebook_log > li{
                           border-radius: 2px !important;
                           padding: 10px;
                           color: black;
                           margin-bottom: 10px;
                           background-color: white;
                        }
                        #facebook_log > li > .facebook-person{
                           font-weight: bold;
                           font-size: 18px;
                           color: #3b5998;
                        }
                        #facebook_log > li > .facebook-person > a{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:hover{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:visited{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:active{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-content{
                           padding-bottom: 5px;
                           border-bottom: solid 0.5px #ccc;
                           color: black;
                        }
                        #facebook_log > li > .facebook-time{
                           color: #999;
                        }
                     </style>
                     <center class="get-notification" id="facebook_log-notification">
                        <h2 style="color: #555">
                           Retrieving data
                        </h2>
                        <p style="color: #999">
                           If in 30 seconds this section is still showing this message, that means no data available or core program is not functioning well. Please contact your administrator if this happens.
                        </p>
                     </center>
                     <ul class="" id="facebook_log">
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-12">
         <div class="portlet box blue tasks-widget">
            <div class="portlet-title">
               <div class="caption"><i class="icon-check"></i>Twitter Log</div>
            </div>
            <div class="portlet-body">
               <div class="task-content">
                  <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
                     <!-- START TASK LIST -->
                     <center class="get-notification" id="twitter_log-notification">
                        <h2 style="color: #999">
                           Retrieving data
                        </h2>
                        <p style="color: #ccc">
                           if in 30 seconds still showing this, it means that no data available or core program  is not functioned well. Contact your administrators
                        </p>
                     </center>
                     <ul class="task-list" id="twitter_log">
                     </ul>
                     <!-- END START TASK LIST -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section ('custom-footer')
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript" src="{{asset('assets/assets/scripts/tunnel.js')}}"></script>
   <script type="text/javascript">
   var data = [];
   var dateStart = "{{$startDate}}";
   var dateStop = "{{$endDate}}";
   $("#sample_2").DataTable();
   showGraph();
   $(function() {
      $('input[name="daterange"]').val(moment(dateStart).format('MM-DD-YYYY') + ' - ' + moment(dateStop).format('MM-DD-YYYY'));
       $('input[name="daterange"]').daterangepicker({
         'startDate' : moment(dateStart).format('MM-DD-YYYY'),
         'endDate' : moment(dateStop).format('MM-DD-YYYY'),
       }, function(start, end, label) {
         dateStart = start.format('YYYY-MM-DD');
         dateStop = end.format('YYYY-MM-DD');
         showGraph();
       });
   });
   function showGraph(){
     var url = '{{URL::to("/bridge/get/sentiment?topic=".$data["topic"])}}&date_start='+dateStart+'&date_stop='+dateStop;
     console.log(url);
     var posts = [];
      var tunnel = new Tunnel($("#facebook_log"), $("#tweeter_log"), $("#portal_log"));
     $.get(url).done(function( response ) {
        response = jQuery.parseJSON(response);
        data = response;
        var pieData = [];
        var positive = 0;
        var negative = 0;
        var neutral = 0;
        for(var i = 0; i < data.length; i++){
          positive += data[i].sentiment.positive;
          negative += data[i].sentiment.negative;
          neutral += data[i].sentiment.neutral;
        }
        pieData.push(['positive', positive]);
        pieData.push(['negative', negative]);
        pieData.push(['neutral', neutral]);
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Persons');
          data.addColumn('number', 'Tweet');
          data.addRows(pieData);
          countData();
          function countData(){
            var total = 0;
            for (var i = 0; i < pieData.length; i++){
              total += pieData[i][1];
            }
            $('#count').text('Total Data: '+total);
          }
          // Instantiate and draw our chart, passing in some options.
          var chart = new google.visualization.PieChart(document.getElementById('piechart'));

          function selectHandler() {
            var selectedItem = chart.getSelection()[0];
            if (selectedItem) {
              var sentiment = data.getValue(selectedItem.row, 0);
              getData(sentiment);
            }
          }

          google.visualization.events.addListener(chart, 'select', selectHandler);
          google.visualization.events.addListener(chart, 'ready', function(){
            $('.download').attr('href', chart.getImageURI());
          });
          chart.draw(data, {colors: ['#0c0', '#c00', '#ccc']});
          function getData(sentiment){
            var sentimentNumber
            switch(sentiment){
              case 'positive':
                sentimentNumber = 1;
                break;
              case 'negative' :
                sentimentNumber = -1;
                break;
              default:
                sentimentNumber = 0;
                break;
            }
            tunnel.beritas = new Berita($("#portal_log"));
            tunnel.facebooks = new Facebook($("#facebook_log"));
            tunnel.twitters = new Twitter($("#twitter_log"));
            var postType = ['tweet', 'fb', 'berita'];
            var totalIter = 0;
            var showIter = 0;
              for (var j = 0; j < postType.length; j++){
                getPosts('{{$data["topic"]}}', postType[j]);
                totalIter++;
              }
            function getPosts(topic, type){
              var getURL = "{{URL::to('/bridge/get/posts')}}"+'?topic='+topic.replace(" ", "%20")+'&type='+type+'&sentiment='+sentimentNumber+'&n=20&date_start='+dateStart+'&date_stop='+dateStop;
              console.log(getURL);
              $.get(getURL).done(function(response) {
                response = jQuery.parseJSON(response);
                for (var i = 0; i < response.length; i++){
                  tunnel.factory(response[i]);
                }
                show();
              });
            }
            function show(){
              showIter++;
              if (showIter >= totalIter)
                tunnel.show();
              tunnel.twitters.clearView();
            }
          }
        }
     });
   }
   </script>
   @endsection