@extends('template.index')
@section('custom-header')
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@section('content')
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
    <div class="row ">
      <div class="col-md-8 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Topic Log</div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                     <tr>
                        <th>No</th>
                        <th>Keyword</th>
                        <th>Assigned</th>
                        <th>Filter</th>
                        <!-- <th>Url Count</th> -->
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     $iter = 0;
                     ?>
                     @foreach($data['content'] as $print)
                     <tr class="odd gradeX">
                        <td>{{++$iter}}</td>
                        <td>{{$print->keyword}}</td>
                        <td>{{$print->created_at}}</td>
                        <td>
                           <ul style="padding-left: 0px; list-style: none;">
                              @foreach($print->crawl_features as $cf)
                                 <li>
                                    @if ($cf['object']['name'] != '')
                                    - {{$cf['object']['name']}}
                                    @endif
                                 </li>
                              @endforeach
                           </ul>
                        </td>
                        <!-- <td>?</td> -->
                        <td>
                           <a href="{{URL::to('selectTopic/'.$print->keyword)}}" class="btn green">Select Topic</a>
                           <a class="btn default" data-toggle="modal" href="#static" reference="{{$print->keyword}}" url="{{URL::to('/dashboard/topic/delete/'.$print->id)}}" onclick="deleteModal(this)">Delete</a>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-12">
         <div class="portlet box green">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>New Topic</div>
            </div>
            <div class="portlet-body">
               {!! Form::open(array('route' => 'dashboard.topic.store', 'method' => 'post','files' => false, 'class' => 'form-horizontal form-bordered form-row-stripped')) !!}
                  <div class="form-body">
                     <div class="form-group">
                        {!! Form::label('keyword','Topic : ', ['class' => 'control-label col-md-3']) !!}
                        @if ($errors->has('keyword'))
                           <span class="help-block">
                                <strong style="color:red">{{ $errors->first('keyword') }}</strong>
                           </span>
                        @endif
                        <div class="col-md-9">
                           {!! Form::text('keyword',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: desa, sungai, pembangunan, pembangunan desa']) !!}
                        </div>
                     </div>
                     <div class="form-group">
                        {!! Form::label('filter','Filter : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                        @if ($errors->has('filter'))
                           <span class="help-block">
                                <strong style="color:red">{{ $errors->first('filter') }}</strong>
                           </span>
                        @endif
                           @foreach($data['objects'] as $obj)
                              <div class="row">
                                    <label class="col-md-12">
                                       {!! Form::checkbox('filter['.$obj['id'].']', $obj['name']) !!}
                                       {{ $obj->name}}
                                    </label>
                              </div>
                           @endforeach
                           <span class="help-block">Select one or more source(s)</span>
                        </div>
                     </div>
                     <div class="form-actions fluid">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="col-md-12">
                                 {!! Form::submit('Tambahkan Topik',['class' => 'btn green col-md-12']) !!}
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               {!! Form::close()!!}
            </div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-8 col-sm-12">
         <div class="portlet box purple">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Portal Log</div>
            </div>
            <div class="portlet-body" style="overflow: scroll;">
               <table class="table table-striped table-bordered table-hover" id="portal_log" table_view="post_view">
                  <thead>
                     <tr>
                         <th>No</th>
                         <th>Title</th>
                         <th>Timestamp</th>
                         <th>URL</th>
                         <th>Source</th>
                     </tr>
                 </thead>
               </table>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-12">
         <div class="portlet box blue tasks-widget">
            <div class="portlet-title">
               <div class="caption"><i class="icon-check"></i>Twitter Log</div>
            </div>
            <div class="portlet-body">
               <div class="task-content">
                  <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
                     <!-- START TASK LIST -->
                     <center class="get-notification" id="twitter_log-notification">
                        <h2 style="color: #999">
                           Retrieving data
                        </h2>
                        <p style="color: #ccc">
                           If in 30 seconds this section is still showing this message, that means no data available or core program is not functioning well. Please contact your administrator if this happens. 
                        </p>
                     </center>
                     <ul class="task-list" id="twitter_log">
                     </ul>
                     <!-- END START TASK LIST -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   <script type="text/javascript">
      $("#sample_1").DataTable();
      $('input[name="daterange"]').daterangepicker();

   </script>
   <script type="text/javascript" src="{{asset('assets/assets/scripts/tunnel.js')}}"></script>
   <script type="text/javascript">
   var table_sample2 = $("#sample_2").DataTable();
   var tableView = $("table[table_view='post_view'] > tbody");
   var postType = ['tweet', 'fb', 'berita'];
   var topicLog = [];
   var dataDisplay = [];
   var dataCounter = 0;
   @foreach($data['content'] as $print)
   topicLog.push("{{str_replace(' ','%20',$print->keyword)}}");
   @endforeach
	var tunnel = new Tunnel($("#facebook_log"), $("#tweeter_log"), $("#portal_log"));
	tunnel.beritas = new Berita($("#portal_log"));
	tunnel.facebooks = new Facebook($("#facebook_log"));
	tunnel.twitters = new Twitter($("#twitter_log"));
	var totalIter = 0;
	var showIter = 0;
   for (var i = topicLog.length-1; i >= 0; i--){
   	for (var j = 0; j < postType.length; j++){
   		getDatas(topicLog[i], postType[j]);
   		totalIter++;
   	}
   }
   function getDatas(topic, type){
   	var getURL = "{{URL::to('/bridge/get/recent_post')}}"+'?topic='+topic.replace(" ", "%20")+'&type='+type;
      console.log(getURL);
   	$.get(getURL).done(function(response) {
   		response = jQuery.parseJSON(response);
   		for (var i = 0; i < response.length; i++){
            response[i].topic = topic;
            response[i].topic_number = topicLog.indexOf(topic);
   			tunnel.factory(response[i]);
   		}
   		show();
      });
  	}
  	function show(){
  		showIter++;
  		if (showIter >= totalIter)
  			tunnel.show();
  	}
   $('input[name="daterange"]').daterangepicker();
   </script>

   @endsection