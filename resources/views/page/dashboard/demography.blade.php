@extends('template.index')
@section('content')
   <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
     
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   <div class="row">
            <div class="col-md-12">
               <!-- BEGIN MARKERS PORTLET-->
               <div class="portlet solid yellow">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i>Demography</div>
                  </div>
                  <div class="portlet-body">
                     <div id="map" class="gmaps" style="height: 500px">
                       
                     </div>
                  </div>
               </div>
               <!-- END MARKERS PORTLET-->
            </div>

            <div class="col-md-12 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Location</div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                     <tr>
                        <th>No</th>
                        <th>Location</th>
                        <th>Count</th>
                     </tr>
                  </thead>
                  <tbody>
                     
                   </tbody>
               </table>
            </div>
         </div>
      </div>
         </div>

         
   @endsection

   @section ('custom-footer')
   <!-- <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/gmaps/gmaps.js')}}" type="text/javascript"></script> 
   <script src="{{asset('assets/assets/scripts/maps-google.js')}}" type="text/javascript"></script>  -->
    <script>
      var url = "{{URL::to('/bridge/get/demography?topic='.str_replace(' ', '%20', $data['topic']))}}";
      var markers = [];
      var geoLocation = [];
      var city = [];
      function initMap() {
        $.get(url).done(function(response){
          dataToDisplay = jQuery.parseJSON(response);
          geoLocation = jQuery.parseJSON(response);
          var myLatLng = {lat: -1.8785136, lng: 113.6952623};
          var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            scrollwheel: false,
            zoom: 5
          });
          for (var i = 0; i< geoLocation.length; i++){
            if (geoLocation[i].coordinates[0] != null && geoLocation[i].coordinates[1] != null){
              var pos = {lat: geoLocation[i].coordinates[1], lng: geoLocation[i].coordinates[0]};
              var marker = new google.maps.Marker({
                map: map,
                position: pos
              });
              getCity(geoLocation[i].coordinates)
            }
          }
        });
      }
      function getCity(coordinates){
        // console.log('hola');
        var lat = coordinates[1];
        var lng = coordinates[0];
        // var targetURL = 'http://nominatim.openstreetmap.org/reverse?format=json&lat='+lat+'&lon='+lng+'&zoom=18&addressdetails=1';
        var url = '{{URL::to("location/")}}/'+lat+'/'+lng;
        
        $.ajax({
          method: "GET",
          url: url,
          dataType: 'json',
          success: function(response){
            console.log('hi');

            validateCity(response);
            updateView();
          }
        });
      }
      function validateCity(response){
        var isExist = 0;
        var existedCity = -1;
        for (var i = 0; i < city.length; i++){
          if (city[i].city == response.geoplugin_place){
            isExist++;
            existedCity = i;
          }
        }
        if (isExist == 0)
          city.push({city: response.geoplugin_place, count: 1});
        else 
          city[existedCity].count++;
      }
      function updateView(){
        console.log('haha');
        $("#sample_1").children().remove();
        for (var i = 0; i < city.length; i++){
          $("#sample_1").append("<tr class='odd gradeX'><td>"+(i+1)+"</td><td>"+city[i].city+"</td><td>"+city[i].count+"</td></tr>");
        }
        // $("#sample_1").DataTable();
      }
   </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsXNCvwTsLM00_NuUvBFK7Meg6sqNwP3I&callback=initMap" async defer></script>
   @endsection