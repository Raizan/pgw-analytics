<?php
$now = date('Y-m-d');
$startDate = strtotime('-7 day', strtotime($now));
$startDate = date('Y-m-d', $startDate);
$endDate = strtotime('-1 day', strtotime($now));
$endDate = date('Y-m-d', $endDate);
?>
@section('custom-header')

@endsection
@extends('template.index')
@section('content')
 
  
    <div class="row">
      <div class="col-md-12">
          @include('template.breadcrumb')
       </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Top Person: {{$data['topic']}}</div>
               <div class="pull-right">
                  <span>
                    <i class="icon-calendar"> Date range: </i>
                    <input type="text" name="daterange" value="01/01/2016 - 01/31/2016" style="color: white; border: none; padding-left: 10px; background-color: #E02222" />
                  </span>
                  <a href="#" class="btn blue btn-sm download" target="_blank" style="margin-top: -5px"><i class="icon-download"> Download</i></a>
               </div>
            </div>
            <div class="portlet-body">
               <b><center>Showing Graph for <i>Top Persons</i> of <i>{{$data['topic']}}</i></center></b>
              <b><center id="count"></center></b>
              <div id="line_top_x"></div>
            </div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box purple">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Portal Berita</div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="portal_log" table_view="post_view">
               <thead>
                  <tr>
                      <th>No</th>
                      <th>Topic</th>
                      <th>Timestamp</th>
                      <th>URL</th>
                      <th>Source</th>
                  </tr>
              </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="row ">
      <div class="col-md-8 col-sm-12">
         <div class="portlet box" style="background-color: #3b5998; border: solid 0.1px #3b5998">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Facebook</div>
            </div>
            <div class="portlet-body" style="background-color: #E9EBEE">
               <div class="task-content">
                  <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
                     <script src="//connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v2.5" async></script>
                     <style type="text/css">
                        ul{
                           list-style-type: none;
                           padding-left: 0px;
                        }
                        #facebook_log > li{
                           border-radius: 2px !important;
                           padding: 10px;
                           color: black;
                           margin-bottom: 10px;
                           background-color: white;
                        }
                        #facebook_log > li > .facebook-person{
                           font-weight: bold;
                           font-size: 18px;
                           color: #3b5998;
                        }
                        #facebook_log > li > .facebook-person > a{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:hover{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:visited{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:active{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-content{
                           padding-bottom: 5px;
                           border-bottom: solid 0.5px #ccc;
                           color: black;
                        }
                        #facebook_log > li > .facebook-time{
                           color: #999;
                        }
                     </style>
                     <ul class="" id="facebook_log">
                        
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-12">
         <div class="portlet box blue tasks-widget">
            <div class="portlet-title">
               <div class="caption"><i class="icon-check"></i>Twitter Log</div>
            </div>
            <div class="portlet-body">
               <div class="task-content">
                  <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
                     <!-- START TASK LIST -->
                     <ul class="task-list" id="twitter_log">
                     </ul>
                     <!-- END START TASK LIST -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript" src="{{asset('assets/assets/scripts/tunnel.js')}}"></script>
   <script type="text/javascript">
   var dateStart = "{{$startDate}}";
   var dateStop = "{{$endDate}}";
   var persons = [];
   showGraph();
   $(function() {
      $('input[name="daterange"]').val(moment(dateStart).format('MM-DD-YYYY') + ' - ' + moment(dateStop).format('MM-DD-YYYY'));
       $('input[name="daterange"]').daterangepicker({
         'startDate' : moment(dateStart).format('MM-DD-YYYY'),
         'endDate' : moment(dateStop).format('MM-DD-YYYY'),
       }, function(start, end, label) {
         // console.log(start.format('YYYY/MM/DD'), end.format('YYYY/MM/DD'), label);
         dateStart = start.format('YYYY-MM-DD');
         dateStop = end.format('YYYY-MM-DD');
         showGraph();
       });
   });
   function showGraph(){
      persons = [];
      var topic = '{{$data["topic"]}}';
      var dataDisplay = [];
      var urls = '{{URL::to("/bridge/get/top_persons")}}?topic={{str_replace(" ","%20",$data["topic"])}}&n=5&date_start='+dateStart+'&date_stop='+dateStop;
      console.log(urls);
      $.get(urls).done(function(response){
         response = jQuery.parseJSON(response);
         // data = response;
         console.log(response);
         google.charts.load('current', {'packages':['line']});
         google.charts.setOnLoadCallback(drawChart);
         var selectedItem = [];
         function personValidation(name){
            var isExist = 0;
            for (var i = 0; i < persons.length; i++){;
               if (persons[i].screen_name == name.screen_name)
                  isExist++;
            }
            if (isExist == 0)
               persons.push(name);
         }
         function drawChart() {
            for (var i = 0; i< response.length; i++){
               for (var j = 0; j < response[i].persons.length; j++){
                  personValidation(response[i].persons[j]);
               }
            }
            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Persons');
            for (var i = 0; i< persons.length; i++){
               data.addColumn('number', persons[i].display_name);
            }
            var contents = [];

            for (var i = 0; i < response.length; i++){
               var content = [];
               content.push(response[i].date);
               for (var j = 0; j < persons.length; j++){
                  var isExist = -1;
                  for (var k = 0; k < response[i].persons.length; k++){
                     if (response[i].persons[k].screen_name == persons[j].screen_name)
                        isExist = k;
                  }
                  if (isExist == -1)
                     content.push(0);
                  else 
                     content.push(response[i].persons[isExist].post_count);
               }
               contents.push(content);
            }
            data.addRows(contents);
            dataDisplay.push(contents);
            dataDisplay.push(persons);
            var options = {
              height: 500,
              axes: {
                x: {
                  0: {side: 'bottom'}
                }
              }
            };

            var chart = new google.charts.Line(document.getElementById('line_top_x'));
            function selectHandler() {
                selectedItem = chart.getSelection()[0];
                if (selectedItem) {
                  var column = selectedItem.column;
                  var row = selectedItem.row;
                  if (column != null && row != null){
                     getDetailPosts(persons[column-1].screen_name, contents[row][0]);
                  }
                }
              }
            google.visualization.events.addListener(chart, 'select', selectHandler);    
            google.visualization.events.addListener(chart, 'ready', function(){
               $('.download').attr('href', chart.getImageURI());
             });
            chart.draw(data, options);
            countData();
            function countData(){
               var total = 0;
               for (var i = 0; i < contents.length; i++){
                  for (var j = 1; j < contents[i].length; j++)
                     total += contents[i][1];
               }
               $('#count').text('Total Data: '+total);
             }
            function validateName(name){
               var names = name.split(' ');
               if (names.length > 1){
                  var newName = '';
                  for (var i = 0; i < names.length; i++){
                     newName += names[i];
                     if (i + 1 < names.length)
                        newName += '%20';
                  }
               }
               else 
                  return name;
            }
            function getDetailPosts(name, date){
               name = validateName(name);
               var tunnel = new Tunnel($("#facebook_log"), $("#tweeter_log"), $("#portal_log"));
               tunnel.beritas = new Berita($("#portal_log"));
               tunnel.facebooks = new Facebook($("#facebook_log"));
               tunnel.twitters = new Twitter($("#twitter_log"));

               var postType = ['tweet', 'fb', 'berita'];
               var totalIter = 0;
               var showIter = 0;

               for (var j = 0; j < postType.length; j++){
                  getPosts('{{$data["topic"]}}', postType[j], name, date);
                  totalIter++;
               }
               function getPosts(topic, type, name,date){
                  var getURL = "{{URL::to('/bridge/get/posts')}}"+'?topic='+"{{$data['topic']}}"+'&type='+type+'&n=20&name='+name+'&date='+date;
                  console.log(getURL);
                  $.get(getURL).done(function(response) {
                     response = jQuery.parseJSON(response);
                     for (var i = 0; i < response.length; i++){
                        tunnel.factory(response[i]);
                     }
                     show();
                  });
               }
               function show(){
                  showIter++;
                  if (showIter >= totalIter)
                    tunnel.show();
               }
            }
         }
      });
   }
   
  </script>
   @endsection