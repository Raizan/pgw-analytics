@extends('template.index')
@section('content')
   <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
     
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         <h3 class="page-title">
            Dashboard <small>statistics and more</small>
         </h3>
         <ul class="page-breadcrumb breadcrumb">
            <li>
               <i class="icon-home"></i>
               <a href="index.html">Home</a> 
               <i class="icon-angle-right"></i>
            </li>
            <li><a href="#">My Topics</a></li>
            <li class="pull-right">
               <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                  <i class="icon-calendar"></i>
                  <span></span>
                  <i class="icon-angle-down"></i>
               </div>
            </li>
         </ul>
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-user"></i>Influencer: {{$data['topic']}}</div>
            </div>
            <div class="portlet-body">
             <div id="piechart" style="width:100%; height: 500px;"></div>
            </div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box purple">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Posted</div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="sample_2" table_view="post_view">
                  <thead>
                     <tr>
                         <th>No</th>
                         <th>Topic</th>
                         <th>Timestamp</th>
                         <th>URL</th>
                         <th>Source</th>
                     </tr>
                 </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="row ">
      <div class="col-md-8 col-sm-12">
         <div class="portlet box" style="background-color: #3b5998; border: solid 0.1px #3b5998">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Facebook</div>
            </div>
            <div class="portlet-body" style="background-color: #E9EBEE">
               <div class="task-content">
                  <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
                     <script src="//connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v2.5" async></script>
                     <style type="text/css">
                        ul{
                           list-style-type: none;
                           padding-left: 0px;
                        }
                        #facebook_log > li{
                           border-radius: 2px !important;
                           padding: 10px;
                           color: black;
                           margin-bottom: 10px;
                           background-color: white;
                        }
                        #facebook_log > li > .facebook-person{
                           font-weight: bold;
                           font-size: 18px;
                           color: #3b5998;
                        }
                        #facebook_log > li > .facebook-person > a{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:hover{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:visited{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:active{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-content{
                           padding-bottom: 5px;
                           border-bottom: solid 0.5px #ccc;
                           color: black;
                        }
                        #facebook_log > li > .facebook-time{
                           color: #999;
                        }
                     </style>
                     <center class="get-notification" id="notif-facebook">
                        <h2 style="color: #555">
                           Retrieving data
                        </h2>
                        <p style="color: #999">
                           if in 30 seconds still showing this, it means that no data available or core program  is not functioned well. Contact your administrators
                        </p>
                     </center>
                     <ul class="" id="facebook_log">
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-12">
         <div class="portlet box blue tasks-widget">
            <div class="portlet-title">
               <div class="caption"><i class="icon-check"></i>Twitter Log</div>
            </div>
            <div class="portlet-body">
               <div class="task-content">
                  <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
                     <!-- START TASK LIST -->
                     <center class="get-notification" id="notif-twitter">
                        <h2 style="color: #999">
                           Retrieving data
                        </h2>
                        <p style="color: #ccc">
                           if in 30 seconds still showing this, it means that no data available or core program  is not functioned well. Contact your administrators
                        </p>
                     </center>
                     <ul class="task-list" id="twitter_log">
                     </ul>
                     <!-- END START TASK LIST -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section ('custom-footer')
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript">
   var table_sample2 = $("#sample_2").DataTable();
   var dataDisplay = [];
   var urls = '{{URL::to("/bridge/get/influencers?topic=".str_replace(" ","%20",$data["topic"]))}}';
   console.log(urls);

   $.get(urls ).done(function( response ) {
      dataDisplay = response;
      var pieData = [];
      for (var i = 0; i< response.length; i++){
         pieData.push([response[i][2], response[i][4]]);
      }
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Persons');
        data.addColumn('number', 'Tweet');
        data.addRows(pieData);
 
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        function selectHandler() {
          var selectedItem = chart.getSelection()[0];
          if (selectedItem) {
            var personName = data.getValue(selectedItem.row, 0);
            getData(personName);
          }
        }

        google.visualization.events.addListener(chart, 'select', selectHandler);    
        chart.draw(data);
        function getData(person){
          var table_sample2 = $("#sample_2").DataTable();
          var tableView = $("table[table_view='post_view'] > tbody");
          personArray = person.split();
          person = '';
          for(var i = 0; i < personArray.length; i++){
            person += personArray[i];
            if (i+1 != personArray.length)
              person += '%20';
          }
          var postURL = "{{URL::to('/bridge/get/posts')}}"+'?topic={{str_replace(" ","%20", $data["topic"])}}&person='+person;
          $.get(postURL + '&type=tweet').done(function(response) {
            console.log(postURL + '&type=tweet');
            dataDisplay = postURL;
            @include('page.script.datatunneling'); 
          });
          @include('page.script.tunnel');
          $.get(postURL + '&type=fb').done(function(response) {
            console.log(postURL + '&type=fb');
            dataDisplay = postURL;
            @include('page.script.datatunneling'); 
          });
          @include('page.script.tunnel');
          $.get(postURL + '&type=berita').done(function(response) {
            console.log(postURL + '&type=berita');
            dataDisplay = postURL;
            @include('page.script.datatunneling'); 
          });
          @include('page.script.tunnel');
       }
      }
   });
   </script>
   @endsection