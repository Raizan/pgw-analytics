@extends('template.index')
@section('content')
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
    <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Mapping</div>
            </div>
            <div class="portlet-body" style="height: 500px;">
               <div id="clouds" style="height: 500px; width: 100%">

                  
               </div>
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/plugins/jQCloud-master/dist/jqcloud.css')}}">
   @endsection
   @section('custom-footer')

   <script type="text/javascript">
      $("#sample_1").DataTable();
      $('input[name="daterange"]').daterangepicker();

   </script>
   <script type="text/javascript" src="{{asset('assets/assets/scripts/tunnel.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/assets/plugins/jQCloud-master/dist/jqcloud.js')}}"></script>
   <script type="text/javascript">
   var words = [];
   var dataDisplay = [];
      $.get("{{URL::to('bridge/get/related_words?topic='.$data['topic'])}}").done(function(response){
         response = jQuery.parseJSON(response);
         dataDisplay = response;
         for (var i = 0; i<response.length; i++){
            words.push({text: response[i].word, weight: response[i].score});
         }
         $('#clouds').jQCloud(words);
      });
   </script>
   @endsection