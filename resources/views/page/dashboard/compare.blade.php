@extends('template.index')

@section('content')
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="row">
      <div class="col-md-6 col-sm-12">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <label >TOPIC #1</label>
                  <select  class="form-control" name="topic_1">
                     <?php
                     $iter = 0;
                     ?>
                     @foreach($data['content'] as $print)
                        @if ($iter == 0)
                        <option value="{{str_replace(' ', '%20', $print->keyword)}}" selected>{{$print->keyword}}</option>
                        @else 
                        <option value="{{str_replace(' ', '%20', $print->keyword)}}">{{$print->keyword}}</option>
                        @endif
                        <?php $iter++?>
                     @endforeach
                  </select>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 col-sm-12">
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <label >TOPIC #2</label>
                  <select  class="form-control" name="topic_2">
                     <?php
                     $iter = 0;
                     ?>
                     @foreach($data['content'] as $print)
                        @if ($iter == 0)
                        <option value="{{str_replace(' ', '%20', $print->keyword)}}" selected>{{$print->keyword}}</option>
                        @else 
                        <option value="{{str_replace(' ', '%20', $print->keyword)}}">{{$print->keyword}}</option>
                        @endif
                        <?php $iter++?>
                     @endforeach
                  </select>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="row" style="margin-bottom: 25px">
      <center>
         <button class="btn red" id="compare_button">Compare</button>
      </center>
   </div>
   <style type="text/css">
      .compare-body{
         height: 400px;
      }
   </style>
    <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Influencer</div>
            </div>
            <div class="portlet-body compare-body">
               <div class="row">
                  <div class="col-md-6 col-sm-12">
                     <div id="piechart_1" style="width:100%;"></div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <div id="piechart_2" style="width:100%;"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-sm-12">
         <div class="portlet box blue">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Top Person</div>
            </div>
            <div class="portlet-body compare-body">
               <div class="row">
                  <div class="col-md-6 col-sm-12">
                     <div id="lineChart_1" style="width:100%;"></div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <div id="lineChart_2" style="width:100%;"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-sm-12">
         <div class="portlet box blue">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Demography</div>
            </div>
            <div class="portlet-body compare-body">
               <div class="row">
                  <div class="col-md-12">
                     <div id="map" style="height: 370px"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-sm-12">
         <div class="portlet box blue">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Sentiment</div>
            </div>
            <div class="portlet-body compare-body">
               <div class="row">
                  <div class="col-md-6 col-sm-12">
                     <div id="sentiment_1" style="width:100%;"></div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <div id="sentiment_2" style="width:100%;"></div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-12 col-sm-12">
         <div class="portlet box blue">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Keyword</div>
            </div>
            <div class="portlet-body compare-body">
               <div class="row">
                  <div class="col-md-6 col-sm-12">
                     <div id="mapping_1" style="height: 500px; width: 100%">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-12">
                     <div id="mapping_2" style="height: 500px; width: 100%">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   <link rel="stylesheet" type="text/css" href="{{asset('assets/assets/plugins/jQCloud-master/dist/jqcloud.css')}}">
   @endsection
   @section('custom-footer')
   <script type="text/javascript" src="{{asset('assets/assets/plugins/jQCloud-master/dist/jqcloud.js')}}"></script>
   <script type="text/javascript">
      <?php
      $now = date('Y-m-d');
      $startDate = strtotime('-30 day', strtotime($now));
      $startDate = date('Y-m-d', $startDate);
      // $endDate = strtotime('-0 day', strtotime($now));
      $endDate = date('Y-m-d');
      ?>

      var dateStart = "{{$startDate}}";
      var dateStop = "{{$endDate}}";

      var dataDisplay_1 = [];
      var dataDisplay_2 = [];
      var map;
      $("#compare_button").click(function(){
         var topic_1 = $('select[name=topic_1]').val();
         var topic_2 = $('select[name=topic_2]').val();

         var influencer_1 = getData(1,topic_1);
         var influencer_2 = getData(2,topic_2);
      });
      function getData(dataDisplay, topic){
         getInfluencer(dataDisplay,topic)
         getTopPerson(dataDisplay, topic);
         getDemography(dataDisplay, topic);
         getSentiment(dataDisplay, topic);
         getKeywordMapping(dataDisplay, topic);
      }
      function setData_1(type, data){
         dataDisplay_1.push({type: type, data: data});
         if (dataDisplay_1.length >= 5)
            showData(dataDisplay_1, 1);
      }
      function setData_2(type, data){
         dataDisplay_2.push({type: type, data: data});
         if (dataDisplay_2.length >= 5)
            showData(dataDisplay_2, 2);
      }

      function getInfluencer(dataDisplay,topic){
         var urls = '{{URL::to("/bridge/get/influencers?topic=")}}' + topic+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
         console.log(urls);
         $.get(urls ).done(function( response ) {
            response = jQuery.parseJSON(response);
            influencerData = response;
            var pieData = [];
            for (var i = 0; i< response.length; i++){
               pieData.push([influencerData[i].display_name, influencerData[i].count]);
            }
            if (dataDisplay == 1)
               setData_1('influencers', pieData);
            else if (dataDisplay == 2)
               setData_2('influencers', pieData);
         });
      }

      function getTopPerson(dataDisplay, topic){
         var urls = '{{URL::to("/bridge/get/top_persons")}}?topic='+topic+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
         console.log(urls);
         $.get(urls).done(function(response){
            response = jQuery.parseJSON(response);
            if (dataDisplay == 1)
               setData_1('top_persons', response);
            else if (dataDisplay == 2)
               setData_2('top_persons', response);
         });
      }

      function getDemography(dataDisplay, topic){
         var urls = "{{URL::to('/bridge/get/demography?topic=')}}"+topic+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
         console.log(urls);
         $.get(urls).done(function(response){
             geoLocation = jQuery.parseJSON(response);
             if (dataDisplay == 1)
               setData_1('demography', geoLocation);
            else if (dataDisplay == 2)
               setData_2('demography', geoLocation);
          });
      }

      function getSentiment(dataDisplay, topic){
         var urls = '{{URL::to("/bridge/get/sentiment?topic=")}}'+topic+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
         console.log(urls);
         $.get(urls).done(function( response ) {
            response = jQuery.parseJSON(response);
            if (dataDisplay == 1)
               setData_1('sentiment', response);
            else if (dataDisplay == 2)
               setData_2('sentiment', response);
         });
      }

      function getKeywordMapping(dataDisplay, topic){
         $.get("{{URL::to('bridge/get/related_words?topic=')}}"+topic).done(function(response){
            response = jQuery.parseJSON(response);
            var words = [];
            for (var i = 0; i<response.length; i++){
               words.push({text: response[i].word, weight: response[i].score})+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
            }
            if (dataDisplay == 1)
               setData_1('mapping', words);
            else if (dataDisplay == 2)
               setData_2('mapping', words);
         });
      }
      function showData(data, dataDisplay){
         for (var i = 0; i < data.length; i++){
            switch(data[i].type){
               case 'influencers':
                  showInfluencers(data[i],dataDisplay);
                  break;
               case 'top_persons':
                  showTopPerson(data[i],dataDisplay);
                  break;
               case 'demography':
                  showDemography(data[i],dataDisplay);
                  break;
               case 'sentiment':
                  showSentiment(data[i],dataDisplay);
                  break;
               case 'mapping':
                  showKeywordMapping(data[i],dataDisplay);
                  break;
               default: 
                  break;
            }
         }
      }

      function showInfluencers(pieData, dataDisplay){
         google.charts.load('current', {'packages':['corechart']});
         google.charts.setOnLoadCallback(drawChart);
         function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Source');
            data.addColumn('number', 'Post Count');
            data.addRows(pieData.data);

            var options = {
               title: 'Influencer for topic '+dataDisplay,
               legend: {position: 'bottom'}
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart_'+dataDisplay));
            chart.draw(data, options);
         }
      }
      function showTopPerson(response, dataDisplay){
         response = response.data;
         var persons = [];
         google.charts.load('current', {'packages':['line']});
         google.charts.setOnLoadCallback(drawChart);
         var selectedItem = [];
         function personValidation(name){
            var isExist = 0;
            for (var i = 0; i < persons.length; i++){
               if (persons[i].screen_name == name.screen_name)
               isExist++;
            }
            if (isExist == 0)
               persons.push(name);
         }
         function drawChart() {
            for (var i = 0; i< response.length; i++){
               for (var j = 0; j < response[i].persons.length; j++){
                  personValidation(response[i].persons[j]);
               }
            }
            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Persons');
            for (var i = 0; i< persons.length; i++){
               data.addColumn('number', persons[i].display_name);
            }
            var contents = [];

            for (var i = 0; i < response.length; i++){
               var content = [];
               content.push(response[i].date);
               for (var j = 0; j < persons.length; j++){
                  var isExist = -1;
                  for (var k = 0; k < response[i].persons.length; k++){
                     if (response[i].persons[k].screen_name == persons[j].screen_name)
                        isExist = k;
                  }
                  if (isExist == -1)
                     content.push(0);
                  else 
                     content.push(response[i].persons[isExist].post_count);
               }
               contents.push(content);
            }
            data.addRows(contents);
            var options = {
               height: 500,
               axes: {
                  x: {
                  0: {side: 'bottom'}
                  }
               }
            };

            var chart = new google.visualization.LineChart(document.getElementById('lineChart_'+dataDisplay));
            function selectHandler() {
               selectedItem = chart.getSelection()[0];
               if (selectedItem) {
               }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);    
            chart.draw(data, options);
         }
      }
      function showDemography(response, dataDisplay){
         var icons = ['http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|09e','http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|e66'];
         if (map != null){
            for (var i = 0; i < response.data.length; i++){
               new google.maps.Marker({
                  position: new google.maps.LatLng(response.data[i].coordinates[1], response.data[i].coordinates[0]),
                  icon: icons[dataDisplay-1],
                  map: map
               });
            }
         }
      }
      function showSentiment(response, dataDisplay){
         console.log(response);
         var pieData = [];
         var positive = 0;
         var negative = 0;
         var neutral = 0;
         for(var i = 0; i < response.data.length; i++){
            positive += response.data[i].sentiment.positive;
            negative += response.data[i].sentiment.negative;
            neutral += response.data[i].sentiment.neutral;
         }
         pieData.push(['positive', positive]);
         pieData.push(['negative', negative]);
         pieData.push(['neutral', neutral]);
         console.log(pieData);
         google.charts.load('current', {'packages':['corechart']});
         google.charts.setOnLoadCallback(drawChart);

         function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Sentiment');
            data.addColumn('number', 'Post Count');
            data.addRows(pieData);

            var chart = new google.visualization.PieChart(document.getElementById('sentiment_'+dataDisplay));
            function selectHandler() {
               var selectedItem = chart.getSelection()[0];
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);
            chart.draw(data, {colors: ['#0c0', '#c00', '#ccc'], legend: 'bottom'});
         }
      }
         var words = [];
      function showKeywordMapping(response, dataDisplay){
         response = response.data;
         $('#mapping_'+dataDisplay).jQCloud(response);
      }
      function addMarker(feature) {
         var marker = new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.type].icon,
            map: map
         });
      }
      function initMap(){
         map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: -1.8785136, lng: 113.6952623},
            zoom: 5
         });
      }
   </script>
   <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsXNCvwTsLM00_NuUvBFK7Meg6sqNwP3I&callback=initMap" async defer></script>
   @endsection