<?php
$now = date('Y-m-d');
$startDate = strtotime('-30 day', strtotime($now));
$startDate = date('Y-m-d', $startDate);
// $endDate = strtotime('-1 day', strtotime($now));
$endDate = date('Y-m-d');
?>
@section('custom-header')
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@extends('template.index')
@section('content')
 
  
    <div class="row">
      <div class="col-md-12">
          @include('template.breadcrumb')
       </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Peak Time</div>
               <div class="pull-right">
                  <input type="text" name="daterange" value="01/01/2016 - 01/31/2016" style="color: red; border: none; padding-left: 10px" />
               </div>
            </div>
            <div class="portlet-body">
              <b><center>Showing Graph for <i>Peak Time</i></center></b>
              <b><center id="count"></center></b>
              <div id="line_top_x"></div>
            </div>
         </div>
      </div>
   </div>
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box purple">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Portal Berita</div>
            </div>
            <div class="portlet-body" style="overflow: scroll;">
               <table class="table table-striped table-bordered table-hover" id="portal_log" table_view="post_view">
               <thead>
                  <tr>
                      <th>No</th>
                      <th>Topic</th>
                      <th>Timestamp</th>
                      <th>URL</th>
                      <th>Source</th>
                  </tr>
              </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
   <div class="row ">
      <div class="col-md-8 col-sm-12">
         <div class="portlet box" style="background-color: #3b5998; border: solid 0.1px #3b5998">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Facebook</div>
            </div>
            <div class="portlet-body" style="background-color: #E9EBEE">
               <div class="task-content">
                  <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
                     <script src="//connect.facebook.net/en_US/sdk.js#xfbml=1&amp;version=v2.5" async></script>
                     <style type="text/css">
                        ul{
                           list-style-type: none;
                           padding-left: 0px;
                        }
                        #facebook_log > li{
                           border-radius: 2px !important;
                           padding: 10px;
                           color: black;
                           margin-bottom: 10px;
                           background-color: white;
                        }
                        #facebook_log > li > .facebook-person{
                           font-weight: bold;
                           font-size: 18px;
                           color: #3b5998;
                        }
                        #facebook_log > li > .facebook-person > a{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:hover{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:visited{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-person > a:active{
                           text-decoration: none
                        }
                        #facebook_log > li > .facebook-content{
                           padding-bottom: 5px;
                           border-bottom: solid 0.5px #ccc;
                           color: black;
                        }
                        #facebook_log > li > .facebook-time{
                           color: #999;
                        }
                     </style>
                     <ul class="" id="facebook_log">
                        
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-12">
         <div class="portlet box blue tasks-widget">
            <div class="portlet-title">
               <div class="caption"><i class="icon-check"></i>Twitter Log</div>
            </div>
            <div class="portlet-body">
               <div class="task-content">
                  <div class="scroller" style="height: 500px;" data-always-visible="1" data-rail-visible1="1">
                     <!-- START TASK LIST -->
                     <ul class="task-list" id="twitter_log">
                     </ul>
                     <!-- END START TASK LIST -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript" src="{{asset('assets/assets/scripts/tunnel.js')}}"></script>
   <script type="text/javascript">
   var dateStart = "{{$startDate}}";
   var dateStop = "{{$endDate}}";
   var persons = [];
   var peakData = [];
   var urls = [];
   var graphData = [];
   var topics = [];
   @foreach($data['content'] as $d)
   var url = '{{URL::to("/bridge/get/post_range")}}?topic={{str_replace(" ","%20",$d->keyword)}}&n=5&date_start='+dateStart+'&date_stop='+dateStop+'&search_type=count';
   urls.push(['{{$d->keyword}}',url]);
   topics.push('{{$d->keyword}}');
   @endforeach
   var count = 0;
   for (var i = 0; i<urls.length; i++){
      getPeak(urls[i]);
   }

   $(function() {
      $('input[name="daterange"]').val(moment(dateStart).format('MM-DD-YYYY') + ' - ' + moment(dateStop).format('MM-DD-YYYY'));
       $('input[name="daterange"]').daterangepicker({
         'startDate' : moment(dateStart).format('MM-DD-YYYY'),
         'endDate' : moment(dateStop).format('MM-DD-YYYY'),
       }, function(start, end, label) {
         
         dateStart = start.format('YYYY-MM-DD');
         dateStop = end.format('YYYY-MM-DD');
         for (var i = 0; i<urls.length; i++){
            getPeak(urls[i]);
         }
       });
   });
   function getPeak(url){
      $.get(url[1]).done(function(response){
         try{
            response = JSON.parse(response);
            for (var i = 0; i < response.length; i++){
               response[i].topic = url[0];
               peakData.push(response[i]);
            }
         } catch(error){

         }
         count++;
         showGraph();
      });
   }
   
   function showGraph(){
      if (count >= urls.length){
         var columns = [];
         for (var i = 0; i < urls.length; i++){
            columns.push(urls[i][0]);
         }
         var rows = [];

         for (var i = 0; i<peakData.length; i++){
            var isExist = -1;
            for (var j = 0; j<rows.length; j++){
               if (rows[j][0] == peakData[i].date)
                  isExist++;
            }
            if (isExist == -1)
               rows.push([peakData[i].date]);
         }

         graphData = {'columns' : columns, 'rows' : rows};
         var graphTemplate = [];
         for (var i = 0; i<rows.length; i++){
            var rowTemplate = [];
            rowTemplate.push(rows[i][0]);
            for (var j = 0; j<topics.length; j++){
               var topicCount = 0;
               for (var k = 0; k < peakData.length; k++){
                  if(peakData[k].date == rowTemplate[0] && peakData[k].topic == topics[j])
                     topicCount = peakData[k].count;
               }
               rowTemplate.push(topicCount);
            }
            // console.log(rowTemplate);
            graphTemplate.push(rowTemplate);
         }
         graphData.rows = graphTemplate;

         google.charts.load('current', {'packages':['line']});
         google.charts.setOnLoadCallback(drawChart);
         
         function drawChart(){
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Date');
            for (var i = 0; i < topics.length; i++){
               data.addColumn('number', topics[i]);
            }
            data.addRows(graphData.rows);
            var chart = new google.charts.Line(document.getElementById('line_top_x'));
            function selectHandler() {
               selectedItem = chart.getSelection()[0];
               if (selectedItem) {
                  var column = selectedItem.column;
                  var row = selectedItem.row;
                  // console.log(column, row);
                  if (column != null && row != null){
                     getDetailPosts(graphData.columns[column-1], graphData.rows[row][0]);
                  }
               }
            }
            function getDetailPosts(name, date){
               // name = validateName(name);
               var tunnel = new Tunnel($("#facebook_log"), $("#tweeter_log"), $("#portal_log"));
               tunnel.beritas = new Berita($("#portal_log"));
               tunnel.facebooks = new Facebook($("#facebook_log"));
               tunnel.twitters = new Twitter($("#twitter_log"));

               var postType = ['tweet', 'fb', 'berita'];
               var totalIter = 0;
               var showIter = 0;

               for (var j = 0; j < postType.length; j++){
                  getPosts(postType[j], name, date);
                  totalIter++;
               }
               function getPosts(type, name,date){
                  var getURL = "{{URL::to('/bridge/get/posts')}}"+'?topic='+name+'&type='+type+'&n=20&date='+date;
                  $.get(getURL).done(function(response) {
                     response = jQuery.parseJSON(response);
                     for (var i = 0; i < response.length; i++){
                        tunnel.factory(response[i]);
                     }
                     show();
                  });
               }
               function show(){
                  showIter++;
                  if (showIter >= totalIter)
                    tunnel.show();
               }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);    
            chart.draw(data, {height: 500, axes: { x: { 0: {side: 'bottom'} } } } );
            countData();
            function countData(){
               var total = 0;
               for (var i = 0; i < graphData.rows.length; i++){
                  for (var j = 1; j<graphData.rows[i].length; j++){
                     total += graphData.rows[i][j];
                  }
               }
               $('#count').text('Total Data: '+total);
            }
         }
      }
   }
   
  </script>
   @endsection