  function prependTwitter(data) {
     if (data.url != '')
     {
     $("#notif-twitter").remove();
        $.ajax({
          method: "GET",
          url: 'https://publish.twitter.com/oembed?url='+data.url,
          dataType: 'jsonp',
          success: function(response){
              $("#twitter_log").prepend(response.html);
          }
        });
     }
  }
  function prependFacebook(data) {
     console.log(data);
     if (data.url != '')
     {
     $("#notif-facebook").remove();
        $("#facebook_log").prepend('<li><div class="facebook-person"><a href="'+data.url+'" target="_blank">'+ data.person +'</a></div><div class="facebook-time">'+data.timestamp+'</div><div class="facebook-content">'+ data.post +'</div></li>');
     }
  }
  function prependPortal(no, topic, timestamp, url, source){
     var counter = '0';
     table_sample2.row.add([no, topic, timestamp, '<a href="'+url+'">'+url+'</a>' , source]).draw(false);
  }