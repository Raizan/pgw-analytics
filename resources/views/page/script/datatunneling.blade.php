var portalData = [];

for (var i = 0; i < response.length - 1; i++) {
  if (response[i].source == 'twitter'){
     prependTwitter(response[i]);
  }
  else if (response[i].source == 'facebook'){
     prependFacebook(response[i]);
  } else{
     portalData.push([portalData.length+1, response[i].topic, response[i].timestamp, response[i].url, response[i].source]);
     prependPortal(portalData.length, response[i].topic, response[i].timestamp, response[i].url, response[i].source);
  }
}