@extends('template.index')
@section('content')
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box green">
            <div class="portlet-title">
            	<div class="caption"><i class="icon-bell"></i>Crawl Progress</div>
               	<div class="actions">
					<button href="{{URL::to('bridge/get/restart_crawler')}}" class="btn red btn-lrg" id="crawl_restart"><i class="icon-refresh"></i> Restart Crawl</button>
				</div>
            </div>
            <div class="portlet-body">
            <style type="text/css">
               tbody > tr > td{
                  max-width: 100px;
                  word-wrap: break-word;
               }
            </style>
               <table class="table table-striped table-bordered table-hover" id="sample_2" table_view="post_view">
                  <thead>
                     <tr>
                         <th>No</th>
                         <th style="max-width: 100px">URL</th>
                         <th>Source</th>
                         <th>Timestamp</th>
                     </tr>
                 </thead>
               </table>
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   <script type="text/javascript">
      $("#sample_1").DataTable();   
   </script>
   <script type="text/javascript">
   var dataResult = [];
   var maxDataResult = 25;
   var getResponse = [];
   var url = "{{URL::to('/bridge/get/latest_crawl')}}";
   var dataTable = $("#sample_2").DataTable({
       "aaSorting": [[ 0, "desc" ]]
   });
   setInterval(function(){
      $.get(url).done(function(response){
         getResponse = response;
         response = jQuery.parseJSON(response);
         dataValidation(response);
         refreshDataTable();
      });

      function dataValidation(response){
         for(var i = 0; i<response.length; i++){
            var isExisted = false;
            for (var j = 0; j<dataResult.length; j++){
               if (jQuery.parseJSON(response[i]).url == dataResult[j].url)
                  isExisted = true;
            }
            if (!isExisted){
               var dataResultLength = dataResult.length;
               var excededDataResultLength = dataResultLength - maxDataResult;
               if (excededDataResultLength > 0){
                  dataResult.splice(0, excededDataResultLength);
               }
               dataResult.push(jQuery.parseJSON(response[i]));
            }
         }
      };
      function refreshDataTable(){
         dataTable.clear();
         for(var i = dataResult.length-1; i >= 0; i--){
            dataTable.row.add([i + 1, '<a href="'+dataResult[i].url +'">' + dataResult[i].url + '</a>',dataResult[i].source, dataResult[i].time.replace('T', ' ')]).draw();
         }
      }
   }, 5000);
   $('#crawl_restart').click(function(){
      $.get($(this).attr('href')).done(function(response){
         
         if (response){
            toastr.success('Crawler Restarted');
         }
      })
   });
   </script>
   @endsection