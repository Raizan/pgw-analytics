<h2>
	TOPIC: {{strtoupper(str_replace('%20', ' ', $data['topic']))}}
</h2>
@if (count($data['header']) > 0)
<h3>General</h3>
<hr>
<table style="width: 300px; ">
	<thead>
		<?php
		$keys = [];
		?>
		@foreach($data['header'][0] as $key => $value)
		<td>
			{{$key}}
			<?php
			array_push($keys, $key);
			?>
		</td>
		@endforeach
	</thead>
	<tbody>
		@foreach($data['header'] as $d)
		<tr>
			@foreach($d as $k => $v)
				<td>{{$v}}</td>
			@endforeach
		</tr>
		@endforeach
	</tbody>
</table>
<br>
@endif
@if (count($data['content']['tweet']) > 0)
<h3>Twitter</h3>
<hr>
<table style="width: 300px; border: black">
	<thead>
		<td>No</td>
		<td>User Name</td>
		<td>Display Name</td>
		<td>URL</td>
		<td>Type</td>
		<td>Timestamp</td>
	</thead>
	<tbody>
		<?php
		$i = 0;
		?>
		@foreach($data['content']['tweet'] as $c)
			<tr>
				<td>{{++$i}} </td>
				<td>{{$c->user}} </td>
				<td>{{$c->user_display}} </td>
				<td>{{$c->url}} </td>
				<td>
					@if ($c->retweet_to != 'null')
						Retweet
					@elseif ($c->reply_to != 'null')
						Reply
					@else
						Tweet
					@endif
				</td>
				<td>{{$c->time}} </td>
			</tr>
		@endforeach
	</tbody>
</table>
<br>
@endif
<h3>Facebook</h3>
<hr>
@if (count($data['content']['berita']) > 0)
<table style="width: 300px; border: black">
	<thead>
		<td>No</td>
		<td>Title</td>
		<td>Source</td>
		<td>Time</td>
		<td style="max-width: 200px">URL</td>
	</thead>
	<tbody>
		<?php
		$i = 0;
		?>
		@foreach($data['content']['berita'] as $c)
			<tr>
				<td>{{++$i}} </td>
				<td>{{$c->title}} </td>
				<td>{{$c->sumber}} </td>
				<td>{{$c->time}} </td>
				<td style="max-width: 200px">{{$c->url}} </td>
			</tr>
		@endforeach
	</tbody>
</table>
@endif