@extends('template.index')
@section('content')
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="clearfix"></div>
   <div class="row ">
      <?php
      $tableIter = 0;
      ?>
      @foreach($data['container'] as $container)
         <div class="col-md-3 col-sm-12">
            <div class="portlet box {{$container['color']}}">
               <div class="portlet-title">
                  <div class="caption"><i class="icon-bell"></i>{{$container['name']}}</div>
               </div>
               <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover" id="sample_{{++$tableIter}}">
                     <thead>
                        <tr>
                           <th>No</th>
                           <th>Keyword</th>
                           <th>Assigned</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        $iter = 0;
                        ?>
                        @foreach($data['content'] as $print)
                        <tr class="odd gradeX">
                           <td>{{++$iter}}</td>
                           <td><a href="{{URL::to('download/'.$container['link'].'/'.$print->id)}}">{{$print->keyword}}</a></td>
                           <td>{{$print->created_at}}</td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      @endforeach
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   <script type="text/javascript">
      
   <?php
   for($i = 1; $i <= count($data['content']); $i++){
      ?>
      $("#sample_{{$i}}").DataTable();
      <?php
      }
   ?>
   </script>
   @endsection
