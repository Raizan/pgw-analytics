
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Flot Examples: Real-time updates</title>
	<style>
        * {	padding: 0; margin: 0; vertical-align: top; }

        body {
            font: 18px/1.5em "proxima-nova", Helvetica, Arial, sans-serif;
        }

        a {	color: #069; }
        a:hover { color: #28b; }

        h2 {
            margin-top: 15px;
            font: normal 32px "omnes-pro", Helvetica, Arial, sans-serif;
        }

        h3 {
            margin-left: 30px;
            font: normal 26px "omnes-pro", Helvetica, Arial, sans-serif;
            color: #666;
        }

        p {
            margin-top: 10px;
        }

        button {
            font-size: 18px;
            padding: 1px 7px;
        }

        input {
            font-size: 18px;
        }

        input[type=checkbox] {
            margin: 7px;
        }

        #header {
            position: relative;
            width: 900px;
            margin: auto;
        }

        #header h2 {
            margin-left: 10px;
            vertical-align: middle;
            font-size: 42px;
            font-weight: bold;
            text-decoration: none;
            color: #000;
        }

        #content {
            width: 880px;
            margin: 0 auto;
            padding: 10px;
        }

        #footer {
            margin-top: 25px;
            margin-bottom: 10px;
            text-align: center;
            font-size: 12px;
            color: #999;
        }

        .demo-container {
            box-sizing: border-box;
            width: 850px;
            height: 200px;
            padding: 20px 15px 15px 15px;
            margin: 15px auto 30px auto;
            border: 1px solid #ddd;
            background: #fff;
            background: linear-gradient(#f6f6f6 0, #fff 50px);
            background: -o-linear-gradient(#f6f6f6 0, #fff 50px);
            background: -ms-linear-gradient(#f6f6f6 0, #fff 50px);
            background: -moz-linear-gradient(#f6f6f6 0, #fff 50px);
            background: -webkit-linear-gradient(#f6f6f6 0, #fff 50px);
            box-shadow: 0 3px 10px rgba(0,0,0,0.15);
            -o-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
            -ms-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
            -moz-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
            -webkit-box-shadow: 0 3px 10px rgba(0,0,0,0.1);
        }

        .demo-placeholder {
            width: 100%;
            height: 100%;
            font-size: 14px;
            line-height: 1.2em;
        }

        .legend table {
            border-spacing: 5px;
        }
    </style>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="../../excanvas.min.js"></script><![endif]-->
	<script src="{{asset('assets/assets/plugins/flot/jquery.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/assets/plugins/flot/jquery.flot.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
    $(function() {
		// We use an inline data source in the example, usually data would
		// be fetched from a server
		var data = [];
		
		// Set up the control widget

		var updateInterval = 1000;
        var counter = 0;
        function getData() {
            $.ajax({
                url: "{{ URL::to('stat/cpu_load') }}",
                type: 'GET',
                success: function(res) {
                    data.push(res);
                    // console.log(data);
                }
            });
            var renderData = [];
            renderData.push([counter, data[counter]]);
            counter += 1;
            return renderData;
        }
        setInterval( getData, 1000 );
		var plot = $.plot("#placeholder", [ getData() ], {
			series: {
				shadowSize: 0	// Drawing is faster without shadows
			},
			yaxis: {
				min: 0,
				max: 100
			},
			xaxis: {
				show: false
			}
		});

		function update() {
			plot.setData([getData()]);
			plot.draw();
			setTimeout(update, updateInterval);
		}

		update();

		// Add the Flot version string to the footer

		$("#footer").prepend("Flot " + $.plot.version + " &ndash; ");
	});

	</script>
</head>
<body>
	<div id="content">
		<div class="demo-container">
			<div id="placeholder" class="demo-placeholder"></div>
		</div>
	</div>
</body>
</html>
