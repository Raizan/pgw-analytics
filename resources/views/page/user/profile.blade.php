@extends('template.index')
@section('content')
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="row">
      <div class="col-md-23 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>User Profile</div>
            </div>
            <div class="portlet-body">
               <div class="row">
                  <div class="col-md-2 col-sm-12 col-md-offset-2">
                    <div><h4>Facebook Media:</h4></div>
                     @if ($data['fb_connected'] == false)
                        <a href="{{ url('/facebook/connect') }}" class="btn blue col-md-12"><i class="icon-facebook"></i>  Connect to Facebook</a>
                     @else
                        @if ($data['fb_expired'] == true)
                           <a href="{{ url('/facebook/connect') }}" class="btn blue col-md-12"><i class="icon-facebook"></i>  Connect to Facebook</a>
                        @else
                           <a class="btn red col-md-14"><i class="icon-facebook"></i>  Connected to Facebook</a>
                        @endif
                     @endif   
                  </div>
                  <div class="col-md-6 col-sm-12">
                     {!! Form::open(array('route' => 'dashboard.user.store', 'method' => 'patch','files' => false, 'class' => 'form-horizontal form-bordered form-row-stripped')) !!}
                        <div class="form-body">
                           <div class="form-group">
                              {!! Form::label('name','Name : ', ['class' => 'control-label col-md-3']) !!}
                              <div class="col-md-9">
                                 {!! Form::text('name',$data['content']->name, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: John Doe', 'disabled' => 'disabled']) !!}
                              </div>
                           </div>
                           <div class="form-group">
                              {!! Form::label('email','Email : ', ['class' => 'control-label col-md-3']) !!}
                              <div class="col-md-9">
                                 {!! Form::text('email',$data['content']->email, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: john.doe@mail.com', 'disabled' => 'disabled']) !!}
                              </div>
                           </div>
                           <div class="form-actions fluid">
                              <div class="row">
                                 <div class="col-md-8 pull-right">
                                    <div class="col-md-8 pull-right">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     {!! Form::close()!!}
                  </div>
                  @if (Auth::user()->privilege_id == 1)
                  <div class="col-md-8 col-sm-12 col-md-offset-2">
                    <div><h4>Twitter Token:</h4></div>
                     {!! Form::open(array('route' => 'twitter.update', 'method' => 'post', 'class' => 'form-horizontal form-bordered form-row-stripped')) !!}
                        <div class="form-body">
                           <div class="form-group">
                              {!! Form::label('access_token','Access Token: ', ['class' => 'control-label col-md-3']) !!}
                              <div class="col-md-9">
                              @if ($errors->has('access_token'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('access_token') }}</strong>
                                    </span>
                              @endif
                                 {!! Form::text('access_token', $data['twitter']->access_token, ['class' => 'form-control', 'required' => 'required']) !!}
                              </div>
                           </div>
                           <div class="form-group">
                              {!! Form::label('access_token_secret','Access Token Secret: ', ['class' => 'control-label col-md-3']) !!}
                              <div class="col-md-9">
                              @if ($errors->has('access_token_secret'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('access_token_secret') }}</strong>
                                    </span>
                              @endif
                                 {!! Form::text('access_token_secret', $data['twitter']->access_token_secret, ['class' => 'form-control', 'required' => 'required']) !!}
                              </div>
                           </div>
                           <div class="form-group">
                              {!! Form::label('consumer_key','Consumer Key: ', ['class' => 'control-label col-md-3']) !!}
                              <div class="col-md-9">
                              @if ($errors->has('consumer_key'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('consumer_key') }}</strong>
                                    </span>
                              @endif
                                 {!! Form::text('consumer_key', $data['twitter']->consumer_key, ['class' => 'form-control', 'required' => 'required']) !!}
                              </div>
                           </div>
                           <div class="form-group">
                              {!! Form::label('consumer_secret','Consumer Secret: ', ['class' => 'control-label col-md-3']) !!}
                              <div class="col-md-9">
                              @if ($errors->has('consumer_secret'))
                                    <span class="help-block">
                                        <strong style="color:red">{{ $errors->first('consumer_secret') }}</strong>
                                    </span>
                              @endif
                                 {!! Form::text('consumer_secret', $data['twitter']->consumer_secret, ['class' => 'form-control', 'required' => 'required']) !!}
                              </div>
                           </div>
                           <div class="form-actions fluid">
                              <div class="row">
                                 <div class="col-md-8 pull-right">
                                    <div class="col-md-8 pull-right">
                                      {{ Form::submit('Save', array('class' => 'btn blue')) }}
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     {!! Form::close()!!}
                  </div>
                  @endif
               </div>
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   <script type="text/javascript">
      $("#sample_1").DataTable();   
   </script>
   <script type="text/javascript">
   
   </script>
   @endsection