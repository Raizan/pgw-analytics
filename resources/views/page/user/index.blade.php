@extends('template.index')
@section('content')
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
    <div class="row ">
      <div class="col-md-8 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Topic Log</div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                     <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Privilege</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     $iter = 0;
                     ?>
                     @foreach($data['content'] as $print)
                     <tr class="odd gradeX">
                        <td>{{++$iter}}</td>
                        <td>{{$print->name}} </td>
                        <td>{{$print->email}} </td>
                        <td>{{$print->privilege->name}}</td>
                        <td>
                           <a class="btn default" data-toggle="modal" href="#static" reference="{{$print->name}}" url="{{route('dashboard.user.delete', $print->id)}}" onclick="deleteModal(this)">Delete</a>
                           <!-- <a href="{{route('dashboard.user.delete', $print->id)}}">Hapus</a> -->
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-12">
         <div class="portlet box green">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>New User</div>
            </div>
            <div class="portlet-body">
               {!! Form::open(array('route' => 'dashboard.user.store', 'method' => 'post','files' => false, 'class' => 'form-horizontal form-bordered form-row-stripped')) !!}
                  <div class="form-body">
                     <div class="form-group">
                        {!! Form::label('name','Name : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                           @if ($errors->has('name'))
                           <span class="help-block">
                                <strong style="color:red">{{ $errors->first('name') }}</strong>
                           </span>
                           @endif
                           {!! Form::text('name',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: John Doe, Peter Schumacker']) !!}
                        </div>
                     </div>
                     <div class="form-group">
                        {!! Form::label('email','Email : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                           @if ($errors->has('email'))
                           <span class="help-block">
                                <strong style="color:red">{{ $errors->first('email') }}</strong>
                           </span>
                           @endif
                           {!! Form::text('email',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: john.doe@mmail.com']) !!}
                        </div>
                     </div>
                     <div class="form-group">      
                        {!! Form::label('password','Password : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                        @if ($errors->has('password'))
                           <span class="help-block">
                                <strong style="color:red">{{ $errors->first('password') }}</strong>
                           </span>
                        @endif
                           {!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                     </div>
                     <div class="form-group">
                        {!! Form::label('privilege','Privilege : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                        @if ($errors->has('privilege'))
                           <span class="help-block">
                                <strong style="color:red">{{ $errors->first('privilege') }}</strong>
                           </span>
                        @endif
                           <div class="row">
                              @foreach($data['privileges'] as $p)
                                 <label class="col-md-6">
                                    {!! Form::radio('privilege', $p['id'])!!}
                                    
                                    {{ ucfirst($p->name) }}
                                 </label>
                              @endforeach
                           </div>
                        </div>
                     </div>
                     <div class="form-actions fluid">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="col-md-12">
                                 {!! Form::submit('Tambahkan User',['class' => 'btn green col-md-12']) !!}
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               {!! Form::close()!!}
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   <script type="text/javascript">
      $("#sample_1").DataTable();   
   </script>
   <script type="text/javascript">
   var table_sample2 = $("#sample_2").DataTable();
   var tableView = $("table[table_view='post_view'] > tbody");
   var data;
   var dataToGet = [];
   var postType = ['tweet', 'fb', 'berita'];
   @foreach($data['content'] as $c)
      for (var i = 0; i < postType.length; i++){
         getDatas('{{$c->keyword}}', postType[i]);
      }
      dataToGet.push('{{$c->keyword}}');
   @endforeach
   function getDatas(topic, type){
      var getURL = "{{URL::to('/dump/recent_post')}}"+'?topic='+topic+'&type='+type;
      console.log(getURL);
      $.get(getURL).done(function(response) {
         data = response;
         var portalData = [];
         for (var i = 0; i < response.length - 1; i++) {
            if (response[i].source == 'twitter.com'){
               prependTwitter(response[i]);
            }
            else if (response[i].source == 'facebook.com'){
               prependFacebook(response[i]);
            } else{
               portalData.push([portalData.length+1, response[i].topic, response[i].timestamp, response[i].url, response[i].source]);
               prependPortal(portalData.length, response[i].topic, response[i].timestamp, response[i].url, response[i].source);
            }
         }
      });

      function prependTwitter(data) {
         if (data.url != '')
         {
            $.ajax({
              method: "GET",
              url: 'https://publish.twitter.com/oembed?url='+data.url,
              dataType: 'jsonp',
              success: function(response){
                  $("#twitter_log").prepend(response.html);
              }
            });
         }
      }
      function prependFacebook(data) {
         console.log(data);
         if (data.url != '')
         {
            $("#facebook_log").prepend('<li><div class="facebook-person"><a href="'+data.url+'" target="_blank">'+ data.person +'</a></div><div class="facebook-time">'+data.timestamp+'</div><div class="facebook-content">'+ data.post +'</div></li>');
         }
      }
      function prependPortal(no, topic, timestamp, url, source){
         var counter = '0';
         table_sample2.row.add([no, topic, timestamp, '<a target="_blank" href="'+url+'">'+url+'</a>' , source]).draw(false);
      }
   }
   </script>
   @endsection