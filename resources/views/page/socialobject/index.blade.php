@extends('template.index')
@section('content')
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-8 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Social Object Name</div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                     <tr>
                        <th>No</th>
                        <th>Facebook ID</th>
                        <th>Facebook Name</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     $iter = 0;
                     ?>
                     @foreach($data['content'] as $print)
                     <tr class="odd gradeX">
                        <td>{{++$iter}}</td>
                        <td>{{$print->id}}</td>
                        <td>{{$print->name}}</td>
                        <td>
                           <a class="btn default" data-toggle="modal" href="#static" reference="{{$print->name}}" url="{{URL::to('dashboard/object/social/delete/'.$print->id)}}" onclick="deleteModal(this)">Delete</a>
                           <!-- <a href="{{URL::to('dashboard/object/social/delete/'.$print->id)}}" class="btn">Delete</a> -->
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-12">
         <div class="portlet box green">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>New Social Object</div>
            </div>
            <div class="portlet-body">
               {!! Form::open(array('route' => 'dashboard.object.social.store', 'method' => 'post','files' => false, 'class' => 'form-horizontal form-bordered form-row-stripped')) !!}
                  <div class="form-body">
                     
                     <div class="form-group">
                        {!! Form::label('id','ID : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                           {!! Form::text('id',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: 227268729878, 156483584365269']) !!}
                           <span class="help-block">Tulis id object</span>
                        </div>
                     </div>
                     <div class="form-group">
                        {!! Form::label('name','Name : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                           {!! Form::text('name',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: Kompas TV, Metro TV']) !!}
                           <span class="help-block">Tulis nama object crawler</span>
                        </div>
                     </div>
                     <div class="form-actions fluid">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="col-md-12">
                                 {!! Form::submit('Tambahkan Object',['class' => 'btn green col-md-12']) !!}
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               {!! Form::close()!!}
            </div>
         </div>
      </div>
   </div>
   
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   <script type="text/javascript">
      
   </script>
   @endsection