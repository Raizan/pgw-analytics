 <div id="static" class="modal fade" tabindex="-1" aria-hidden="true" style="">
   <div class="modal-dialog" style="left: 0px">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Confirmation</h4>
         </div>
         <div class="modal-body">
            <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
               <p>Would you like to delete <span id="delete-name"></span>?</p>
            </div>
            <div class="modal-footer">
               <button type="button" data-dismiss="modal" class="btn default">Cancel</button>
               <a class="btn red" id="delete">Delete</a>
            </div>
         </div>
      </div>
   </div>

   <div class="footer">
      <div class="footer-inner">
         2016 &copy; Kemensos Social Media Analytic | Kementerian Sosial RI.
      </div>
      <div class="footer-tools">
         <span class="go-top">
         <i class="icon-angle-up"></i>
         </span>
      </div>
   </div>
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
   <!-- BEGIN CORE PLUGINS -->   
   <!--[if lt IE 9]>
   <script src="{{asset('assets/assets/plugins')}}/respond.min.js"></script>
   <script src="{{asset('assets/assets/plugins')}}/excanvas.min.js"></script> 
   <![endif]-->   
   <script src="{{asset('assets/assets/plugins/jquery-1.10.2.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jquery-migrate-1.2.1.min.js')}}" type="text/javascript"></script>   
   <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
   <script src="{{asset('assets/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js')}}" type="text/javascript" ></script>
   <script src="{{asset('assets/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>  
   <script src="{{asset('assets/assets/plugins/jquery.cookie.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script src="{{asset('assets/assets/plugins/jqvmap/jqvmap/jquery.vmap.js')}}" type="text/javascript"></script>   
   <script src="{{asset('assets/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}" type="text/javascript"></script>  
   <script src="{{asset('assets/assets/plugins/flot/jquery.flot.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/flot/jquery.flot.resize.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jquery.pulsate.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/bootstrap-daterangepicker/moment.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}" type="text/javascript"></script>     
   <script src="{{asset('assets/assets/plugins/gritter/js/jquery.gritter.js')}}" type="text/javascript"></script>
   <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
   <script src="{{asset('assets/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>

   <!-- END PAGE LEVEL PLUGINS -->
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{asset('assets/assets/scripts/app.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/scripts/index.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/scripts/tasks.js')}}" type="text/javascript"></script>
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   <script type="text/javascript" src="{{asset('assets/assets/plugins/select2')}}/select2.min.js"></script>

   
   <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
   <!-- <script type="text/javascript" src="{{asset('assets/assets/plugins/data-tables')}}/jquery.dataTables.js"></script>
   <script type="text/javascript" src="{{asset('assets/assets/plugins/data')}}-tables/DT_bootstrap.js"></script> -->
   <!-- END PAGE LEVEL PLUGINS -->
   <!-- END PAGE LEVEL SCRIPTS -->  
    <script>
      $.ajax({
          url: "{{ URL::to('/stat/storage') }}",
          type: "GET",
          dataType: "html",
          success: function (data) {  
              $('#storage').html(data);
          },
      });

      jQuery(document).ready(function() {    
         App.init(); // initlayout and core plugins
         Index.init();
         Index.initCharts(); // init index page's custom scripts
         Index.initMiniCharts();
         Index.initDashboardDaterange();
         Index.initIntro();
         Tasks.initDashboardWidget();
         
      });
      function deleteModal(target){
         target = $(target);
         $("#delete-name").text(target.attr('reference'));
         $("#delete").attr('href', target.attr('url'));
      }
      <?php
         $toastr = Session::get('toastr');
         // print_r($toastr);
         // if(Session::has('toasr')){
            // $toasr = Session::get('toasr');
            if ($toastr['type'] == 'success'){
               ?>
               toastr.success('{{$toastr["message"]}}');
               <?php
            } else if ($toastr['type'] == 'error'){
               ?>
               toastr.error('{{$toastr["message"]}}');
               <?php
            } else if ($toastr['type'] == 'warning'){
               ?>
               toastr.warning('{{$toastr["message"]}}');
               <?php
            }
            Session::forget('toastr');
         // }
         ?>
   </script>
   <!-- END JAVASCRIPTS -->