         <h3 class="page-title">
            @if (!is_null($data['page_title']))
                {{ $data['page_title'] }}
            @endif
            @if (!is_null(Session::get('selectedTopic')))
                | <small>Selected Topic: <b>{{ Session::get('selectedTopic') }}</b></small>
            @endif
         </h3>
         <ul class="page-breadcrumb breadcrumb">
            @if (!is_null($data['breadcrumb']))
                @for ($i = 0; $i < count($data['breadcrumb']); $i++)
                    @if ($i == 0)
                        <li>
                            <i class="icon-home"></i>
                                <a href="{{ $data['breadcrumb'][$i]['link'] }}">{{ $data['breadcrumb'][$i]['title'] }}</a> 
                        </li>
                    @else
                        <li>
                            <a href="{{ $data['breadcrumb'][$i]['link'] }}">{{ $data['breadcrumb'][$i]['title'] }}</a>
                        </li>
                    @endif
                    @if ($i != (count($data['breadcrumb']) - 1))
                        <i class="icon-angle-right"></i>
                    @endif
                @endfor
            @endif
         </ul>