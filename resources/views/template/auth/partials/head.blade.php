  <meta charset="utf-8" />
   <title>Kemensos Social Media Analytic | Kementerian Sosial RI</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="Kemensos RI" name="author" />
   <meta name="MobileOptimized" content="320">
    <link href="{{asset('assets/assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
    
   <link href="{{asset('assets/assets/plugins/select2/select2_metro.css')}}" rel="stylesheet" type="text/css"/>
    
   <link href="{{asset('assets/assets/css/style-metronic.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/style.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/style-responsive.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/pages/tasks.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/pages/login-soft.css')}}" rel="stylesheet" type="text/css" id="style_color"/>
      <link href="{{asset('assets/assets/css/themes/default.css')}}" rel="stylesheet" type="text/css" id="style_color"/>

   <link href="{{asset('assets/assets/css/custom.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="favicon.ico" />
 
