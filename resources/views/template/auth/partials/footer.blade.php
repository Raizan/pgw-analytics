<script src="{{asset('assets/assets/plugins/jquery-1.10.2.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jquery-migrate-1.2.1.min.js')}}" type="text/javascript"></script>   
    <script src="{{asset('assets/assets/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js')}}" type="text/javascript" ></script>
   <script src="{{asset('assets/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>  
   <script src="{{asset('assets/assets/plugins/jquery.cookie.min.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript" ></script>
   
   <script src="{{asset('assets/assets/plugins/jquery-validation/dist/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/assets/plugins/backstretch/jquery.backstretch.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/assets/plugins/select2/select2.min.js')}}" type="text/javascript"></script>

   <script src="{{asset('assets/assets/scripts/app.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/assets/scripts/login-soft.js')}}" type="text/javascript"></script>
 
      
     
   <!-- END PAGE LEVEL SCRIPTS --> 
   <script>
      jQuery(document).ready(function() {     
        App.init();
        Login.init();
      });
   </script>
 
  
