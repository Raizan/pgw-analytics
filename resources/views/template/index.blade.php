<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
@include('template.header')
@yield('custom-header')
</head>
 
 
<body class="page-sidebar-closed">
   @include('template.navigation')
   <div class="clearfix"></div>
   <!-- BEGIN CONTAINER --> 
   <div class="page-container" style="margin-top: 30px;">

     @include('template.sidebar')
      <!-- BEGIN PAGE -->
      <div class="page-content">
         @yield('content')
      </div>
      <!-- END PAGE -->
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
   @include('template.footer')
   @yield('custom-footer')
</body>
 
</html>