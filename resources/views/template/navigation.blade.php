<!-- BEGIN HEADER -->   
   <div class="header navbar navbar-inverse navbar-fixed-top">
      <!-- BEGIN TOP NAVIGATION BAR -->
      <div class="header-inner">
         <!-- BEGIN LOGO -->  
         <a class="navbar-brand" style="color:#fff;padding-left:9px" href="{{ url('/dashboard') }}">
         <!-- <img src="../assets/img/logo.png" alt="logo" class="img-responsive" /> -->
         KSM - Analytic
         </a>
         <!-- END LOGO -->
         <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
         <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
         <img src="{{ asset('assets/assets/img/menu-toggler.png') }}" alt="" />
         </a> 
         <!-- END RESPONSIVE MENU TOGGLER -->
         <!-- BEGIN TOP NAVIGATION MENU -->
         <ul class="nav navbar-nav pull-right">
            <!-- BEGIN INBOX DROPDOWN -->
            <li class="dropdown" id="header_inbox_bar">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                  data-close-others="true">
               <i class="icon-external-link"></i>
                </a>
               <ul class="dropdown-menu extended inbox">
                  <li>
                    <a href="javascript:;" id="trigger_fullscreen"><i class="icon-move"></i>Toggle Full Screen</a>
                  </li>
                  
                  
               </ul>
            </li>
            <!-- END INBOX DROPDOWN -->
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown user">
               <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
               <span class="username">{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</span>
               <i class="icon-angle-down"></i>
               </a>
               <ul class="dropdown-menu">
                  <li><a href="{{URL::to('dashboard/profile')}}"><i class="icon-user"></i> My Profile</a>
                  </li>
                  <li class="divider"></li>
                   
                  <li><a href="{{URL::to('/logout')}}"><i class="icon-key"></i> Log Out</a>
                  </li>
               </ul>
            </li>
            <!-- END USER LOGIN DROPDOWN -->
         </ul>
         <!-- END TOP NAVIGATION MENU -->
      </div>
      <!-- END TOP NAVIGATION BAR -->
   </div>
   <!-- END HEADER -->