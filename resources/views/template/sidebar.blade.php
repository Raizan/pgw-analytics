 <!-- BEGIN SIDEBAR -->
      <div class="page-sidebar navbar-collapse collapse">
         <!-- BEGIN SIDEBAR MENU -->        
         <ul class="page-sidebar-menu">
            <li>
               <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
               <div class="sidebar-toggler hidden-phone"></div>
               <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            </li>
            <li>
               <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
               <form class="sidebar-search" action="#" method="POST">
                  <div class="form-container">
                     <div class="input-box">
                        <a href="javascript:;" class="remove"></a>
                        <input type="text" placeholder="Search..."/>
                        <input type="button" class="submit" value=" "/>
                     </div>
                  </div>
               </form>
               <!-- END RESPONSIVE QUICK SEARCH FORM -->
            </li>
            <style type="text/css">
               ul.page-sidebar-menu > li.active > a{
                  background: #3e3e3e !important;
               }
            </style>
           <li class="">
               <a href="{{URL::to('/dashboard/index')}}">
               <i class="icon-home"></i> 
               <span class="title">Dashboard</span>
               <span class=""></span>
               </a>
            </li>
            <li class="">
               <a href="index.html">
               <i class="icon-bullhorn"></i> 
               <span class="title">Analytics</span>
               <span class=""></span>
               </a>
               <ul class="sub-menu">
                  <li >
                     <a href="{{URL::to('/dashboard/influencer/'.$data['topic'])}}">
                     Influencer</a>
                  </li>
                  <li >
                     <a href="{{URL::to('/dashboard/top-person/'.$data['topic'])}}">
                     Top Person</a>
                  </li>
                  <li>
                     <a href="{{URL::to('/dashboard/demography/'.$data['topic'])}}">
                        Demography</a>
                  </li>
                  <li >
                     <a href="{{URL::to('/dashboard/sentiment/'.$data['topic'])}}">
                     Sentiment</a>
                  <li >
                     <a href="{{URL::to('/dashboard/mapping/'.$data['topic'])}}">
                     Keyword Mapping</a>
                  </li>
                  </li>
               </ul>
            </li>
            <li class="">
               <a href="index.html">
               <i class="icon-cogs"></i> 
               <span class="title">Statistic & Report</span>
               <span class=""></span>
               </a>
               <ul class="sub-menu">
                  <li >
                     <a href="{{URL::to('dashboard/peak')}}">
                     Peak Time</a>
                  </li>
                  <li >
                     <a href="{{URL::to('dashboard/compare')}}">
                     Compare Topic</a>
                  </li>
                 
               </ul>
            </li>
            <li class="">
               <a href="{{URL::to('/dashboard/crawl')}}">
               <i class="icon-home"></i> 
               <span class="title">Crawl Streaming</span>
               <span class=""></span>
               </a>
            </li>
             <li class="">
               <a href="index.html">
               <i class="icon-download"></i> 
               <span class="title">Download</span>
               <span class=""></span>
               </a>
               <ul class="sub-menu">
                  <li >
                     <a href="{{URL::to('dashboard/download/csv')}}">
                     Download as CSV</a>
                  </li>
                  <li >
                     <a href="{{URL::to('dashboard/download/pdf')}}">
                     Download as PDF</a>
                  </li>
               </ul>
            </li>
            <li class="">
               <a href="index.html">
               <i class="icon-user"></i> 
               <span class="title">User Management</span>
               <span class=""></span>
               </a>
               <ul class="sub-menu">
                  <li >
                    <a href="{{URL::to('/dashboard/user')}}">
                     User List</a>
                  </li>
                  <li >
                     <a href="{{URL::to('/dashboard/log')}}">
                     Logs User</a>
                  </li>
               </ul>
            </li>
            <li class="">
               <a href="index.html">
               <i class="icon-rss"></i> 
               <span class="title">Object Management</span>
               <span class=""></span>
               </a>
               <ul class="sub-menu">
                  <li >
                    <a href="{{URL::to('/dashboard/object/all')}}">
                     Portal and Twitter</a>
                  </li>
                  <li >
                     <a href="{{URL::to('/dashboard/object/social')}}">
                     Social</a>
                  </li>
               </ul>
            </li>
         </ul>
         <!-- END SIDEBAR MENU -->
      </div>
      <!-- END SIDEBAR -->