<!-- BEGIN DASHBOARD STATS -->
<div class="row">
@if (Auth::user()->privilege_id == "1")
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   <!--<div class="portlet solid bordered light-grey">-->
      <div class="portlet-title">
         <!--<div class="caption"><i class="icon-signal"></i>Web Server Load</div>-->
         <!--<div class="tools">
            <div class="btn-group pull-right" data-toggle="buttons">
               <a href="" class="btn red btn-sm active">Web</a>
            </div>
         </div>-->
      </div>
      <div class="portlet-body">
         <div id="load_statistics_loading" style="display: none;">
            <img src="{{ asset('assets/assets/img/loading.gif') }}" alt="loading">
         </div>
         <!--<div id="load_statistics_content" class="display-none" style="display: block;">
            Placeholder for Web Server Load stat
         </div>-->
      </div>
   <!--</div>-->
</div> 
@endif
</div>
   <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
         <div class="dashboard-stat blue">
            <div class="visual">
               <i class="icon-bullhorn"></i>
            </div>
            <div class="details">
               <div class="number" style="font-size:24px">
                  {{$data['latest_topic']->keyword}}
               </div>
               <div class="desc">                           
                  Latest Topic
               </div>
            </div>
            <a class="more" href="{{ url('/dashboard') }}">
            View all topic<i class="m-icon-swapright m-icon-white"></i>
            </a>                 
         </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
         <div class="dashboard-stat green">
            <div class="visual">
               <i class="icon-hdd"></i>
            </div>
            <div class="details">
               <div id="storage" class="number"></div>
               <div class="desc">Used</div>
            </div>
            <div class="more">
            Storage
            </div>                 
         </div>
      </div>
      <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
         <div class="dashboard-stat purple">
            <div class="visual">
               <i class="icon-globe"></i>
            </div>
            <div class="details">
               <div class="number">403.434</div>
               <div class="desc">Items Crawled</div>
            </div>
            <a class="more" href="{{ url('/dashboard/crawl') }}">
            Stream Now <i class="m-icon-swapright m-icon-white"></i>
            </a>                 
         </div>
      </div>-->
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
         <div class="dashboard-stat yellow">
            <div class="visual">
               <i class="icon-external-link"></i>
            </div>
            <div class="details">
               <div class="number">{{$data['crawl_object']}}</div>
               <div class="desc">Crawl Object</div>
            </div>
            <a class="more" href="{{ url('/dashboard/object') }}">
            View all <i class="m-icon-swapright m-icon-white"></i>
            </a>                 
         </div>
      </div>
   </div>
   <!-- END DASHBOARD STATS -->
