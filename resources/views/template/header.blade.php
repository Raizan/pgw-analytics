   <meta charset="utf-8" />
   <title>Kemensos Social Media Analytic | Kementerian Sosial RI</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="Kemensos RI" name="author" />
   <meta name="MobileOptimized" content="320">
 
   <link href="{{asset('assets/assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css"/>
   
   <link href="{{asset('assets/assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" />
   <link href="{{asset('assets/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css')}}" rel="stylesheet" type="text/css"/>

   <link href="{{asset('assets/assets/css/style-metronic.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/style.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/style-responsive.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/pages/tasks.css')}}" rel="stylesheet" type="text/css"/>
   <link href="{{asset('assets/assets/css/themes/default.css')}}" rel="stylesheet" type="text/css" id="style_color"/>
   <link href="{{asset('assets/assets/css/custom.css')}}" rel="stylesheet" type="text/css"/>
   <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" type="text/css"/>
   <link rel="shortcut icon" href="favicon.ico" />
   <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
   <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
   <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
   <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
   <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
