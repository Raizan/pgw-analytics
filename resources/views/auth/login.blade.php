<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
@include('template.auth.partials.head')
 </head>
 
<body class="login">
   <!-- BEGIN LOGO -->
   <div class="logo">
      <!-- <img src="assets/img/logo_login.png" alt="" />  --><br><br><br><br><br><br><br>
   </div>
   <!-- END LOGO -->
   <!-- BEGIN LOGIN -->
   <div class="content" >
      <!-- BEGIN LOGIN FORM -->
      <div class="login-form"  >
         <h3 class="form-title">Login KSM - Analytic</h3>

         <div class="alert alert-error hide">
            <button class="close" data-dismiss="alert"></button>
            <span>Enter any username and password.</span>
         </div>
         <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="form-group">
               <!--ie8, ie9 does not support html5 placeholder, so we just show field title for tha
               t-->
               @if ($errors->has('email'))
                    <span class="help-block">
                        <strong style="color:red">{{ $errors->first('email') }}</strong>
                    </span>
               @endif
               <label class="control-label visible-ie8 visible-ie9">Email</label>
               <div class="input-icon">
                  <i class="icon-user"></i>
                  <input id="email" type="email" class="form-control placeholder-no-fix" name="email" value="{{ old('email') }}" placeholder="email">
               </div>
            </div>
            <div class="form-group">
               @if ($errors->has('password'))
                    <span class="help-block">
                        <strong style="color:red">{{ $errors->first('password') }}</strong>
                    </span>
               @endif
               <label class="control-label visible-ie8 visible-ie9">Password</label>
               <div class="input-icon">
                  <i class="icon-lock"></i>
                  <input id="password" type="password" class="form-control placeholder-no-fix" name="password" placeholder="password">
               </div>
            </div>
            <div class="form-actions">
               <label class="checkbox">
               <input type="checkbox" name="remember" value="1"/> Remember me
               </label>
               <button type="submit" class="btn blue pull-right">
               Login <i class="m-icon-swapright m-icon-white"></i>
               </button>            
            </div>
            <div class="forget-password">
               <h4>Forgot your password ? <a href="{{ url('/password/reset') }}"  id="forget-password">reset</a></h4>
                
            </div>
         </form> 
      </div>
   </div>
   <!-- END LOGIN -->
   <!-- BEGIN COPYRIGHT -->
   <div class="copyright">
      2016 &copy; Kemensos Social Media Analytic - Kementerian Sosial RI.
   </div>
    @include('template.auth.partials.footer')
 </body>
</html>
