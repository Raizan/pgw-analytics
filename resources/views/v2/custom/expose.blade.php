	<script type="text/javascript">
    var isStacked = true;
    $('#change-chart').click(function(){
      if (isStacked == false)
        isStacked = true;
      else if (isStacked == true)
        isStacked = false;

      refreshChart();
    });
    var tunnel = new Tunnel();
    var objects = [];
  	@foreach($data['objects'] as $o)
  	objects.push(['{{$o->name}}', '{{$o->logo}}', '{{$o->url}}']);
  	@endforeach
    var topics = [];
    var iter = 0;
    <?php
    $iter = 0;
    ?>
    @foreach($data['topics'] as $t)
      
      topics.push({name:'{{$t->keyword}}', url:'{{str_replace(" ", "%20", $t->keyword)}}', selected: @if($iter < 5) true @else false @endif, data: {}});
      <?php
      $iter++;
      ?>
    @endforeach
    <?php
  	$now = date('Y-m-d');
  	$startDate = strtotime('-365 day', strtotime($now));
  	$startDate = date('Y-m-d', $startDate);
  	$endDate = date('Y-m-d');
  	?>
    var dateStart = "{{$startDate}}";
  	var dateStop = "{{$endDate}}";
    validateData();

    setInterval(function(){
      refreshChart();
    }, 10000);
    function refreshChart(){
      $('#loader').hide();
      var dataToShow = [];
      dataToShow.push(['Topic', 'Influencer', 'Top Person', 'Sentiment Positive','Sentiment Negative','Sentiment Neutral']);
      var wordToShow = [];
      var wordTopicPlaceholder = 1;
      for (var i = 0; i < topics.length; i++){
        if (topics[i].selected == true){
          var influencerCount = 0;
          var top_personsCount = 0;
          var positiveCount = 0;
          var negativeCount = 0;
          var neutralCount = 0;
          if (typeof topics[i].data.influencers != 'undefined')
            influencerCount = topics[i].data.influencers.postCount;
          if (typeof topics[i].data.top_persons != 'undefined')
            top_personsCount = topics[i].data.top_persons.postCount;
          if (typeof topics[i].data.positive != 'undefined')
            positiveCount = topics[i].data.positive.postCount;
          if (typeof topics[i].data.negative != 'undefined')
            negativeCount = topics[i].data.negative.postCount;
          if (typeof topics[i].data.neutral != 'undefined')
            neutralCount = topics[i].data.neutral.postCount;
          dataToShow.push([topics[i].name, influencerCount, top_personsCount, positiveCount, negativeCount, neutralCount]);

          if (typeof topics[i].data.words != 'undefined'){
            var words = [];
            for (var j = 0; j<topics[i].data.words.length; j++){
              words.push(topics[i].data.words[j]);
            }
            wordToShow.push({id : i, words: words});
          }
        }
      }
      
      showData(dataToShow);
      console.log('dataToShow', dataToShow);
      showWordMapping('wordMapping',wordToShow);
    }
    function validateData(){
      for (var i = 0; i < topics.length; i++){
        if (topics[i].selected == true){
          if (typeof topics[i].data.influencers == 'undefined'){
            // Get Data
            getData(i, topics[i].url);
          } else {
            // Call Show Data
          }
        }
      }
    }
    function getData(index, topic){
      getInfluencer(topics[index].data, topic);
      getTopPerson(topics[index].data, topic);
      getSentiment(topics[index].data, topic);
      getWords(topics[index].data, topic);
    }
    function getInfluencer(dataDisplay,topic){
       var urls = '{{URL::to("/bridge/get/influencers?topic=")}}' + topic+'&date_start='+dateStart+'&date_stop='+dateStop;
       console.log('influencers',urls);
       $.get(urls ).done(function( response ) {
          // console.log('response influencers', response);
          response = jQuery.parseJSON(response);
          var count = 0;
          for (var i = 0; i < response.length; i++){
            count += response[i].count;
          }
          dataDisplay.influencers = {postCount: count, personCount: response.length};
       });
    }
    function getTopPerson(dataDisplay, topic){
        var urls = '{{URL::to("/bridge/get/top_persons")}}?topic='+topic+'&date_start='+dateStart+'&date_stop='+dateStop;
        console.log('TopPerson',urls);
        $.get(urls).done(function(response){
          response = jQuery.parseJSON(response);
          // console.log('response TopPerson', response);

          var persons = [];
          var selectedItem = [];

          for (var i = 0; i< response.length; i++){
             for (var j = 0; j < response[i].persons.length; j++){
                personValidation(response[i].persons[j]);
             }
          }
          
          var contents = [];
          var postCount = 0;
          for (var i = 0; i < response.length; i++){
             var content = [];
             content.push(response[i].date);
             for (var j = 0; j < persons.length; j++){
                var isExist = -1;
                for (var k = 0; k < response[i].persons.length; k++){
                   if (response[i].persons[k].screen_name == persons[j].screen_name)
                      isExist = k;
                }
                if (isExist == -1)
                   content.push(0);
                else {
                  content.push(response[i].persons[isExist].post_count);
                  postCount++;
                }
             }
             contents.push(content);
          }
          dataDisplay.top_persons = {postCount: postCount, personCount: persons.length};
          // console.log('data TopPerson', dataDisplay);
          function personValidation(name){
            var isExist = 0;
            for (var i = 0; i < persons.length; i++){
               if (persons[i].screen_name == name.screen_name)
               isExist++;
            }
            if (isExist == 0)
               persons.push(name);
         }
       });
    }
    function getSentiment(dataDisplay, topic){
      var urls = '{{URL::to("/bridge/get/sentiment?topic=")}}'+topic+'&date_start='+dateStart+'&date_stop='+dateStop;
      console.log('sentiment', urls);
      $.get(urls).done(function( response ) {
        // console.log('response sentiment', response);
        response = jQuery.parseJSON(response);
        var positive = 0;
        var negative = 0;
        var neutral = 0;
        for(var i = 0; i < response.length; i++){
          positive += response[i].sentiment.positive;
          negative += response[i].sentiment.negative;
          neutral += response[i].sentiment.neutral;
        }
        dataDisplay.positive = {postCount: positive};
        dataDisplay.negative = {postCount: negative};
        dataDisplay.neutral = {postCount: neutral};
      });
    }
    function getWords(dataDisplay, topic){
      $.get("{{URL::to('bridge/get/related_words?topic=')}}"+topic).done(function(response){
          response = jQuery.parseJSON(response);
          var words = [];
          var barData = [];
          var barLength = 5;
          var total = 0;
          for (var i = 0; i < response.length; i++){
          	total += response[i].score;
          }
          console.log('total', total);
          if (response.length < 5)
            barLength = response.length
          for (var i = 0; i<barLength; i++){
             words.push({text: response[i].word, weight: response[i].score, percentage: response[i].score / total * 100});
             barData.push([response[i].word, response[i].score, response[i].score / total * 100]);
          }
          dataDisplay.words = barData;
       });
    }
    function showData(dataToShow){
    	google.charts.load("current", {packages:["corechart"]});
	    google.charts.setOnLoadCallback(drawChart);
	    function drawChart() {
	      var data = google.visualization.arrayToDataTable(dataToShow);

	      var view = new google.visualization.DataView(data);
	      
	      var options = {
	        legend: { position: "bottom" },
	        isStacked: isStacked
	      };
	      var chart = new google.visualization.BarChart(document.getElementById("barchart_material"));
        google.visualization.events.addListener(chart, 'select', function(){
          console.log(chart.getSelection()[0]);
          getPosts(topics[chart.getSelection()[0].row].url, dataToShow[0][chart.getSelection()[0].column]);
        });
	      chart.draw(view, options);
	  }

    }
    function getPosts(topic, graph){
    	var sentiment = false;
    	var postsShowGet = new PostsShowGet;
    	if (graph == 'sentiment positive')
    		sentiment = 1;
    	else if (graph == 'sentiment negative')
    		sentiment == -1;
    	else if (graph == 'sentiment neutral')
    		sentiment = 0;
    	if (sentiment != false)
	    	showAdvancedModal({
	    		topic: validateName(topic),
	    		startDate: 'global',
	    		endDate: 'global',
	    		sentiment: sentiment
	    	});
	    else 
	    	showAdvancedModal({
	    		topic: validateName(topic),
	    		startDate: 'global',
	    		endDate: 'global'
	    	});
    }

    function validateName(name){
  		var names = [];
  		names = name.split(' ');
	  	if (names.length < 2)
  			return name;
  		var name = '';
  		for (var i = 0; i < names.length; i++){
	  		name += names[i];
  			if (i + 1 < names.length)
  				name += '%20';
  		}
  		return name;
  	}
    function requestURLmapper(request){
    	var urls = [];
    	var nFlag = false;
    	for (var k in request){
      		switch(k){
        		case 'topic':
          		if (request[k] == 'global')
            		urls.push('topic='+topic);
          		else if (typeof request[k] != 'undefined')
            		urls.push('topic='+request[k]);
          break;
          case 'startDate':
            if (request[k] == 'global')
              urls.push('date_start='+dateStart);
            else if (typeof request[k] != 'undefined')
              urls.push('date_start='+request[k]);
            break;
          case 'endDate' : 
            if (request[k] == 'global')
            urls.push('date_stop='+dateStop);
            else if (typeof request[k] != 'undefined')
              urls.push('date_stop='+request[k]);
            break;
          case 'date':
	        	urls.push('date='+request[k]);
	    	    break
          case 'person' : 
            if(typeof request[k] != 'undefined')
              urls.push('person='+validateName(request[k]));
            break;
          case 'name' : 
            if(typeof request[k] != 'undefined')
              urls.push('name='+validateName(request[k]));
            break;
          case 'n' :
            if (typeof request[k] != 'undefined'){
              nFlag = true;
              urls.push('n='+request[k]);
            }
            break;
          case 'sentiment':
            if (typeof request[k] != 'undefined'){
              sentimentNumber = 0;
              switch(request[k]){
                case 'positive':
                  sentimentNumber = 1;
                  break;
                case 'negative' :
                  sentimentNumber = -1;
                  break;
                default:
                  sentimentNumber = 0;
                  break;
              }
              urls.push('sentiment='+sentimentNumber);
            }
          case 'q':
            if (typeof request[k] != 'undefined')
              urls.push('q='+request[k]);
          default:
            break;
        }
      }
      if (!nFlag)
        urls.push('n=50');
      var url = '';
      for(var i = 0; i < urls.length; i++){
        if (i == 0)
          url += '?';
        url += urls[i];
        if (i+1 < urls.length)
          url += '&';
      }
      return url;
    }
    
    function showAdvancedModal(request, urlPost = ''){
      var url = '{{URL::to("bridge/get/posts")}}' +requestURLmapper(request);
      if (urlPost != '')
        url = urlPost + requestURLmapper(request);
      $('#advancedModal').modal('toggle');

      var twitterNotification = $('#twitterPostLoader');
      twitterNotification.show();
      var facebookNotification = $('#facebookPostLoader');
      facebookNotification.show();
      var portalNotification = $('#portalPostLoader');
      portalNotification.show();

      tunnel.beritas = new Berita($("#portalPostContent"), objects, '{{URL::to("userFile")}}');
    	tunnel.facebooks = new Facebook($("#facebookPostContent"));
    	tunnel.twitters = new Twitter($("#twitterPostContent"));

    	var postSource = ['tweet', 'fb', 'berita'];
    	for (var i = 0; i< postSource.length; i++){
    		var type = postSource[i];
    		console.log(url+'&type='+type);
    		$.get(url+'&type='+type).done(function(response){
    			response = jQuery.parseJSON(response);
    			dataDisplay = response;
    			for(var j = 0; j<response.length; j++){
    				tunnel.factory(response[j]);
    			}
    			$(tunnel.facebooks.container).children().remove();
    			$(tunnel.twitters.container).children().remove();
    			$(tunnel.beritas.container).children().remove();
    			tunnel.clearView();
    			tunnel.show();
    			twitterNotification.hide();
    			facebookNotification.hide();
    			portalNotification.hide();
    		});
    	}
    }

    function showWordMapping(topicPlaceholder, data){
      var wordsToShow = [];
      console.log(data);
      wordsToShow.push(['Related Words', 'Post Count', { role: 'style' }, { role: 'anotation'}]);
      var tempData = data;
      var colors = ['blue', 'cyan', 'green', 'orange', 'yellow'];
      var wordToTopicRelation = [];
      var iteration = 0;
      for (var i = 0; i < data.length; i++){
        for (var j = 0; j < data[i].words.length; j++){
        	var scoreValue = Math.round(data[i].words[j][2] * 10) /10;
        	wordsToShow.push([data[i].words[j][0] + '(' + scoreValue + '%)', data[i].words[j][1], colors[i], data[i].words[j][0]]);
          wordToTopicRelation.push([iteration, i]);
          iteration++;
        }
        $('#lw_'+(i+1)).text(topics[data[i].id].name);
      }
      
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        console.log(wordsToShow);
        var data = google.visualization.arrayToDataTable(wordsToShow);

        var view = new google.visualization.DataView(data);
        var chart = new google.visualization.BarChart(document.getElementById(topicPlaceholder));
        google.visualization.events.addListener(chart, 'select', function(){
          // var wordsToShowI = wordToTopicRelation[chart.getSelection()[0].row][1];
          // console.log(topics[tempData[wordsToShowI].id].url, wordsToShow[chart.getSelection()[0].row][3]);
          // console.log(wordsToShow[chart.getSelection()[0].row+1][3], chart.getSelection()[0].row, wordsToShow);
          showRelatedData(topics[tempData[wordToTopicRelation[chart.getSelection()[0].row][1]].id].url, wordsToShow[chart.getSelection()[0].row+1][3])
        });
        chart.draw(view, {legend: {position: 'none'}});
      }
    }

    function showRelatedData(topic, q){
      showAdvancedModal({
        topic: topic,
        startDate: 'global',
        endDate: 'global',
        q: q
      }, '{{URL::to("bridge/get/post_range")}}');
    }
    </script>

