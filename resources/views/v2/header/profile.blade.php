<div class="row-fluid">
   <div class="span12">
      {!! Form::open(array('route' => 'v2.user.store', 'method' => 'patch','files' => false, 'class' => 'form-horizontal form-bordered form-row-stripped')) !!}
         <div class="form-body">
            <div class="control-group">
               {!! Form::label('name','Name : ', ['class' => 'control-label']) !!}
               <div class="controls">
                  {!! Form::text('name',$data['content']->name, ['clas' => 'span12', 'required' => 'required', 'placeholder' => 'ex: John Doe', 'disabled' => 'disabled']) !!}
               </div>
            </div>
            <div class="control-group">
               {!! Form::label('email','Email : ', ['class' => 'control-label']) !!}
               <div class="controls">
                  {!! Form::text('email',$data['content']->email, ['clas' => 'span12', 'required' => 'required', 'placeholder' => 'ex: john.doe@mail.com', 'disabled' => 'disabled']) !!}
               </div>
            </div>
         </div>
      {!! Form::close()!!}
   </div>
   <div class="span12" style="margin-bottom: 15px">
     <div><h4>Facebook Media:</h4></div>
     <center>
         @if ($data['fb_connected'] == false)
            <a href="{{ url('/facebook/connect') }}" class="btn btn-info"><i class="icon-facebook"></i>  Connect to Facebook</a>
         @else
            @if ($data['fb_expired'] == true)
               <a href="{{ url('/facebook/connect') }}" class="btn btn-info"><i class="icon-facebook"></i>  Connect to Facebook</a>
            @else
               <a class="btn btn-success"><i class="icon-facebook"></i>  Connected to Facebook</a>
            @endif
         @endif
     </center>
   </div>
   @if (Auth::user()->privilege_id == 1)
   <div class="span12">
     <div><h4>Twitter Token:</h4></div>
      {!! Form::open(array('route' => 'v2.twitter.update', 'method' => 'post', 'class' => 'form-horizontal form-bordered form-row-stripped')) !!}
         <div class="form-body">
            <div class="control-group">
               {!! Form::label('access_token','Access Token: ', ['class' => 'control-label']) !!}
               <div class="controls">
               @if ($errors->has('access_token'))
                     <span class="help-block">
                         <strong style="color:red">{{ $errors->first('access_token') }}</strong>
                     </span>
               @endif
                  {!! Form::text('access_token', $data['twitter']->access_token, ['clas' => 'span12', 'required' => 'required']) !!}
               </div>
            </div>
            <div class="control-group">
               {!! Form::label('access_token_secret','Access Token Secret: ', ['class' => 'control-label']) !!}
               <div class="controls">
               @if ($errors->has('access_token_secret'))
                     <span class="help-block">
                         <strong style="color:red">{{ $errors->first('access_token_secret') }}</strong>
                     </span>
               @endif
                  {!! Form::text('access_token_secret', $data['twitter']->access_token_secret, ['clas' => 'span12', 'required' => 'required']) !!}
               </div>
            </div>
            <div class="control-group">
               {!! Form::label('consumer_key','Consumer Key: ', ['class' => 'control-label']) !!}
               <div class="controls">
               @if ($errors->has('consumer_key'))
                     <span class="help-block">
                         <strong style="color:red">{{ $errors->first('consumer_key') }}</strong>
                     </span>
               @endif
                  {!! Form::text('consumer_key', $data['twitter']->consumer_key, ['clas' => 'span12', 'required' => 'required']) !!}
               </div>
            </div>
            <div class="control-group">
               {!! Form::label('consumer_secret','Consumer Secret: ', ['class' => 'control-label']) !!}
               <div class="controls">
               @if ($errors->has('consumer_secret'))
                     <span class="help-block">
                         <strong style="color:red">{{ $errors->first('consumer_secret') }}</strong>
                     </span>
               @endif
                  {!! Form::text('consumer_secret', $data['twitter']->consumer_secret, ['clas' => 'span12', 'required' => 'required']) !!}
               </div>
            </div>
            <div class="row-fluid">
               <center>
                  {{ Form::submit('Save', array('class' => 'btn blue')) }}
               </center>
            </div>
         </div>
      {!! Form::close()!!}
   </div>
   @endif
</div>