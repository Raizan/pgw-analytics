   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

 
       <div class="col-md-12 col-sm-12">
         <div class="portlet box red">
            
            <div class="portlet-body"> 
               <table class="table table-striped table-bordered table-hover" id="sample_1" >
                  <thead>
                     <tr>
                         <th>Name</th>
                        <th>Action</th>
                        <th>Table</th>
                        <th>Timestamp</th>
                        <th>References</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     $iter = 0;
                     ?>
                     @foreach($data['content'] as $print)
                     <tr class="odd gradeX">
                         <td>{{$print->user->name}} </td>
                        <td>{{$print->action}} </td>
                        <td>{{$print->table}} </td>
                        <td>{{$print->created_at}}</td>
                        <td>{{$print->reference}}</td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
 <style type="text/css">
 .dataTables_info, .bInfo, .dataTables_length{ display: none; }
 </style>
   <script type="text/javascript">
      $("#sample_1").DataTable();   
   </script>
   <script type="text/javascript">
   var table_sample2 = $("#sample_2").DataTable({ "aaData": orgContent,
            "bLengthChange": true //used to hide the property  });

   var tableView = $("table[table_view='post_view'] > tbody");
   var data;
   var dataToGet = [];
   var postType = ['tweet', 'fb', 'berita'];
   @foreach($data['content'] as $c)
      for (var i = 0; i < postType.length; i++){
         getDatas('{{$c->keyword}}', postType[i]);
      }
      dataToGet.push('{{$c->keyword}}');
   @endforeach
   function getDatas(topic, type){
      var getURL = "{{URL::to('/dump/recent_post')}}"+'?topic='+topic+'&type='+type;
      console.log(getURL);
      $.get(getURL).done(function(response) {
         data = response;
         var portalData = [];
         for (var i = 0; i < response.length - 1; i++) {
            if (response[i].source == 'twitter.com'){
               prependTwitter(response[i]);
            }
            else if (response[i].source == 'facebook.com'){
               prependFacebook(response[i]);
            } else{
               portalData.push([portalData.length+1, response[i].topic, response[i].timestamp, response[i].url, response[i].source]);
               prependPortal(portalData.length, response[i].topic, response[i].timestamp, response[i].url, response[i].source);
            }
         }
      });

      function prependTwitter(data) {
         if (data.url != '')
         {
            $.ajax({
              method: "GET",
              url: 'https://publish.twitter.com/oembed?url='+data.url,
              dataType: 'jsonp',
              success: function(response){
                  $("#twitter_log").prepend(response.html);
              }
            });
         }
      }
      function prependFacebook(data) {
         console.log(data);
         if (data.url != '')
         {
            $("#facebook_log").prepend('<li><div class="facebook-person"><a href="'+data.url+'" target="_blank">'+ data.person +'</a></div><div class="facebook-time">'+data.timestamp+'</div><div class="facebook-content">'+ data.post +'</div></li>');
         }
      }
      function prependPortal(no, topic, timestamp, url, source){
         var counter = '0';
         table_sample2.row.add([no, topic, timestamp, '<a target="_blank" href="'+url+'">'+url+'</a>' , source]).draw(false);
      }
   }
   </script> 