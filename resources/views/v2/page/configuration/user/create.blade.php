<div class="row-fluid">
	{!! Form::open(array('route' => 'v2.user.store', 'method' => 'post','files' => true, 'class' => 'form-horizontal no-margin')) !!}
		<div class="control-group">
			{!! Form::label('name','Name : ', ['class' => 'control-label']) !!}
			<div class="controls">
            {!! Form::text('name',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: John Doe, Peter Schumacker']) !!}
			</div>
		</div>
		<div class="control-group">
         {!! Form::label('email','Email : ', ['class' => 'control-label col-md-3']) !!}
         <div class="controls">
            {!! Form::text('email',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: john.doe@mmail.com']) !!}
         </div>
      </div>
      <div class="control-group">
         {!! Form::label('password','Password : ', ['class' => 'control-label col-md-3']) !!}
         <div class="controls">
            {!! Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Min 8 Character']) !!}
         </div>
      </div>
      <div class="control-group">
         {!! Form::label('privilege','Privilege : ', ['class' => 'control-label col-md-3']) !!}
         <div class="controls">
            @foreach($privileges as $p)
               <label class="span12">
                  {!! Form::radio('privilege', $p['id'])!!}
                  {{ ucfirst($p->name) }}
               </label>
            @endforeach
         </div>
      </div>
      <div class="">
         <div class="row">
            <div class="span12">
            	<center>
            		{!! Form::submit('Tambahkan User',['class' => 'btn btn-success']) !!}
            	</center>
            </div>
         </div>
      </div>
	{!! Form::close() !!}
</div>