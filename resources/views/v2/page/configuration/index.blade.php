@extends('v2.template.index')
@section('custom-head')
 <link href="{{asset('v2/css/pace_orange.css')}}" rel="stylesheet">
  <script src="{{asset('v2/js/pace.min.js')}}"></script>
  <style type="text/css">
  body > :not(.pace),body:before,body:after {
  -webkit-transition:opacity .4s ease-in-out;
  -moz-transition:opacity .4s ease-in-out;
  -o-transition:opacity .4s ease-in-out;
  -ms-transition:opacity .4s ease-in-out;
  transition:opacity .4s ease-in-out
}

body:not(.pace-done) > :not(.pace),body:not(.pace-done):before,body:not(.pace-done):after {
  opacity:0.3
}
  </style>
 <script>
   paceOptions = {
  elements: {
    selectors: ['.loader_pre']
    eventLag: false, // disabled
  }
}
  </script>
   <script>
    function load(time){
      var x = new XMLHttpRequest()
      x.open('GET', "{{ URL::to('/v2/help') }}" + time, true);
      x.send();
    };

    load(20);
    load(100);
    load(500);
    load(2000);
    load(3000);

    setTimeout(function(){
      Pace.ignore(function(){
        load(3100);
      });
    }, 4000);

    Pace.on('hide', function(){
      console.log('done');
    });
  </script>

 <script>

    range.addEventListener('input', function(){
      document.querySelector('.pace').classList.remove('pace-inactive');
      document.querySelector('.pace').classList.add('pace-active');

      document.querySelector('.pace-progress').setAttribute('data-progress-text', range.value + '%');
      document.querySelector('.pace-progress').setAttribute('style', '-webkit-transform: translate3d(' + range.value + '%, 0px, 0px)');
    });
  </script>
  @endsection
@section('main-content')
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-header">
        <div class="title">
          <span class="fs1" aria-hidden="true" data-icon="&#xe0b6;"></span> Configuration
        </div>
      </div>
      <div class="widget-body">
        <ul class="nav nav-tabs no-margin myTabBeauty">
          <li class="active">
            <a data-toggle="tab" href="#object">
              News Portal Object Config
            </a>
          </li>
          <li class="">
            <a data-toggle="tab" href="#social">
              Social Object Config
            </a>
          </li>
          <li class="">
            <a data-toggle="tab" href="#user">
              User Management
            </a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div id="object" class="tab-pane fade active in">
            <button class="pull-right btn btn-success standardModalButton" href="#standardModal" role="button" data-toggle="modal" url="{{route('v2.object.create')}}">+ Add</button>
            <table class="table table-condensed table-striped table-bordered table-hover no-margin dataTable" id="table_1">
              <thead>
                <tr>
                  <th style="width:10%">
                    No.
                  </th>
                  <th style="width:15%">
                    Logo
                  </th>
                  <th style="width:15%">
                    Name
                  </th>
                  <th style="width:45%" class="hidden-phone">
                    URL
                  </th>
                  <th style="width:5%" class="hidden-phone">
                    Location
                  </th>
                  <th style="width:10%" class="hidden-phone">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 0;
                ?>
                @foreach($data['objects'] as $o)
                  <tr>
                    <td>{{++$no}}</td>
                    <td style="width: 60px">
                      <center>
                        @if($o->logo != '')
                          <img src="{{asset('userFile/'.$o->logo)}}" style="max-width: 50px; max-height: 50px">
                        @endif
                      </center>
                    </td>
                    <td>{{$o->name}}</td>
                    <td>{{$o->url}}</td>
                    <td>{{$o->location}}</td>
                    <td>
                    	@if($o->id != 1 && $o->id != 2)
                    	<button class="btn btn-danger standardModalButton" action='confirmation' href="#standardModal" role="button" data-toggle="modal" url="{{route('v2.object.delete', $o->id)}}">x Delete</button>
                    	@else
                    	<button class="btn btn-default" disabled="">x Delete</button>
                    	@endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div id="social" class="tab-pane fade">
            <button class="pull-right btn btn-success standardModalButton" href="#standardModal" role="button" data-toggle="modal" url="{{route('v2.social.create')}}">+ Add</button>
            <table class="table table-condensed table-striped table-bordered table-hover no-margin dataTable" id="table_1">
              <thead>
                <tr>
                  <th style="width:10%">
                    No.
                  </th>
                  <th style="width:15%">
                    Facebook ID
                  </th>
                  <th style="width:45%" class="hidden-phone">
                    Facebook Name
                  </th>
                  <th style="width:10%" class="hidden-phone">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 0;
                ?>
                @foreach($data['social_objects'] as $o)
                  <tr>
                    <td>{{++$no}}</td>
                    <td>{{$o->id}}</td>
                    <td>{{$o->name}}</td>
                    <td>
                    	<button class="btn btn-danger standardModalButton" action='confirmation' href="#standardModal" role="button" data-toggle="modal" url="{{route('v2.social.delete', $o->id)}}">x Delete</button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <div id="user" class="tab-pane fade">
            <button class="pull-right btn btn-success standardModalButton" href="#standardModal" role="button" data-toggle="modal" url="{{route('v2.user.create')}}">+ Add</button>
            <table class="table table-condensed table-striped table-bordered table-hover no-margin dataTable" id="table_1">
              <thead>
                <tr>
                  <th style="width:10%">
                    No.
                  </th>
                  <th style="width:15%">
                    Name
                  </th>
                  <th style="width:45%" class="hidden-phone">
                    Email
                  </th>
                  <th style="width:5%" class="hidden-phone">
                    Privilege
                  </th>
                  <th style="width:10%" class="hidden-phone">
                    Action
                  </th>
                </tr>
              </thead>
              <tbody>
                <?php
                $no = 0;
                ?>
                @foreach($data['users'] as $o)
                  <tr>
                    <td>{{++$no}}</td>
                    <td>{{$o->name}}</td>
                    <td>{{$o->email}}</td>
                    <td>{{$o->privilege->name}}</td>
                    <td>
                      @if ($o->id != 2)
                    	<button class="btn btn-danger standardModalButton" action='confirmation' href="#standardModal" role="button" data-toggle="modal" url="{{route('v2.user.delete', $o->id)}}">x Delete</button>
                      @endif
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom-footer')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
  $('.dataTable').DataTable();
</script>
@endsection