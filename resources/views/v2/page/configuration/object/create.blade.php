<div class="row-fluid">
	{!! Form::open(array('route' => 'v2.object.store', 'method' => 'post','files' => true, 'class' => 'form-horizontal no-margin')) !!}
		<div class="control-group">
			{!! Form::label('name','Name : ', ['class' => 'control-label']) !!}
			<div class="controls">
               {!! Form::text('name',null, ['class' => 'span12', 'required' => 'required', 'placeholder' => 'ex: kompas media, detik, cnn indonesia']) !!}
			</div>
		</div>
		<div class="control-group">
         {!! Form::label('url','URL : ', ['class' => 'control-label']) !!}
         <div class="controls">
            {!! Form::text('url',null, ['class' => 'span12', 'required' => 'required', 'placeholder' => 'ex: detik.com, cnnindonesia.com']) !!}
         </div>
      </div>
      <div class="control-group">
         {!! Form::label('location','Location : ', ['class' => 'control-label']) !!}
         <div class="controls">
            {{ Form::select('location', $city) }}
         </div>
      </div>
      <div class="control-group">
         {!! Form::label('photo','Photo : ', ['class' => 'control-label']) !!}
         <div class="controls">
            {!! Form::file('photo')!!}
         </div>
      </div>
      <div class="">
         <div class="row">
            <div class="span12">
            	<center>
            		{!! Form::submit('Tambahkan Object',['class' => 'btn btn-success']) !!}
            	</center>
            </div>
         </div>
      </div>
	{!! Form::close() !!}
</div>