@extends('v2.template.index')
@section('custom-head')
 <link href="{{asset('v2/css/pace_orange.css')}}" rel="stylesheet">
  <script src="{{asset('v2/js/pace.min.js')}}"></script>
  <style type="text/css">
  body > :not(.pace),body:before,body:after {
  -webkit-transition:opacity .4s ease-in-out;
  -moz-transition:opacity .4s ease-in-out;
  -o-transition:opacity .4s ease-in-out;
  -ms-transition:opacity .4s ease-in-out;
  transition:opacity .4s ease-in-out
}

body:not(.pace-done) > :not(.pace),body:not(.pace-done):before,body:not(.pace-done):after {
  opacity:0.3
}
  </style>
 <script>
   paceOptions = {
  elements: {
    selectors: ['.loader_pre']
  }
}
  </script>
   <script>
    function load(time){
      var x = new XMLHttpRequest()
      x.open('GET', "{{ URL::to('/v2/help') }}" + time, true);
      x.send();
    };

    load(20);
    load(100);
    load(500);
    load(2000);
    load(3000);

    setTimeout(function(){
      Pace.ignore(function(){
        load(3100);
      });
    }, 4000);

    Pace.on('hide', function(){
      console.log('done');
    });
  </script>

 <script>

    range.addEventListener('input', function(){
      document.querySelector('.pace').classList.remove('pace-inactive');
      document.querySelector('.pace').classList.add('pace-active');

      document.querySelector('.pace-progress').setAttribute('data-progress-text', range.value + '%');
      document.querySelector('.pace-progress').setAttribute('style', '-webkit-transform: translate3d(' + range.value + '%, 0px, 0px)');
    });
  </script>
  @endsection
@section('main-content')

<button href="#advancedModal" role="button" data-toggle="modal" class="btn btn-large btn-info btn-block" style="visibility: hidden;" id="advancedModalButton">Advanced Modal</button>
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-header">
        <div class="title">
          <span class="fs1" aria-hidden="true" data-icon="&#xe0b6;"></span> Hasil Pencarian Topik : <b style="color:red;padding-right:10px">{{$topic}}</b>
          <span style=""></span><i class="fa fa-calendar"></i> Pencarian dimulai tgl :<b style="color:red;padding-right:10px">{{$current->created_at}}</b>
          <span style=""></span><i class="fa fa-calculator"></i> Crawled Total :<b style="color:red;padding-right:10px" id="crawlTotal">~</b>
          <span style=""></span><i class="fa fa-external-link"></i> Object Crawl :<b style="color:red;padding-right:10px"></b>
        </div>
      </div>
      <div class="widget-body">
        <div class="row-fluid">
          <div class="pull-left">
            <span><input class="btn blue btn-sm download" type="button" value="Refresh Page" onClick="window.location.reload()"></span>
          </div>
          <div class="pull-right">
            <span>
              <i class="fa fa-calendar"></i> Date range: 
              <input type="text" name="daterange" value="01/01/2016 - 01/31/2016"/>
            </span>
            <a href="{{URL::to('/download/influencers/'.$current->id.'/csv')}}" class="btn blue btn-sm download" target="_blank" style="margin-top: -5px"><i class="icon-download"></i> Download as CSV</a>
            <a href="{{URL::to('/download/influencers/'.$current->id.'/pdf')}}" class="btn blue btn-sm download" target="_blank" style="margin-top: -5px"><i class="icon-download"></i> Download as PDF</a>
            <a href="#" class="btn blue btn-sm download" target="_blank" style="margin-top: -5px"><i class="icon-download"></i> Download as PNG</a>

          </div>
        </div>
        <div class="row-fluid" >
          <ul class="nav nav-tabs no-margin myTabBeauty">
            <li class="active">
              <a data-toggle="tab" href="#demography">
                Demography
              </a>
            </li>
            <li class="">
              <a data-toggle="tab" href="#influencer">
                Influencers
              </a>
            </li>
            <li class="">
              <a data-toggle="tab" href="#toppersons">
                Top Persons
              </a>
            </li>
            <li class="">
              <a data-toggle="tab" href="#sentiment">
                Sentiment
              </a>
            </li>
            <li class="">
              <a data-toggle="tab" href="#mapping">
                Related Words
              </a>
            </li>
          </ul>
          <div class="tab-content" id="compareContent">
            <div id="demography" class="tab-pane fade active in">
                <div id="gmaps" style="width:100%; height: 500px; height: 600px">
                  <center>
                    <img src="{{asset('v2/img/loading-red.gif')}}">
                  </center>

                </div>
            </div>
            <div id="influencer" class="tab-pane fade active in">
                <div id="influencer_1" style="width:100%; height: 600px">
                  <center>
                    <img src="{{asset('v2/img/loading-red.gif')}}">
                    <br>
                    If in 30 seconds this section is still showing this message, that means no data available or core program is not functioning well. Please contact your administrator if this happens. 
                  </center>
                </div>
            </div>
            <div id="toppersons" class="tab-pane fade active in">
                <div id="toppersons_1" style="width:100%; height: 600px">
                  <center>
                    <img src="{{asset('v2/img/loading-red.gif')}}">
                    <br>
                    If in 30 seconds this section is still showing this message, that means no data available or core program is not functioning well. Please contact your administrator if this happens. 
                  </center>
                </div>
            </div>
            <div id="sentiment" class="tab-pane fade active in">
                <div id="sentiment_1" style="width:100%; height: 600px">
                  <center>
                    <img src="{{asset('v2/img/loading-red.gif')}}">
                    <br>
                    If in 30 seconds this section is still showing this message, that means no data available or core program is not functioning well. Please contact your administrator if this happens. 
                  </center>
                </div>
            </div>
            <div id="mapping" class="tab-pane fade active in">
                <div id="mapping_1" style="width:100%; height: 600px">
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom-head')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link href="{{asset('assets/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<style type="text/css">
  .calendar.left, .calendar.right{
    border: none;
  }
</style>
@endsection
@section('custom-footer')
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{asset('assets/assets/scripts/tunnel.js')}}"></script>
<script type="text/javascript">
	var map;
	<?php
	$now = date('Y-m-d');
	$startDate = strtotime('-30 day', strtotime($now));
	$startDate = date('Y-m-d', $startDate);
	$endDate = date('Y-m-d');
	?>
	var objects = [];
	@foreach($objects as $o)
	objects.push(['{{$o->name}}', '{{$o->logo}}', '{{$o->url}}']);
	@endforeach
	var dateStart = "{{$startDate}}";
	var dateStop = "{{$endDate}}";
	var dataDisplay_1 = [];
	var topic_1 = '{{str_replace(' ', "%20", $topic)}}';
	var topic = topic_1;
	var tunnel = new Tunnel();
	analyzeJS();

	$('input[name="daterange"]').val(moment(dateStart).format('MM-DD-YYYY') + ' - ' + moment(dateStop).format('MM-DD-YYYY'));
	$('input[name="daterange"]').daterangepicker({
	  'startDate' : moment(dateStart).format('MM-DD-YYYY'),
	  'endDate' : moment(dateStop).format('MM-DD-YYYY'),
	}, function(start, end, label) {
	dateStart = start.format('YYYY-MM-DD');
	dateStop = end.format('YYYY-MM-DD');
	analyzeJS();
	});
	function analyzeJS(){
	  var influencer_1 = getData(1,topic_1);
	  function getData(dataDisplay, topic){
	     getInfluencer(dataDisplay,topic)
	     getTopPerson(dataDisplay, topic);
	     getDemography(dataDisplay, topic);
	     getSentiment(dataDisplay, topic);
	     getKeywordMapping(dataDisplay, topic);
	  }
	  function setData_1(type, data){
	     dataDisplay_1.push({type: type, data: data});
	     if (dataDisplay_1.length >= 5)
	        showData(dataDisplay_1, 1);
	  }
	  function setRawData_1(type, raw){
	    for (var i = 0; i<dataDisplay_1.length; i++){
	      if (dataDisplay_1[i].type == type)
	        dataDisplay_1[i].raw = raw;
	    }
	  }
	  function setData_2(type, data){
	     dataDisplay_2.push({type: type, data: data});
	     if (dataDisplay_2.length >= 5)
	        showData(dataDisplay_2, 2);
	  }

	  function getInfluencer(dataDisplay,topic){
	     var urls = '{{URL::to("/bridge/get/influencers?topic=")}}' + topic+'&date_start='+dateStart+'&date_stop='+dateStop;
	     console.log('influencer',urls);
	     $.get(urls ).done(function( response ) {
	        response = jQuery.parseJSON(response);
	        influencerData = response;
	        var pieData = [];
	        for (var i = 0; i< response.length; i++){
	           pieData.push([influencerData[i].display_name, influencerData[i].count]);
	        }            
	        if (dataDisplay == 1){
	          setData_1('influencers', pieData);
	          setRawData_1('influencers', influencerData);
	        }
	     });
	  }

	  function getTopPerson(dataDisplay, topic){
	     var urls = '{{URL::to("/bridge/get/top_persons")}}?n=5&topic='+topic+'&date_start='+dateStart+'&date_stop='+dateStop;
	     console.log('TopPerson',urls);
	     $.get(urls).done(function(response){
	        response = jQuery.parseJSON(response);
	        if (dataDisplay == 1)
	           setData_1('top_persons', response);
	        else if (dataDisplay == 2)
	           setData_2('top_persons', response);
	     });
	  }

	  function getDemography(dataDisplay, topic){
	     var urls = "{{URL::to('/bridge/get/demography?topic=')}}"+topic+'&date_start='+dateStart+'&date_stop='+dateStop;
	     console.log('demography',urls);
	     $.get(urls).done(function(response){
	         geoLocation = jQuery.parseJSON(response);
	         if (dataDisplay == 1)
	           setData_1('demography', geoLocation);
	        else if (dataDisplay == 2)
	           setData_2('demography', geoLocation);
	      });
	  }

	  function getSentiment(dataDisplay, topic){
	     var urls = '{{URL::to("/bridge/get/sentiment?topic=")}}'+topic+'&date_start='+dateStart+'&date_stop='+dateStop;
	     console.log('sentiment',urls);
	     $.get(urls).done(function( response ) {
	        response = jQuery.parseJSON(response);
	        if (dataDisplay == 1)
	           setData_1('sentiment', response);
	        else if (dataDisplay == 2)
	           setData_2('sentiment', response);
	     });
	  }

	  function getKeywordMapping(dataDisplay, topic){
	     $.get("{{URL::to('bridge/get/related_words?topic=')}}"+topic).done(function(response){
	        response = jQuery.parseJSON(response);
	        var words = [];
	        var barData = [];
	        for (var i = 0; i<response.length; i++){
	           words.push({text: response[i].word, weight: response[i].score})+'&date_start='+dateStart+'&date_stop='+dateStop;
	           barData.push([response[i].word, response[i].score]);
	        }
	        if (dataDisplay == 1)
	           setData_1('mapping', barData);
	        else if (dataDisplay == 2)
	           setData_2('mapping', barData);
	     });
	  }
	  function showData(data, dataDisplay){
	     for (var i = 0; i < data.length; i++){
	        switch(data[i].type){
	           case 'influencers':
	              showInfluencers(data[i],dataDisplay);
	              break;
	           case 'top_persons':
	              showTopPerson(data[i],dataDisplay);
	              break;
	           case 'demography':
	              showDemography(data[i],dataDisplay);
	              break;
	           case 'sentiment':
	              showSentiment(data[i],dataDisplay);
	              break;
	           case 'mapping':
	              showKeywordMapping(data[i],dataDisplay);
	              break;
	           default: 
	              break;
	        }
	     }
	  }

	  function showInfluencers(pieData, dataDisplay){
	     google.charts.load('current', {'packages':['corechart']});
	     google.charts.setOnLoadCallback(drawChart);
	     
	     function drawChart() {
	        var data = new google.visualization.DataTable();
	        data.addColumn('string', 'Source');
	        data.addColumn('number', 'Post Count');
	        data.addRows(pieData.data);
	        var chart = new google.visualization.PieChart(document.getElementById('influencer_'+dataDisplay));
	        google.visualization.events.addListener(chart, 'select', function(){
	          var influencer = pieData.raw[chart.getSelection()[0].row].screen_name;
	          showAdvancedModal({
	            person: influencer,
	            topic: 'global',
	            startDate: 'global',
	            endDate: 'global'
	          });
	        });
	        chart.draw(data, {legend: {position: 'bottom'}});
	        $('#influencer').removeClass('active in');
	     }
	  }
	  function showTopPerson(response, dataDisplay){
	     response = response.data;
	     var persons = [];
	     google.charts.load('current', {'packages':['line']});
	     google.charts.setOnLoadCallback(drawChart);
	     var selectedItem = [];
	     function personValidation(name){
	        var isExist = 0;
	        for (var i = 0; i < persons.length; i++){
	           if (persons[i].screen_name == name.screen_name)
	           isExist++;
	        }
	        if (isExist == 0)
	           persons.push(name);
	     }
	     function drawChart() {
	        for (var i = 0; i< response.length; i++){
	           for (var j = 0; j < response[i].persons.length; j++){
	              personValidation(response[i].persons[j]);
	           }
	        }
	        var data = new google.visualization.DataTable();

	        data.addColumn('string', 'Persons');
	        for (var i = 0; i< persons.length; i++){
	           data.addColumn('number', persons[i].display_name);
	        }
	        var contents = [];

	        for (var i = 0; i < response.length; i++){
	           var content = [];
	           content.push(response[i].date);
	           for (var j = 0; j < persons.length; j++){
	              var isExist = -1;
	              for (var k = 0; k < response[i].persons.length; k++){
	                 if (response[i].persons[k].screen_name == persons[j].screen_name)
	                    isExist = k;
	              }
	              if (isExist == -1)
	                 content.push(0);
	              else 
	                 content.push(response[i].persons[isExist].post_count);
	           }
	           contents.push(content);
	        }
	        data.addRows(contents);
	        var options = {
	           axes: {
	              x: {
	              0: {side: 'bottom'}
	              }
	           },
	        };

	        var chart = new google.visualization.LineChart(document.getElementById('toppersons_'+dataDisplay));
	        function selectHandler() {
	           selectedItem = chart.getSelection()[0];
	           if (selectedItem) {
	           }
	        }
	        google.visualization.events.addListener(chart, 'select', function(){
	        	var person = persons[chart.getSelection()[0].column-1].screen_name;
	          	showAdvancedModal({
	                name: person,
	                topic: 'global',
	                date: contents[chart.getSelection()[0].row][0]
	          	})
	        });
	        chart.draw(data);
	        $('#toppersons').removeClass('active in');
	     }
	  }
	  function showDemography(response, dataDisplay){
	     var icons = ['http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|09e','http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|e66'];
	     if (map != null){
	        for (var i = 0; i < response.data.length; i++){
	           new google.maps.Marker({
	              position: new google.maps.LatLng(response.data[i].coordinates[1], response.data[i].coordinates[0]),
	              icon: icons[dataDisplay-1],
	              map: map
	           });
	        }
	     }
	  }
	  function showSentiment(response, dataDisplay){
	     var pieData = [];
	     var positive = 0;
	     var negative = 0;
	     var neutral = 0;
	     for(var i = 0; i < response.data.length; i++){
	        positive += response.data[i].sentiment.positive;
	        negative += response.data[i].sentiment.negative;
	        neutral += response.data[i].sentiment.neutral;
	     }
	     pieData.push(['positive', positive]);
	     pieData.push(['negative', negative]);
	     pieData.push(['neutral', neutral]);
	     google.charts.load('current', {'packages':['corechart']});
	     google.charts.setOnLoadCallback(drawChart);

	     function drawChart() {
	        var data = new google.visualization.DataTable();
	        data.addColumn('string', 'Sentiment');
	        data.addColumn('number', 'Post Count');
	        data.addRows(pieData);

	        var chart = new google.visualization.PieChart(document.getElementById('sentiment_'+dataDisplay));

	        google.visualization.events.addListener(chart, 'select', function(){
	          var sentiment = pieData[chart.getSelection()[0].row][0];
	          showAdvancedModal({
	            sentiment: sentiment,
	            topic: 'global',
	            startDate: 'global',
	            endDate: 'global'
	          });
	        });
	        chart.draw(data, {legend: {position: 'bottom'}});
	        $('#sentiment').removeClass('active in');
	     }
	  }
	     var words = [];
	  function showKeywordMapping(response, dataDisplay){
	    google.charts.load("current", {packages:["corechart"]});
	    google.charts.setOnLoadCallback(drawChart);

	    response = response.data; 
	    var content = [];
	    content.push(['word', 'post count']);
	    console.log(response);
	    for (var i = 0; i < response.length; i++){
	      content.push([response[i][0],response[i][1]]);
	    }
	    console.log(content);
	    function drawChart() {
	      var data = google.visualization.arrayToDataTable(content);

	      var view = new google.visualization.DataView(data);
	      var chart = new google.visualization.BarChart(document.getElementById("mapping_"+dataDisplay));
	      chart.draw(view);
	      $('#mapping').removeClass('active in');
	    }
	  }
	  function addMarker(feature) {
	     var marker = new google.maps.Marker({
	        position: feature.position,
	        icon: icons[feature.type].icon,
	        map: map
	     });
	  }
	}
	function initMap(){
		map = new google.maps.Map(document.getElementById('gmaps'), {
		  center: {lat: -1.8785136, lng: 113.6952623},
		  zoom: 5
		});
	}
	function validateName(name){
		var names = [];
		names = name.split(' ');
		if (names.length < 2)
			return name;
		var name = '';
		for (var i = 0; i < names.length; i++){
			name += names[i];
			if (i + 1 < names.length)
				name += '%20';
		}
		return name;
	}
	function requestURLmapper(request){
		var urls = [];
		var nFlag = false;
		for (var k in request){
		  switch(k){
		    case 'topic':
		      if (request[k] == 'global')
		        urls.push('topic='+topic);
		      else if (typeof request[k] != 'undefined')
		        urls.push('topic='+request[k]);
		      break;
		    case 'startDate':
		      if (request[k] == 'global')
		        urls.push('date_start='+dateStart);
		      else if (typeof request[k] != 'undefined')
		        urls.push('date_start='+request[k]);
		      break;
		    case 'endDate' : 
		      if (request[k] == 'global')
		        urls.push('date_stop='+dateStop);
		      else if (typeof request[k] != 'undefined')
		        urls.push('date_stop='+request[k]);
		      break;
		    case 'date':
		    	urls.push('date='+request[k]);
		    	break
		    case 'person' : 
		      if(typeof request[k] != 'undefined')
		        urls.push('person='+validateName(request[k]));
		      break;
		    case 'name' : 
		      if(typeof request[k] != 'undefined')
		        urls.push('name='+validateName(request[k]));
		      break;
		    case 'n' :
		      if (typeof request[k] != 'undefined'){
		        nFlag = true;
		        urls.push('n='+request[k]);
		      }
		      break;
		    case 'sentiment':
		      if (typeof request[k] != 'undefined'){
		        sentimentNumber = 0;
		        switch(request[k]){
		          case 'positive':
		            sentimentNumber = 1;
		            break;
		          case 'negative' :
		            sentimentNumber = -1;
		            break;
		          default:
		            sentimentNumber = 0;
		            break;
		        }
		        urls.push('sentiment='+sentimentNumber);
		      }
		    default:
		      break;
		  }
		}
		if (!nFlag)
		  urls.push('n=20');
		var url = '';
		for(var i = 0; i < urls.length; i++){
		  if (i == 0)
		    url += '?';
		  url += urls[i];
		  if (i+1 < urls.length)
		    url += '&';
		}
		return url;
	}
	function showAdvancedModal(request){
		var url = '{{URL::to("bridge/get/posts")}}' +requestURLmapper(request);
		$('#advancedModal').modal('toggle');

		var twitterNotification = $('#twitterPostLoader');
		twitterNotification.show();
		var facebookNotification = $('#facebookPostLoader');
		facebookNotification.show();
		var portalNotification = $('#portalPostLoader');
		portalNotification.show();

		tunnel.beritas = new Berita($("#portalPostContent"), objects, '{{URL::to("userFile")}}');
		tunnel.facebooks = new Facebook($("#facebookPostContent"));
		tunnel.twitters = new Twitter($("#twitterPostContent"));

		var postSource = ['tweet', 'fb', 'berita'];
		for (var i = 0; i< postSource.length; i++){
			var type = postSource[i];
			console.log(url+'&type='+type);
			$.get(url+'&type='+type).done(function(response){
				response = jQuery.parseJSON(response);
				dataDisplay = response;
				for(var j = 0; j<response.length; j++){
					tunnel.factory(response[j]);
				}
				$(tunnel.facebooks.container).children().remove();
				$(tunnel.twitters.container).children().remove();
				$(tunnel.beritas.container).children().remove();
				tunnel.clearView();
				tunnel.show();
				twitterNotification.hide();
				facebookNotification.hide();
				portalNotification.hide();
			});
		}
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsXNCvwTsLM00_NuUvBFK7Meg6sqNwP3I&callback=initMap" async defer></script>
@endsection
