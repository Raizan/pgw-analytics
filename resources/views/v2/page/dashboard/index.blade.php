@extends('v2.template.index')
 
@section('main-content')
<div class="row-fluid">
  <div class="span12">
    <ul class="breadcrumb-beauty">
      <li>
        <a href="{{ URL::to('/v2/index') }}"><span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span> Dashboard</a>
      </li>
      <li>
        <a href="{{ URL::to('/v2/index') }}">Main</a>
      </li>
    </ul>
  </div>
</div>

<br>

@include('v2.template.top_graph')
<!-- latest topic list -->
<div class="row-fluid">
  <div class="span12">
    <div class="widget">
      <div class="widget-header">
      <div class="title">
      <span class="fs1" aria-hidden="true" data-icon="&#xe1cd;"></span> Topik List | Silahkan memilih topik berikut atau  
      <button class="badge badge-success input-bottom-margin standardModalButton" style="border: none" href="#standardModal" role="button" data-toggle="modal" url="{{route('v2.topic.create')}}">
            Tambahkan Topik
      </button>
      </div>
      <div class="tools pull-right">
      </div>
      </div>
      <div class="widget-body">
         <!-- tabel topik -->
        <div class="row-fluid" >
        <?php
        $topicCounter = 0;
        ?>
        @foreach($data['topics'] as $print)
          @if(++$topicCounter%4 == 3)
          </div>
          <div class="row-fluid">
          @endif
          <div class="span3" style="margin-left:4px">
            <div class="widget widget-border">
              <div class="widget-body">
                <div class="current-stats">
                  <h4 class="text-warning" style="font-size:15px">{{$print->keyword}}</h4>
                  <p>Crawl Start:&nbsp;<b>{{$print->created_at}}</b></p>
                  <div class="type">
                    <span class="fs1 text-warning fa fa-search-plus fs1 fa-3x" aria-hidden="true"  ></span>
                  </div>
                </div>
              </div>
              <div class="widget ">
                <center>Object :
                @foreach($print->crawl_features as $cf)
                    @if ($cf['object']['logo'] != '')
                      <img src="{{asset('/userFile/'.$cf['object']['logo'])}}" title="{{$cf['object']['name']}}" style="max-width: 16px; max-height: 16px">
                    @endif
                @endforeach
                </center>
              </div>
              <center>
                <a class="btn btn-warning2 popover-pop" href="{{URL::to('v2/analyze/'.str_replace(' ', '%20', $print->keyword))}}">
                  <i class="fa fa-sign-in"></i>&nbsp;pilih topik
                </a>
              </center> 
              <br>
            </div>
          </div>
        @endforeach

        </div>
      </div>   
    </div>
<!-- CRAWL STAT -->
<div class="row-fluid">
  <div class="span6">
    <div class="plain-header">
      <h4 class="title">
       <span class="fs1 fa fa-line-chart" aria-hidden="true" ></span> Crawl Stats
      </h4>
    </div>
    <div class="row-fluid">
      <div class="span6">
        <div class="widget less-bottom-margin widget-border widget-border">
          <div class="widget-body">
            <div class="current-stats">
              <h4 class="text-info">{{$data['latest_topic']->keyword}}</h4>
              <p>Latest Topic</p>
              <div class="type">
                <span class="fs1 arrow text-info" aria-hidden="true" data-icon="&#xe048;"></span> 
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="span6">
        <div class="widget less-bottom-margin widget-border">
          <div class="widget-body">
            <div class="current-stats">
              <h4 class="text-success" id="storage">~ GB</h4>
              <p>Storage Used</p>
              <div class="type">
                <span class="fs1 fa fa-hdd-o fa-3x arrow" aria-hidden="true"></span> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span6">
        <div class="widget widget-border">
          <div class="widget-body">
            <div class="current-stats">
              <h4 class="text-warning">{{$data['crawl_object']}}</h4>
              <p>Crawl Object</p>
              <div class="type">
                <span class="fs1 arrow text-warning" aria-hidden="true" data-icon="&#xe077;"></span> 
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="span6">
        <div class="widget widget-border">
          <div class="widget-body">
            <div class="current-stats">
              <h4 id="total"> ~ </h4>
              <p>Total Items Crawled</p>
              <div class="type">
                <span class="fs1 fa fa-search-plus fa-3x arrow" aria-hidden="true"></span> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="span6">
    <div class="plain-header">
      <h4 class="title">
        <span class="fs1 fa fa-clock-o" aria-hidden="true" ></span>&nbsp;Crawl Latest Logs
      </h4>
    </div>
    <div class="row-fluid">
      <div class="span6">
        <div class="widget less-bottom-margin widget-border">
          <div class="widget-body">
            <div class="current-stats">
              <h4 class="text-success">Portal Berita</h4>
              <p>View Crawl Logs</p>
              <div class="type">
                <span class="fs1 arrow fa fa-newspaper-o fa-3x text-success" aria-hidden="true"></span> 
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="span6">
        <div class="widget less-bottom-margin widget-border">
          <div class="widget-body">
            <div class="current-stats">
              <h4 class="text-info">Facebook</h4>
              <p>View Crawl Logs</p>
              <div class="type">
                <span class="fs1 arrow fa fa-facebook fa-3x text-info" aria-hidden="true"></span> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span6">
        <div class="widget widget-border">
          <div class="widget-body">
            <div class="current-stats">
              <h4 class="text-warning" style="color:#0084b4">Twitter</h4>
              <p>View Crawl Logs</p>
              <div class="type">
                <span class="fs1 arrow fa fa-twitter fa-3x text-warning" style="color:#0084b4" aria-hidden="true"  ></span> 
              </div>
            </div>
          </div>
        </div>
      </div>
       
    </div>
  </div>
</div>

@endsection

 @section('custom-footer')
<script type="text/javascript">
  $.ajax({
      url: "{{ URL::to('/stat/storage') }}",
      type: "GET",
      dataType: "html",
      success: function (data) {  
          $('#storage').html(data);
      },
  });
  $.ajax({
      url: "{{ URL::to('/stat/totalcrawled') }}",
      type: "GET",
      dataType: "html",
      success: function (data) {  
        data = jQuery.parseJSON(data);
        $('#total').html(data.total);
      },
  });
  var dtl = [];
  for (var i = 0; i < 30; i++){
    dtl.push(0);
  }
  setInterval(function(){
    $.get("{{URL::to('stat/cpu_load')}}").done(function(response){
      dtl.shift();
      dtl.push(response);
      showDTL();
    });
    function showDTL(){
      var newDTL = [];
      for (var i = 0; i < dtl.length; i++){
        newDTL.push([i+1, dtl[i]]);
      }
      $(function() {
        var data = [{
          label: "Server Stats (Core CPU/Second)",
          data: newDTL
        }];
        var options = {
          series: {
            lines: { show: true,
              lineWidth: 2,
              fill: false,
              },
            points: { show: true, 
              lineWidth: 2 
              },
            shadowSize: 0
          },
          grid: { hoverable: true, 
            clickable: true, 
            tickColor: "#eeeeee",
            borderWidth: 0
          },
          legend: {
            noColumns: 3
          },
          colors: ["#3b5a9b", "#d3503e"],
           xaxis: {ticks:12, tickDecimals: 0},
           yaxis: {ticks:3, tickDecimals: 0},
          selection: {
            mode: "x"
          }
        };

        var placeholder = $("#selectionCharts");

        var plot = $.plot(placeholder, data, options);
      });
    }
  }, 1000);
</script>
<script src="{{asset('v2/js/custom-index.js')}}"></script>
 @endsection