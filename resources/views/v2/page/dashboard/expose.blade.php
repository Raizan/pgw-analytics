@extends('v2.template.index')
@section('custom-head')
 <link href="{{asset('v2/css/pace_orange.css')}}" rel="stylesheet">
  <script src="{{asset('v2/js/pace.min.js')}}"></script>
  <style type="text/css">
  body > :not(.pace),body:before,body:after {
  -webkit-transition:opacity .4s ease-in-out;
  -moz-transition:opacity .4s ease-in-out;
  -o-transition:opacity .4s ease-in-out;
  -ms-transition:opacity .4s ease-in-out;
  transition:opacity .4s ease-in-out
}

body:not(.pace-done) > :not(.pace),body:not(.pace-done):before,body:not(.pace-done):after {
  opacity:0.3
}
  </style>
 <script>
   paceOptions = {
  elements: {
    selectors: ['.loader_pre']
  }
}
  </script>
   <script>
    function load(time){
      var x = new XMLHttpRequest()
      x.open('GET', "{{ URL::to('/v2/help') }}" + time, true);
      x.send();
    };

    load(20);
    load(100);
    load(500);
    load(2000);
    load(3000);

    setTimeout(function(){
      Pace.ignore(function(){
        load(3100);
      });
    }, 4000);

    Pace.on('hide', function(){
      console.log('done');
    });
  </script>

 <script>

    range.addEventListener('input', function(){
      document.querySelector('.pace').classList.remove('pace-inactive');
      document.querySelector('.pace').classList.add('pace-active');

      document.querySelector('.pace-progress').setAttribute('data-progress-text', range.value + '%');
      document.querySelector('.pace-progress').setAttribute('style', '-webkit-transform: translate3d(' + range.value + '%, 0px, 0px)');
    });
  </script>
  @endsection

@section('main-content')
<div class="row-fluid">
  <button id="change-chart">Change Chart</button>
</div>
<div class="row-fluid loader_pre" style="background-color: white">
  <div class="span12">
   <!--  <center id="loader">
      <img src="{{asset('v2/img/loading-red.gif')}}">
      Mohon ditunggu, Sedang mengunduh data...
    </center> -->
    <div class="row-fluid">
      <div class="span6">
        <div id="barchart_material" style="width: 100%; height: 700px;"></div>
      </div>
      <div class="span6">
        <div class="row-fluid">
          <div id="wordMapping" style="width: 100%; height: 650px; float: left"></div>
          <span style="height: 16px; width: 16px; background-color: blue; margin-right: 16px; float:left"></span>
          <span style="margin-right: 16px; float:left" id="lw_1"></span>
          <span style="height: 16px; width: 16px; background-color: cyan; margin-right: 16px; float:left"></span>
          <span style="margin-right: 16px; float:left" id="lw_2"></span>
          <span style="height: 16px; width: 16px; background-color: green; margin-right: 16px; float:left"></span>
          <span style="margin-right: 16px; float:left" id="lw_3"></span>
          <span style="height: 16px; width: 16px; background-color: orange; margin-right: 16px; float:left"></span>
          <span style="margin-right: 16px; float:left" id="lw_4"></span>
          <span style="height: 16px; width: 16px; background-color: yellow; margin-right: 16px; float:left"></span>
          <span style="margin-right: 16px; float:left" id="lw_5"></span>
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
   <div class="row-fluid">
  <div class="span12">
  	<h5>Topic List Object Crawl - Please wait 30-40 Second to get all data Loaded.</h5>
  	@foreach($data['topics'] as $t)
  	<span style="padding-right:15px"><b>{{$t->keyword}} :</b>
  		@foreach($t->crawl_features as $cf)
		
		@if ($cf['object']['name'] != '')
			<span><img src="{{asset('userFile/'.$cf['object']['logo'])}}" title="{{$cf['object']['name']}}" style="max-width: 16px; max-height: 16px"></span>
		@endif
		@endforeach
    </span>
  	@endforeach
  </div>
</div>
 
 @endsection

 @section('custom-footer')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="{{asset('/dev/getgraphicdata.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/assets/scripts/tunnel.js')}}"></script>
<script type="text/javascript" src="{{asset('dev/getposts.js')}}"></script>
@include('v2.custom.expose')

<script src="{{asset('v2/js/custom-index.js')}}"></script>
 @endsection