@extends('v2.template.index')
@section('custom-head')
 <link href="{{asset('v2/css/pace_orange.css')}}" rel="stylesheet">
  <script src="{{asset('v2/js/pace.min.js')}}"></script>
  <style type="text/css">
  body > :not(.pace),body:before,body:after {
  -webkit-transition:opacity .4s ease-in-out;
  -moz-transition:opacity .4s ease-in-out;
  -o-transition:opacity .4s ease-in-out;
  -ms-transition:opacity .4s ease-in-out;
  transition:opacity .4s ease-in-out
}

body:not(.pace-done) > :not(.pace),body:not(.pace-done):before,body:not(.pace-done):after {
  opacity:0.3
}
  </style>
 <script>
   paceOptions = {
  elements: {
    selectors: ['.loader_pre']
  }
}
  </script>
   <script>
    function load(time){
      var x = new XMLHttpRequest()
      x.open('GET', "{{ URL::to('/v2/help') }}" + time, true);
      x.send();
    };

    load(20);
    load(100);
    load(500);
    load(2000);
    load(3000);

    setTimeout(function(){
      Pace.ignore(function(){
        load(8100);
      });
    }, 8000);

    Pace.on('hide', function(){
      console.log('done');
    });
  </script>

 <script>

    range.addEventListener('input', function(){
      document.querySelector('.pace').classList.remove('pace-inactive');
      document.querySelector('.pace').classList.add('pace-active');

      document.querySelector('.pace-progress').setAttribute('data-progress-text', range.value + '%');
      document.querySelector('.pace-progress').setAttribute('style', '-webkit-transform: translate3d(' + range.value + '%, 0px, 0px)');
    });
  </script>
  @endsection
@section('main-content')
<div class="row-fluid">
  <div class="span12">
    <ul class="breadcrumb-beauty">
      <li>
        <a href="index.html"><span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span> Dashboard</a>
      </li>
      <li>
        <a href="#">Stream</a>
      </li>
    </ul>
  </div>
</div>
<div class="row-fluid">
  <div class="span12">
    <div class="widget no-margin">
      <div class="widget-header">
        <div class="title">
          <span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span> Crawl Stream
        </div>
      </div>
      <div class="widget-body">
        <div id="dt_example" class="example_alt_pagination">
          <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">
            <thead>
              <tr>
                <th style="max-width: 50px">No</th>
                <th style="max-width: 80%">URL</th>
                <th style="max-width: 250px">Source</th>
                <th>Timestamp</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<br>
@endsection
@section('custom-footer')
<script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
var dataResult = [];
var maxDataResult = 25;
var getResponse = [];
var url = "{{URL::to('/bridge/get/latest_crawl')}}";
var dataTable = $("#data-table").DataTable();
setInterval(function(){
  $.get(url).done(function(response){
    console.log(response);
    response = jQuery.parseJSON(response);
    getResponse = response;
    dataValidation(response);
    refreshDataTable();
  });

  function dataValidation(response){
     for(var i = 0; i<response.length; i++){
        console.log(response[i]);        
        var isExisted = false;
        for (var j = 0; j<dataResult.length; j++){
           if ((response[i]).url == dataResult[j].url)
              isExisted = true;
        }
        if (!isExisted){
           var dataResultLength = dataResult.length;
           var excededDataResultLength = dataResultLength - maxDataResult;
           if (excededDataResultLength > 0){
              dataResult.splice(0, excededDataResultLength);
           }
           dataResult.push((response[i]));
        }
     }
  };
  function refreshDataTable(){
     dataTable.clear();
     for(var i = dataResult.length-1; i >= 0; i--){
        dataTable.row.add([i + 1, '<a href="'+dataResult[i].url +'">' + dataResult[i].url + '</a>',dataResult[i].source, dataResult[i].time.replace('T', ' ')]).draw();
     }
  }
}, 5000);
$('#crawl_restart').click(function(){
  $.get($(this).attr('href')).done(function(response){
     if (response){
        toastr.success('Crawler Restarted');
     }
  })
});
</script>
@endsection
