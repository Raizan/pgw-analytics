@extends('v2.template.index')

@section('custom-head')
 <link href="{{asset('v2/css/pace_orange.css')}}" rel="stylesheet">
  <script src="{{asset('v2/js/pace.min.js')}}"></script>
  <style type="text/css">
  body > :not(.pace),body:before,body:after {
  -webkit-transition:opacity .4s ease-in-out;
  -moz-transition:opacity .4s ease-in-out;
  -o-transition:opacity .4s ease-in-out;
  -ms-transition:opacity .4s ease-in-out;
  transition:opacity .4s ease-in-out
}

body:not(.pace-done) > :not(.pace),body:not(.pace-done):before,body:not(.pace-done):after {
  opacity:0.3
}
  </style>
 <script>
   paceOptions = {
  elements: {
    selectors: ['.loader_pre']
  }
}
  </script>
   <script>
    function load(time){
      var x = new XMLHttpRequest()
      x.open('GET', "{{ URL::to('/v2/help') }}" + time, true);
      x.send();
    };

    load(20);
    load(100);
    load(500);
    load(2000);
    load(3000);

    setTimeout(function(){
      Pace.ignore(function(){
        load(3100);
      });
    }, 4000);

    Pace.on('hide', function(){
      console.log('done');
    });
  </script>

 <script>

    range.addEventListener('input', function(){
      document.querySelector('.pace').classList.remove('pace-inactive');
      document.querySelector('.pace').classList.add('pace-active');

      document.querySelector('.pace-progress').setAttribute('data-progress-text', range.value + '%');
      document.querySelector('.pace-progress').setAttribute('style', '-webkit-transform: translate3d(' + range.value + '%, 0px, 0px)');
    });
  </script>
  @endsection
 @section('main-content')
  
<div class="row-fluid">
            <div class="span12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span> KSM Analytic | Help Page
                  </div>
                </div>
                <div class="widget-body">
                  <div id="accordion1" class="accordion no-margin">
                    <div class="accordion-group">
                      <div class="accordion-heading">
                        <a href="#collapseOne" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                          <b>1.</b>
                          Create Topic (Menambahkan Topik Baru) ?
                        </a>
                      </div>
                      <div class="accordion-body collapse" id="collapseOne" style="height: 0px;">
                        <div class="accordion-inner">
                        	Pada Dashboard utama KSM Analytic terdapat tabel "Topic List", di tabel ini akan terlihat topic yang sedang di crawl. 
                        	<br>Jika Anda ingin menambahan topic baru untuk dicrawl silahkan pilih tombol " Tambahkan Topik " 
                        	<br><span style="font-style:italic;color:red">(lihat gbr. dibawah ini)</span>
							<p><img src="{{asset('v2/img/help/help1.png')}}">
							</p>
						                       </div>
                      </div>
                    </div>
                    <div class="accordion-group">
                      <div class="accordion-heading">
                        <a href="#2" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                          <b>2.</b>
                          Melihat hasil Crawl Topic yang sedang berjalan? 
                        </a>
                      </div>
                      <div class="accordion-body collapse" id="2" style="height: 0px;">
                        <div class="accordion-inner">
						Untuk melihat crawl topic yang sedang berjalan silahkan pilih salah satu box topic, dengan menekan tombol "pilih topic"
						<br><span style="font-style:italic;color:red">(lihat gbr. dibawah ini)</span>
						<p><img src="{{asset('v2/img/help/help2.png')}}">
							</p>
						</div>
                      </div>
                    </div>
                    <div class="accordion-group">
                      <div class="accordion-heading">
                        <a href="#3" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                          <b>3.</b>
                          Membandingkan 2 data crawl topic yang sedang berjalan?
                        </a>
                      </div>
                      <div class="accordion-body collapse" id="3" style="height: 0px;">
                        <div class="accordion-inner">
                        	Untuk bisa membandingkan 2 topic yang sedang berjalan, silahkan pilih tombol "statistic" pada sidebar menu, 
                        	<br>kemudian pilih tab "compare topic", setelah itu pilih topic yang akan di compare dan tekan tombol "compare"
                        	<br><span style="font-style:italic;color:red">(lihat gbr. dibawah ini)</span>
						<p><img src="{{asset('v2/img/help/help3.png')}}">
							</p>
                        </div>
                      </div>
                    </div>
                    <div class="accordion-group">
                      <div class="accordion-heading">
                        <a href="#4" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                          <b>4.</b>
                          Menambahkan Object Crawl (Facebook ID)
                        </a>
                      </div>
                      <div class="accordion-body collapse" id="4" style="height: 0px;">
                        <div class="accordion-inner">
                        	Untuk menambahkan Object Crawl (Facebook ID), silahkan pilih tombol "configuration" pada sidebar.
                        	<br>Kemudian pilih tab "Social Object Config" dan pilih tombol "add"
						<br><span style="font-style:italic;color:red">(lihat gbr. dibawah ini)</span>
						<p><img src="{{asset('v2/img/help/help4.png')}}">
							</p>
	                        </div>
                      </div>
                    </div>
                    <div class="accordion-group">
                      <div class="accordion-heading">
                        <a href="#5" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                          <b>5.</b>
                         Menambahkan Object Crawl (URL Portal)
                        </a>
                      </div>
                      <div class="accordion-body collapse" id="5" style="height: 0px;">
                        <div class="accordion-inner">
                        	Untuk menambahkan Object Crawl (Facebook ID), silahkan pilih tombol "configuration" pada sidebar.
                        	<br>Kemudian pilih tab "Social Object Config" dan pilih tombol "add"
						<br><span style="font-style:italic;color:red">(lihat gbr. dibawah ini)</span>
						<p><img src="{{asset('v2/img/help/help5.png')}}">
							</p>
                        </div>
                      </div>
                    </div>
                    <div class="accordion-group">
                      <div class="accordion-heading">
                        <a href="#collapseThree" data-parent="#accordion1" data-toggle="collapse" class="accordion-toggle" data-original-title="">
                          <b>6.</b>
                          Menambahkan User Baru
                        </a>
                      </div>
                      <div class="accordion-body in collapse" id="collapseThree" style="height: auto;">
                        <div class="accordion-inner">
						Untuk menambahkan User baru, silahkan pilih tombol "configuration" pada sidebar.
                        	<br>Kemudian pilih tab "user Management" dan pilih tombol "add"
						<br><span style="font-style:italic;color:red">(lihat gbr. dibawah ini)</span>
						<p><img src="{{asset('v2/img/help/help6.png')}}">
							</p>
	                        </div>

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection
 
 @section('custom-footer')

 @endsection
  