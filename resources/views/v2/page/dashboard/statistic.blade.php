@extends('v2.template.index')
@section('custom-head')
 <link href="{{asset('v2/css/pace_orange.css')}}" rel="stylesheet">
  <script src="{{asset('v2/js/pace.min.js')}}"></script>
  <style type="text/css">
  body > :not(.pace),body:before,body:after {
  -webkit-transition:opacity .4s ease-in-out;
  -moz-transition:opacity .4s ease-in-out;
  -o-transition:opacity .4s ease-in-out;
  -ms-transition:opacity .4s ease-in-out;
  transition:opacity .4s ease-in-out
}

body:not(.pace-done) > :not(.pace),body:not(.pace-done):before,body:not(.pace-done):after {
  opacity:0.1
}
  </style>
 <script>
 paceOptions = {
  ajax: false, // disabled
  document: false, // disabled
  eventLag: false, // disabled
  elements: {
    selectors: ['.loader-ok']
  }
};
  </script>
   <script>
    function load(time){
      var x = new XMLHttpRequest()
      x.open('GET', "{{ URL::to('/v2/help') }}" + time, true);
      x.send();
    };

    load(20);
    load(100);
    load(500);
    load(2000);
    load(3000);

    setTimeout(function(){
      Pace.ignore(function(){
        load(3100);
      });
    }, 4000);

    Pace.on('hide', function(){
      console.log('done');
    });
  </script>

 <script>

    range.addEventListener('input', function(){
      document.querySelector('.pace').classList.remove('pace-inactive');
      document.querySelector('.pace').classList.add('pace-active');

      document.querySelector('.pace-progress').setAttribute('data-progress-text', range.value + '%');
      document.querySelector('.pace-progress').setAttribute('style', '-webkit-transform: translate3d(' + range.value + '%, 0px, 0px)');
    });
  </script>
  @endsection
@section('main-content')
<div class="row-fluid loader-ok">
  <div class="span12">
    <div class="widget">
      <div class="widget-header">
        <div class="title">
          <span class="fs1" aria-hidden="true" data-icon="&#xe0b6;"></span> Statistic
        </div>
      </div>
      <div class="widget-body">
        <ul class="nav nav-tabs no-margin myTabBeauty">
          <li class="active">
            <a data-toggle="tab" href="#peak">
              Peak Time
            </a>
          </li>
          <li class="">
            <a data-toggle="tab" href="#compare">
              Compare Topic
            </a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div id="peak" class="tab-pane fade active in">
            <div id="peak_chart" style="height: 400px">
              <center>
                No Data Available
              </center>
            </div>
          </div>
          <div id="compare" class="tab-pane fade">
            <div class="span6">
               <div class="row">
                  <div class="span12">
                     <div class="form-group">
                      <center>
                        <label >TOPIC #1</label>
                        <select  class="form-control" name="topic_1">
                           <?php
                           $iter = 0;
                           ?>
                           @foreach($crawl_queue as $print)
                              @if ($iter == 0)
                              <option value="{{str_replace(' ', '%20', $print->keyword)}}" selected>{{$print->keyword}}</option>
                              @else 
                              <option value="{{str_replace(' ', '%20', $print->keyword)}}">{{$print->keyword}}</option>
                              @endif
                              <?php $iter++?>
                           @endforeach
                        </select>
                      </center>
                     </div>
                  </div>
               </div>
            </div>
            <div class="span6">
               <div class="row">
                  <div class="span12">
                     <div class="form-group">
                      <center>
                        <label >TOPIC #2</label>
                        <select  class="form-control" name="topic_2">
                           <?php
                           $iter = 0;
                           ?>
                           @foreach($crawl_queue as $print)
                              @if ($iter == 0)
                              <option value="{{str_replace(' ', '%20', $print->keyword)}}" selected>{{$print->keyword}}</option>
                              @else 
                              <option value="{{str_replace(' ', '%20', $print->keyword)}}">{{$print->keyword}}</option>
                              @endif
                              <?php $iter++?>
                           @endforeach
                        </select>
                      </center>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row-fluid" style="margin-bottom: 25px">
              <center>
                 <button class="btn red" id="compare_button">Compare</button>
              </center>
            </div>
            <div class="row-fluid" >
              <ul class="nav nav-tabs no-margin myTabBeauty">
                <li class="active">
                  <a data-toggle="tab" href="#influencer">
                    Influencers
                  </a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#toppersons">
                    Top Persons
                  </a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#sentiment">
                    Sentiment
                  </a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#demograpy">
                    Demography
                  </a>
                </li>
                <li class="">
                  <a data-toggle="tab" href="#mapping">
                    Related Words
                  </a>
                </li>
              </ul>
              <div class="tab-content" id="compareContent">
                <div id="influencer" class="tab-pane fade active in">
                  <div class="span6">
                    <div id="influencer_1" style="width:100%; min-height: 400px">
                      <center>
                        <img src="{{asset('v2/img/loading-red.gif')}}" class="loader">
                      </center>
                    </div>
                  </div>
                  <div class="span6">
                    <div id="influencer_2" style="width:100%; min-height: 400px">
                      <center>
                        <img src="{{asset('v2/img/loading-red.gif')}}" class="loader">
                      </center>
                    </div>
                  </div>
                </div>
                <div id="toppersons" class="tab-pane fade">
                  <div class="span6">
                    <div id="toppersons_1" style="width:100%; min-height: 400px">
                      <center>
                        <img src="{{asset('v2/img/loading-red.gif')}}" class="loader">
                      </center>
                    </div>
                  </div>
                  <div class="span6">
                    <div id="toppersons_2" style="width:100%; min-height: 400px">
                      <center>
                        <img src="{{asset('v2/img/loading-red.gif')}}" class="loader">
                      </center>
                    </div>
                  </div>
                </div>
                <div id="sentiment" class="tab-pane fade">
                  <div class="span6">
                    <div id="sentiment_1" style="width:100%; min-height: 400px">
                      <center>
                        <img src="{{asset('v2/img/loading-red.gif')}}" class="loader">
                      </center>
                    </div>
                  </div>
                  <div class="span6">
                    <div id="sentiment_2" style="width:100%; min-height: 400px">
                      <center>
                        <img src="{{asset('v2/img/loading-red.gif')}}" class="loader">
                      </center>
                    </div>
                  </div>
                </div>
                <div id="demograpy" class="tab-pane fade">
                  <div id="demograpy_graph" style="width:100%; height: 400px"></div>
                </div>
                <div id="mapping" class="tab-pane fade">
                	<div class="row-fluid">	
	                  <div class="span6">
	                    <div id="mapping_1" style="height: 500px; width: 100%"></div>
	                  </div>
	                  <div class="span6">
	                    <div id="mapping_2" style="height: 500px; width: 100%"></div>
	                  </div>
                	</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('custom-head')
@section('custom-header')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="{{asset('assets/assets/plugins/jQCloud-master/dist/jqcloud.css')}}">
@endsection
@section('custom-footer')
  <?php
  $now = date('Y-m-d');
  $startDate = strtotime('-30 day', strtotime($now));
  $startDate = date('Y-m-d', $startDate);
  $endDate = date('Y-m-d');
  ?>
   <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
   <script type="text/javascript" src="{{asset('assets/assets/scripts/tunnel.js')}}"></script>
   <script type="text/javascript" src="{{asset('assets/assets/plugins/jQCloud-master/dist/jqcloud.js')}}"></script>
   <script type="text/javascript">
   peakJS();
   compareJS();
  $('.loader').hide();
   function peakJS(){
     var dateStart = "{{$startDate}}";
     var dateStop = "{{$endDate}}";
     var persons = [];
     var peakData = [];
     var urls = [];
     var graphData = [];
     var topics = [];
     @foreach($crawl_queue as $d)
     var url = '{{URL::to("/bridge/get/post_range")}}?topic={{str_replace(" ","%20",$d->keyword)}}&n=5&date_start='+dateStart+'&date_stop='+dateStop+'&search_type=count';
     urls.push(['{{$d->keyword}}',url]);
     topics.push('{{$d->keyword}}');
     @endforeach
     var count = 0;

     for (var i = 0; i<urls.length; i++){
        getPeak(urls[i]);
     }

     $(function() {
        $('input[name="daterange"]').val(moment(dateStart).format('MM-DD-YYYY') + ' - ' + moment(dateStop).format('MM-DD-YYYY'));
         $('input[name="daterange"]').daterangepicker({
           'startDate' : moment(dateStart).format('MM-DD-YYYY'),
           'endDate' : moment(dateStop).format('MM-DD-YYYY'),
         }, function(start, end, label) {
           dateStart = start.format('YYYY-MM-DD');
           dateStop = end.format('YYYY-MM-DD');
           for (var i = 0; i<urls.length; i++){
              getPeak(urls[i]);
           }
         });
     });

     function getPeak(url){
        $.get(url[1]).done(function(response){
           try{
              response = JSON.parse(response);
              for (var i = 0; i < response.length; i++){
                 response[i].topic = url[0];
                 peakData.push(response[i]);
              }
           } catch(error){

           }
           count++;
           showGraph();
        });
     }
     
     function showGraph(){
        if (count >= urls.length){
           var columns = [];
           for (var i = 0; i < urls.length; i++){
              columns.push(urls[i][0]);
           }
           var rows = [];

           for (var i = 0; i<peakData.length; i++){
              var isExist = -1;
              for (var j = 0; j<rows.length; j++){
                 if (rows[j][0] == peakData[i].date)
                    isExist++;
              }
              if (isExist == -1)
                 rows.push([peakData[i].date]);
           }

           graphData = {'columns' : columns, 'rows' : rows};
           var graphTemplate = [];
           for (var i = 0; i<rows.length; i++){
              var rowTemplate = [];
              rowTemplate.push(rows[i][0]);
              for (var j = 0; j<topics.length; j++){
                 var topicCount = 0;
                 for (var k = 0; k < peakData.length; k++){
                    if(peakData[k].date == rowTemplate[0] && peakData[k].topic == topics[j])
                       topicCount = peakData[k].count;
                 }
                 rowTemplate.push(topicCount);
              }
              graphTemplate.push(rowTemplate);
           }
           graphData.rows = graphTemplate;

           google.charts.load('current', {'packages':['line']});
           google.charts.setOnLoadCallback(drawChart);
           
           function drawChart(){
              var data = new google.visualization.DataTable();
              data.addColumn('string', 'Date');
              for (var i = 0; i < topics.length; i++){
                 data.addColumn('number', topics[i]);
              }
              data.addRows(graphData.rows);
              var chart = new google.charts.Line(document.getElementById('peak_chart'));
              function selectHandler() {
                 selectedItem = chart.getSelection()[0];
                 if (selectedItem) {
                    var column = selectedItem.column;
                    var row = selectedItem.row;
                    if (column != null && row != null){
                       getDetailPosts(graphData.columns[column-1], graphData.rows[row][0]);
                    }
                 }
              }
              function getDetailPosts(name, date){
                 var tunnel = new Tunnel($("#facebook_log"), $("#tweeter_log"), $("#portal_log"));
                 tunnel.beritas = new Berita($("#portal_log"));
                 tunnel.facebooks = new Facebook($("#facebook_log"));
                 tunnel.twitters = new Twitter($("#twitter_log"));

                 var postType = ['tweet', 'fb', 'berita'];
                 var totalIter = 0;
                 var showIter = 0;

                 for (var j = 0; j < postType.length; j++){
                    getPosts(postType[j], name, date);
                    totalIter++;
                 }
                 function getPosts(type, name,date){
                    var getURL = "{{URL::to('/bridge/get/posts')}}"+'?topic='+name+'&type='+type+'&n=20&date='+date;
                    $.get(getURL).done(function(response) {
                       response = jQuery.parseJSON(response);
                       for (var i = 0; i < response.length; i++){
                          tunnel.factory(response[i]);
                       }
                       show();
                    });
                 }
                 function show(){
                    showIter++;
                    if (showIter >= totalIter)
                      tunnel.show();
                 }
              }
              google.visualization.events.addListener(chart, 'select', selectHandler);    
              chart.draw(data, {height: 400, axes: { x: { 0: {side: 'bottom'} } } } );

              countData();
              function countData(){
                 var total = 0;
                 for (var i = 0; i < graphData.rows.length; i++){
                    for (var j = 1; j<graphData.rows[i].length; j++){
                       total += graphData.rows[i][j];
                    }
                 }
                 $('#count').text('Total Data: '+total);
              }
           }
        }
     }
   }
   function compareJS(){
    <?php
      $now = date('Y-m-d');
      $startDate = strtotime('-30 day', strtotime($now));
      $startDate = date('Y-m-d', $startDate);
      $endDate = date('Y-m-d');
      ?>

      var dateStart = "{{$startDate}}";
      var dateStop = "{{$endDate}}";

      var dataDisplay_1 = [];
      var dataDisplay_2 = [];
      var map;

      $("#compare_button").click(function(){
          $('.loader').show();
         var topic_1 = $('select[name=topic_1]').val();
         var topic_2 = $('select[name=topic_2]').val();

         var influencer_1 = getData(1,topic_1);
         var influencer_2 = getData(2,topic_2);
      });
      function getData(dataDisplay, topic){
         getInfluencer(dataDisplay,topic)
         getTopPerson(dataDisplay, topic);
         getDemography(dataDisplay, topic);
         getSentiment(dataDisplay, topic);
         getKeywordMapping(dataDisplay, topic);
      }
      function setData_1(type, data){
         dataDisplay_1.push({type: type, data: data});
         if (dataDisplay_1.length >= 5)
            showData(dataDisplay_1, 1);
      }
      function setData_2(type, data){
         dataDisplay_2.push({type: type, data: data});
         if (dataDisplay_2.length >= 5)
            showData(dataDisplay_2, 2);
      }

      function getInfluencer(dataDisplay,topic){
         var urls = '{{URL::to("/bridge/get/influencers?topic=")}}' + topic+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
         console.log(urls);
         $.get(urls ).done(function( response ) {
            response = jQuery.parseJSON(response);
            influencerData = response;
            var pieData = [];
            for (var i = 0; i< response.length; i++){
               pieData.push([influencerData[i].display_name, influencerData[i].count]);
            }
            if (dataDisplay == 1)
               setData_1('influencers', pieData);
            else if (dataDisplay == 2)
               setData_2('influencers', pieData);
         });
      }

      function getTopPerson(dataDisplay, topic){
         var urls = '{{URL::to("/bridge/get/top_persons")}}?topic='+topic+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
         console.log(urls);
         $.get(urls).done(function(response){
            response = jQuery.parseJSON(response);
            if (dataDisplay == 1)
               setData_1('top_persons', response);
            else if (dataDisplay == 2)
               setData_2('top_persons', response);
         });
      }

      function getDemography(dataDisplay, topic){
         var urls = "{{URL::to('/bridge/get/demography?topic=')}}"+topic+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
         console.log(urls);
         $.get(urls).done(function(response){
             geoLocation = jQuery.parseJSON(response);
             if (dataDisplay == 1)
               setData_1('demography', geoLocation);
            else if (dataDisplay == 2)
               setData_2('demography', geoLocation);
          });
      }

      function getSentiment(dataDisplay, topic){
         var urls = '{{URL::to("/bridge/get/sentiment?topic=")}}'+topic+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
         console.log(urls);
         $.get(urls).done(function( response ) {
            response = jQuery.parseJSON(response);
            if (dataDisplay == 1)
               setData_1('sentiment', response);
            else if (dataDisplay == 2)
               setData_2('sentiment', response);
         });
      }

      function getKeywordMapping(dataDisplay, topic){
         $.get("{{URL::to('bridge/get/related_words?topic=')}}"+topic).done(function(response){
            response = jQuery.parseJSON(response);
            var words = [];
            for (var i = 0; i<response.length; i++){
               words.push({text: response[i].word, weight: response[i].score})+'&n=5&date_start='+dateStart+'&date_stop='+dateStop;
            }
            if (dataDisplay == 1)
               setData_1('mapping', words);
            else if (dataDisplay == 2)
               setData_2('mapping', words);
         });
      }
      function showData(data, dataDisplay){
         for (var i = 0; i < data.length; i++){
            switch(data[i].type){
               case 'influencers':
                  showInfluencers(data[i],dataDisplay);
                  break;
               case 'top_persons':
                  showTopPerson(data[i],dataDisplay);
                  break;
               case 'demography':
                  showDemography(data[i],dataDisplay);
                  break;
               case 'sentiment':
                  showSentiment(data[i],dataDisplay);
                  break;
               case 'mapping':
                  showKeywordMapping(data[i],dataDisplay);
                  break;
               default: 
                  break;
            }
         }
      }

      function showInfluencers(pieData, dataDisplay){
         google.charts.load('current', {'packages':['corechart']});
         google.charts.setOnLoadCallback(drawChart);
         function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Source');
            data.addColumn('number', 'Post Count');
            data.addRows(pieData.data);

            var options = {
               title: 'Influencer for topic '+dataDisplay,
               legend: {position: 'bottom'}
            };

            var chart = new google.visualization.PieChart(document.getElementById('influencer_'+dataDisplay));
            chart.draw(data, options);
         }
      }
      function showTopPerson(response, dataDisplay){
         response = response.data;
         var persons = [];
         google.charts.load('current', {'packages':['line']});
         google.charts.setOnLoadCallback(drawChart);
         var selectedItem = [];
         function personValidation(name){
            var isExist = 0;
            for (var i = 0; i < persons.length; i++){
               if (persons[i].screen_name == name.screen_name)
               isExist++;
            }
            if (isExist == 0)
               persons.push(name);
         }
         function drawChart() {
            for (var i = 0; i< response.length; i++){
               for (var j = 0; j < response[i].persons.length; j++){
                  personValidation(response[i].persons[j]);
               }
            }
            var data = new google.visualization.DataTable();

            data.addColumn('string', 'Persons');
            for (var i = 0; i< persons.length; i++){
               data.addColumn('number', persons[i].display_name);
            }
            var contents = [];

            for (var i = 0; i < response.length; i++){
               var content = [];
               content.push(response[i].date);
               for (var j = 0; j < persons.length; j++){
                  var isExist = -1;
                  for (var k = 0; k < response[i].persons.length; k++){
                     if (response[i].persons[k].screen_name == persons[j].screen_name)
                        isExist = k;
                  }
                  if (isExist == -1)
                     content.push(0);
                  else 
                     content.push(response[i].persons[isExist].post_count);
               }
               contents.push(content);
            }
            data.addRows(contents);
            var options = {
               height: 500,
               axes: {
                  x: {
                  0: {side: 'bottom'}
                  }
               }
            };

            var chart = new google.visualization.LineChart(document.getElementById('toppersons_'+dataDisplay));
            function selectHandler() {
               selectedItem = chart.getSelection()[0];
               if (selectedItem) {
               }
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);    
            chart.draw(data, options);
         }
      }
      function showDemography(response, dataDisplay){
         var icons = ['http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|09e','http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|e66'];
         if (map != null){
            for (var i = 0; i < response.data.length; i++){
               new google.maps.Marker({
                  position: new google.maps.LatLng(response.data[i].coordinates[1], response.data[i].coordinates[0]),
                  icon: icons[dataDisplay-1],
                  map: map
               });
            }
         }
      }
      function showSentiment(response, dataDisplay){
         var pieData = [];
         var positive = 0;
         var negative = 0;
         var neutral = 0;
         for(var i = 0; i < response.data.length; i++){
            positive += response.data[i].sentiment.positive;
            negative += response.data[i].sentiment.negative;
            neutral += response.data[i].sentiment.neutral;
         }
         pieData.push(['positive', positive]);
         pieData.push(['negative', negative]);
         pieData.push(['neutral', neutral]);
         console.log(pieData);
         google.charts.load('current', {'packages':['corechart']});
         google.charts.setOnLoadCallback(drawChart);

         function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Sentiment');
            data.addColumn('number', 'Post Count');
            data.addRows(pieData);

            var chart = new google.visualization.PieChart(document.getElementById('sentiment_'+dataDisplay));
            function selectHandler() {
               var selectedItem = chart.getSelection()[0];
            }
            google.visualization.events.addListener(chart, 'select', selectHandler);
            chart.draw(data, {colors: ['#0c0', '#c00', '#ccc'], legend: 'bottom'});
         }
      }
         var words = [];
      function showKeywordMapping(response, dataDisplay){
         response = response.data;
         $('#mapping_'+dataDisplay).jQCloud(response);
      }
      function addMarker(feature) {
         var marker = new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.type].icon,
            map: map
         });
      }
   }
  function initMap(){
  	console.log('hola');
    map = new google.maps.Map(document.getElementById('demograpy_graph'), {
      center: {lat: -1.8785136, lng: 113.6952623},
      zoom: 5
    });
  }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsXNCvwTsLM00_NuUvBFK7Meg6sqNwP3I&callback=initMap" async defer></script>
  @endsection