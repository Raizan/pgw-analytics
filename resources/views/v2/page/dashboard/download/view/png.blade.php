@extends('template.index')
@section('content')
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn blue">Save changes</button>
               <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal -->
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
  
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         @include('template.breadcrumb')
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   @include('template.top_stats')
   <div class="clearfix"></div>
   <div class="row ">
      <div class="col-md-8 col-sm-12">
         <div class="portlet box red">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Object Name</div>
            </div>
            <div class="portlet-body">
               <table class="table table-striped table-bordered table-hover" id="sample_1">
                  <thead>
                     <tr>
                        <th>No</th>
                        <th>Object Name</th>
                        <th>URL</th>
                        <th>Crawl</th>
                        <th>Action</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     $iter = 0;
                     ?>
                     @foreach($data['content'] as $print)
                     <tr class="odd gradeX">
                        <td>{{++$iter}}</td>
                        <td>{{$print->name}}</td>
                        <td>{{$print->url}}</td>
                        <td></td>
                        <td>
                           <a href="{{URL::to('dashboard/object/all/delete/'.$print->id)}}" class="btn">Delete</a>
                        </td>
                     </tr>
                     @endforeach
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-12">
         <div class="portlet box green">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>New Object</div>
            </div>
            <div class="portlet-body">
               {!! Form::open(array('route' => 'dashboard.object.all.store', 'method' => 'post','files' => false, 'class' => 'form-horizontal form-bordered form-row-stripped')) !!}
                  <div class="form-body">
                     <div class="form-group">
                        {!! Form::label('name','Name : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                           {!! Form::text('name',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: kompas media, detik, cnn indonesia']) !!}
                           <span class="help-block">Tulis nama object crawler</span>
                        </div>
                     </div>
                     <div class="form-group">
                        {!! Form::label('url','URL : ', ['class' => 'control-label col-md-3']) !!}
                        <div class="col-md-9">
                           {!! Form::text('url',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: detik.com, cnnindonesia.com']) !!}
                           <span class="help-block">Tulis url object</span>
                        </div>
                     </div>
                     <div class="form-actions fluid">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="col-md-12">
                                 {!! Form::submit('Tambahkan Object',['class' => 'btn green col-md-12']) !!}
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               {!! Form::close()!!}
            </div>
         </div>
      </div>
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   
   @endsection