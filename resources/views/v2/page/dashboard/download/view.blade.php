@extends('v2.template.index')
@section('main-content')
 
   <div class="clearfix"></div>
   <div class="row ">
      <?php
      $tableIter = 0;
      ?>
      @foreach($data['container'] as $container)
         <div class="col-md-3 col-sm-12">
            <div class="portlet box {{$container['color']}}">
               <div class="portlet-title">
                  <div class="caption"><i class="icon-bell"></i>{{$container['name']}}</div>
               </div>
               <div class="portlet-body">
                  <table class="table table-striped table-bordered table-hover" id="sample_{{++$tableIter}}">
                     <thead>
                        <tr>
                           <th>No</th>
                           <th>Keyword</th>
                           <th>Assigned</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        $iter = 0;
                        ?>
                        @foreach($data['content'] as $print)
                        <tr class="odd gradeX">
                           <td>{{++$iter}}</td>
                           <td><a href="{{URL::to('download/'.$container['link'].'/'.$print->id.'/'.$data['download'])}}">{{$print->keyword}}</a></td>
                           <td>{{$print->created_at}}</td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      @endforeach
   </div>
   @endsection
   @section('custom-header')
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   @endsection
   @section('custom-footer')
   <script type="text/javascript">
      
   <?php
   for($i = 1; $i <= count($data['content']); $i++){
      ?>
      $("#sample_{{$i}}").DataTable();
      <?php
      }
   ?>
   </script>
   @endsection
