<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<div class="row ">
      <div class="col-md-12 col-sm-12">
         <div class="portlet box purple">
            
            <div class="portlet-body" style="overflow: scroll;">
               <table class="table table-striped table-bordered table-hover" id="portal_log" table_view="post_view">
                  <thead>
                     <tr>
                         <th>No</th>
                         <th>Title</th>
                         <th>Timestamp</th>
                         <th>URL</th>
                         <th>Source</th>
                     </tr>
                 </thead>
               </table>
            </div>
         </div>
      </div>
       
   </div>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
     
   <script type="text/javascript" src="{{asset('assets/assets/scripts/tunnel.js')}}"></script>
   <script type="text/javascript">
   var table_sample2 = $("#sample_2").DataTable();
   var tableView = $("table[table_view='post_view'] > tbody");
   var postType = ['tweet', 'fb', 'berita'];
   var topicLog = [];
   var dataDisplay = [];
   var dataCounter = 0;
   @foreach($data['content'] as $print)
   topicLog.push("{{str_replace(' ','%20',$print->keyword)}}");
   @endforeach
  var tunnel = new Tunnel($("#facebook_log"), $("#tweeter_log"), $("#portal_log"));
  tunnel.beritas = new Berita($("#portal_log"));
  tunnel.facebooks = new Facebook($("#facebook_log"));
  tunnel.twitters = new Twitter($("#twitter_log"));
  var totalIter = 0;
  var showIter = 0;
   for (var i = topicLog.length-1; i >= 0; i--){
    for (var j = 0; j < postType.length; j++){
      getDatas(topicLog[i], postType[j]);
      totalIter++;
    }
   }
   function getDatas(topic, type){
    var getURL = "{{URL::to('/bridge/get/recent_post')}}"+'?topic='+topic.replace(" ", "%20")+'&type='+type;
      console.log(getURL);
    $.get(getURL).done(function(response) {
      response = jQuery.parseJSON(response);
      for (var i = 0; i < response.length; i++){
            response[i].topic = topic;
            response[i].topic_number = topicLog.indexOf(topic);
        tunnel.factory(response[i]);
      }
      show();
      });
    }
    function show(){
      showIter++;
      if (showIter >= totalIter)
        tunnel.show();
    }
   $('input[name="daterange"]').daterangepicker();
   </script>