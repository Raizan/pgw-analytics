
<div class="row-fluid">
	{!! Form::open(array('route' => 'v2.topic.store', 'method' => 'post','files' => false, 'class' => 'form-horizontal no-margin')) !!}
		<div class="control-group">
			{!! Form::label('keyword','Name : ', ['class' => 'control-label']) !!}
			<div class="controls">
            {!! Form::text('keyword',null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'ex: John Doe, Peter Schumacker']) !!}
			</div>
		</div>
      <div class="control-groupe">
         {!! Form::label('filter','Object : ', ['class' => 'control-label col-md-3']) !!}
         <div class="controls">
            @foreach($objects as $obj)
               <label class="span12">
                  {!! Form::checkbox('filter['.$obj['id'].']', $obj['name']) !!}
                  {{ ucfirst($obj->name) }}
               </label>
            @endforeach
         </div>
      </div>
      <div class="">
         <div class="row">
            <div class="span12">
            	<center>
            		{!! Form::submit('Add Topic',['class' => 'btn btn-success']) !!}
            	</center>
            </div>
         </div>
      </div>
	{!! Form::close() !!}
</div>