<header>
  <a href="{{ URL::to('/v2/index') }}" class="logo"><img src="{{asset('v2/img/logo-kemensos-png.png')}}" width="45px">KSM Analytic</a>
  <div id="mini-nav">
    <ul class="hidden-phone">
    	<li><a class="standardModalButton"  href="#aboutksm" role="button" data-toggle="modal">About KSM Analytic</a></li>
    	<li><a class="standardModalButton" href="#standardModal" role="button" data-toggle="modal" url="{{URL::to('/v2/profile')}}">My Profile</a></li>
    	<li><a class="standardModalButton"  href="#standardModal" role="button" data-toggle="modal" url="{{URL::to('/v2/loguser')}}">Log Users</a></li>
     <!--   <li><a href="{{URL::to('v2/download/csv')}}">Download</a></li> -->
       <li><a href="{{URL::to('/logout')}}">Logout</a></li>
    </ul>
  </div>
</header>