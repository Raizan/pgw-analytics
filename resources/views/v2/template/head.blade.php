<meta charset="utf-8">
<title>KSM Analytic - Kemensos RI</title>
 <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
<meta name="description" content="KSM Analytic - Kemensos RI">
<meta name="keywords" content="kemsos RI">
<script src="{{asset('v2/js/html5-trunk.js')}}"></script>
 <link href="{{asset('v2/icomoon/style.css')}}" rel="stylesheet">
  <!--[if lte IE 7]>
<script src="css/icomoon-font/lte-ie7.js"></script>
<![endif]

<!-- bootstrap css -->
<link href="{{asset('v2/css/main.css')}}" rel="stylesheet">
<link href="{{asset('v2/css/fullcalendar.css')}}" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<!-- Alertify JS -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/alertifyjs/css/alertify.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/alertifyjs/css/themes/default.min.css')}}">

<style type="text/css">
ul{
   list-style-type: none;
   padding-left: 0px;
}
#facebookPostTab{
	margin: -10px;
    background-color: #efefef;
}
#facebookPostContent > li{
   border-radius: 2px !important;
   padding: 10px;
   color: black;
   margin: 10px;
   margin-bottom: 10px;
   background-color: white;
}
#facebookPostContent > li > .facebook-person > a{
   font-weight: bold;
   font-size: 18px;
   color: #3b5998;
}
#facebookPostContent > li > .facebook-person > a{
   text-decoration: none
}
#facebookPostContent > li > .facebook-person > a:hover{
   text-decoration: none
}
#facebookPostContent > li > .facebook-person > a:visited{
   text-decoration: none
}
#facebookPostContent > li > .facebook-person > a:active{
   text-decoration: none
}
#facebookPostContent > li > .facebook-content{
   padding-bottom: 5px;
   border-bottom: solid 0.5px #ccc;
   color: black;
}
#facebookPostContent > li > .facebook-time{
   color: #999;
}
</style>