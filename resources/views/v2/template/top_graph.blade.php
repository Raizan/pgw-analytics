<div class="row-fluid">
  <div class="span12">
     
    <div class="row-fluid">
      <div class="span9">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon=""></span> Server Stats | Local IP:  <?php
$serverIP = $_SERVER["SERVER_ADDR"];
 echo "<b>{$serverIP}</b>";  
                      ?>
                  </div>
                </div>
                <div class="widget-body">
<div class="widget-body">
                      <div id="selectionCharts" style="height:200px;"></div>
                    </div>                </div>
              </div>
            </div>
      <div class="span3">
        <div id="scrollbar-three">
          <div class="scrollbar">
            <div class="track">
              <div class="thumb">
                <div class="end"></div>
              </div>
            </div>
          </div>
          <div class="viewport">
            <div class="overview">
              @foreach($data['objects'] as $o)
              <a href="//{{$o->url}}" target="_blank">
                <div class="stats-count">
                  <span class="fs1 arrow text-success hidden-tablet" aria-hidden="true"><img src="{{asset('userFile/'.$o->logo)}}" width="35px"></span> 
                    <h5 class="stat-value text-info">{{$o->name}}</h5>
                    <span class="stat-name">{{$o->url}}</span>
                </div>
              </a>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>