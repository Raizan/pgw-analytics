<div id="mainnav" class="hidden-phone hidden-tablet">
  <ul>
    <li class="@if($selectedTab == 'dashboard') active @endif">
      <span class="@if($selectedTab == 'dashboard') current-arrow @endif"></span>
      <a href="{{URL::to('v2/index')}}">
        <div class="icon">
          <span class="fs1 fa fa-tachometer fs1 fa-4x" title="Dashboard Utama KSM Analytic" aria-hidden="true"  ></span>
        </div>
        Dashboard
      </a>
    </li>
     <li class="@if($selectedTab == 'expose') active @endif">
      <span class="@if($selectedTab == 'expose') current-arrow @endif"></span>
      <a href="{{URL::to('v2/expose')}}">
        <div class="icon">
          <span class="fs1 fa fa-bar-chart-o fs1 fa-4x" title="All topic Expose" aria-hidden="true"  ></span>
        </div>
        Expose
      </a>
    </li>
    <li class="@if($selectedTab == 'statistic') active @endif">
      <span class="@if($selectedTab == 'statistic') current-arrow @endif"></span>
      <a href="{{URL::to('v2/statistic')}}">
        <div class="icon">
          <span class="fs1 fa fa-area-chart fs1 fa-4x" title="Compare Topic & Peak Time" aria-hidden="true"  ></span>
        </div>
        Statistic
      </a>
    </li>
    <li class="@if($selectedTab == 'stream') active @endif">
      <span class="@if($selectedTab == 'stream') current-arrow @endif"></span>
      <a href="{{URL::to('v2/stream')}}">
        <div class="icon">
          <span class="fs1 fa fa-server fs1 fa-4x" aria-hidden="true" title="Crawl Stream KSM Analytic"  ></span>
        </div>
        Stream
      </a>
    </li>
    <li class="@if($selectedTab == 'configuration') active @endif">
      <span class="@if($selectedTab == 'configuration') current-arrow @endif"></span>
      <a href="{{URL::to('v2/configuration')}}">
        <div class="icon">
          <span class="fs1 fa fa-cogs fs1 fa-4x" aria-hidden="true" title="Konfigurasi User & Object" ></span>
        </div>
        Configuration
      </a>
    </li>
    <li class="@if($selectedTab == 'help') active @endif">
      <span class="@if($selectedTab == 'help') current-arrow @endif"></span>
      <a href="{{URL::to('v2/help')}}">
        <div class="icon">
          <span class="fs1 fa fa-info-circle fs1 fa-4x" aria-hidden="true" title="Panduan Penggunaan" ></span>
        </div>
        Help 
      </a>
    </li>
     
    
  </ul>
</div>