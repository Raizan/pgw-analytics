<!DOCTYPE html>
  
    <html lang="en">
  <head>
    @include('v2.template.head')
    @yield('custom-head')
  </head>
  <body>
    
    @include('v2.template.header')

    <div class="container-fluid">
      @include('v2.template.sidebar')
      
      <div class="dashboard-wrapper" style="min-height: 400px">
        <div class="main-container">
          @yield('main-content')
        </div>
      </div>
    </div>
    @include('v2.template.footer')
    @include('v2.template.messaging')
    @yield('custom-footer')
  </body>
</html>