<div id="advancedModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="advancedModalLabel" aria-hidden="true" style="width: 90%; margin-left: -45%">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      ×
    </button>
    <h4 id="advancedModalLabel">
      Posts gatered
    </h4>
  </div>
  <div class="modal-body" id="advancedModalBody">
    <div class="row-fluid" id="advancedModalContent">
      <ul class="nav nav-tabs no-margin myTabBeauty">
        <li class="active">
          <a data-toggle="tab" href="#twitterPostTab">
            Twitter
          </a>
        </li>
        <li class="">
          <a data-toggle="tab" href="#portalPostTab">
            Portal
          </a>
        </li>
        <li class="">
          <a data-toggle="tab" href="#facebookPostTab">
            Facebook
          </a>
        </li>
      </ul>
      <div class="tab-content" id="compareContent">
        <div id="twitterPostTab" class="tab-pane fade active in">
            <div id="twitterPostLoader" style="width:100%">
              <center>
                <img src="{{asset('v2/img/loading-red.gif')}}">
              </center>
            </div>
            <div class="row-fluid" id="twitterPostContent">
            </div>
        </div>
        <div id="portalPostTab" class="tab-pane fade">
            <div id="portalPostLoader" style="width:100%;">
              <center>
                <img src="{{asset('v2/img/loading-red.gif')}}">
              </center>
            </div>
            <ul class="row-fluid" id="portalPostContent">
              
            </ul>
        </div>
        <div id="facebookPostTab" class="tab-pane fade">
            <ul class="row-fluid" id="facebookPostContent">
              
            </ul>
            <div id="facebookPostLoader" style="width:100%;">
              <center>
                <img src="{{asset('v2/img/loading-red.gif')}}">
              </center>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="standardModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="standardModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      ×
    </button>
    <h4 id="standardModalLabel">
      <center>
        <img src="{{asset('v2/img/loading-red.gif')}}">
      </center>
    </h4>
  </div>
  <div class="modal-body" id="standardModalBody">
    <center>
      <img src="{{asset('v2/img/loading-red.gif')}}">
    </center>
  </div>
  <div class="modal-footer" id="standardModalFooter">
    <button class="btn" data-dismiss="modal" aria-hidden="true">
      Cancel
    </button>
    <a class="btn btn-primary" id="standardModalContinue">
      close
    </a>
  </div>
</div>

<div id="aboutksm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="standardModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      ×
    </button>
    <h4 id="standardModalLabel">
      About KSM Analityc | V.1.0
    </h4>
  </div>
  <div class="modal-body" id="standardModalBody">
    <center>
       <img src="{{asset('v2/img/logo-kemensos-png.png')}}"><br><br><br>
      <p>“KMS Analytics” merupakan sebuah aplikasi web yang digunakan untuk menganalisis topik yang ada pada media sosial dan portal berita. 
      Aplikasi ini mampu melakukan analisis top person, influencer, sentimen, dan demografi dari sebuah topik. 
      Manajemen User yang di ijinkan untuk mengakses aplikasi ini, diatur oleh Admin KSM Analytic.</p>
    </center>
  </div>
  <div class="modal-footer" id="standardModalFooter">
    
    <a class="btn btn-primary" id="standardModalContinue" data-dismiss="modal">
      Close
    </a>
  </div>
</div>

<div id="streamlogfront" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="standardModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
      ×
    </button>
    <h4 id="standardModalLabel">
      <center>
        <img src="{{asset('v2/img/loading-red.gif')}}">
      </center>
    </h4>
  </div>
  <div class="modal-body" id="standardModalBody">
   <div class="col-md-8 col-sm-12">
         <div class="portlet box purple">
            <div class="portlet-title">
               <div class="caption"><i class="icon-calendar"></i>Portal Log</div>
            </div>
            <div class="portlet-body" style="overflow: scroll;">
               <table class="table table-striped table-bordered table-hover" id="portal_log" table_view="post_view">
                  <thead>
                     <tr>
                         <th>No</th>
                         <th>Title</th>
                         <th>Timestamp</th>
                         <th>URL</th>
                         <th>Source</th>
                     </tr>
                 </thead>
               </table>
            </div>
         </div>
      </div>
  </div>
  <div class="modal-footer" id="standardModalFooter">
    <button class="btn" data-dismiss="modal" aria-hidden="true">
      Close
    </button>
     
  </div>
</div>