<script src="{{asset('v2/js/jquery.min.js')}}"></script>
<script src="{{asset('v2/js/bootstrap.js')}}"></script>
<script src="{{asset('v2/js/jquery-ui-1.8.23.custom.min.js')}}"></script>

<!-- morris charts -->
<script src="{{asset('v2/js/morris/morris.js')}}"></script>
<script src="{{asset('v2/js/morris/raphael-min.js')}}"></script>

<!-- Flot charts -->
<script src="{{asset('v2/js/flot/jquery.flot.js')}}"></script>
<script src="{{asset('v2/js/flot/jquery.flot.resize.min.js')}}"></script>

<!-- Calendar Js -->
<script src="{{asset('v2/js/fullcalendar.js')}}"></script>

<!-- Tiny Scrollbar JS -->
<script src="{{asset('v2/js/tiny-scrollbar.js')}}"></script>

<!-- custom Js -->
<!-- <script src="{{asset('v2/js/custom-index.js')}}"></script>
<script src="{{asset('v2/js/custom.js')}}"></script> -->

<script src="{{asset('v2/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('assets/alertifyjs/alertify.min.js')}}"></script>

<script type="text/javascript">
	$('.standardModalButton').click(function(){
		if ($(this).attr('action') == 'confirmation'){
			var modalName = $(this).attr('href');
			var modalURL = $(this).attr('url');
			$(modalName+'Label').text('Delete Confirmation');
			$(modalName+'Body').html('Are you sure want to delete? This action cant be undo');
			$(modalName+'Footer').show();
		 	$(modalName+'Continue').attr('href',$(this).attr('url'));
		 	$(modalName+'Continue').text('Delete');
		} else {
			var modalName = $(this).attr('href');
			var modalURL = $(this).attr('url');
			console.log('url',modalURL);
			$.get(modalURL).done(function(response){
				console.log('response',response);
			 	$(modalName+'Label').text(response.label);
			 	$(modalName+'Body').html(response.body);
			 	if (response.button != 'disabled'){
			 		$(modalName+'Footer').show();
				 	$(modalName+'Continue').attr('href',response.continue.href);
				 	$(modalName+'Continue').text(response.continue.text);
			 	} else {
			 		$(modalName+'Footer').hide();
			 	}
			});
		}
	});
	@if (Session::has('toastr'))
		<?php
		$notification = Session::get('toastr');
		Session::forget('toastr');
		?>
		@if ($notification['type'] == 'success')
			alertify.notify("{{$notification['message']}}", "{{$notification['type']}}", 5);
		@else 
			alertify.error("{{$notification['message']}}");
		@endif
		
	@endif
</script>
