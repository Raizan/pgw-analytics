<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopInfluencersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('top_influencers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('queue_id');
            $table->string('screen_name');
            $table->string('display_name');
            $table->integer('post_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('top_influencers');
    }
}
