function getData(dataDisplay, topic){
   getInfluencer(dataDisplay,topic)
   getTopPerson(dataDisplay, topic);
   getDemography(dataDisplay, topic);
   getSentiment(dataDisplay, topic);
   getKeywordMapping(dataDisplay, topic);
}
function setData_1(type, data){
   dataDisplay_1.push({type: type, data: data});
   if (dataDisplay_1.length >= 5)
      showData(dataDisplay_1, 1);
}
function setRawData_1(type, raw){
  for (var i = 0; i<dataDisplay_1.length; i++){
    if (dataDisplay_1[i].type == type)
      dataDisplay_1[i].raw = raw;
  }
}
function setData_2(type, data){
   dataDisplay_2.push({type: type, data: data});
   if (dataDisplay_2.length >= 5)
      showData(dataDisplay_2, 2);
}

function getInfluencer(dataDisplay,topic){
   var urls = '{{URL::to("/bridge/get/influencers?topic=")}}' + topic+'&date_start='+dateStart+'&date_stop='+dateStop;
   $.get(urls ).done(function( response ) {
      response = jQuery.parseJSON(response);
      influencerData = response;
      var pieData = [];
      for (var i = 0; i< response.length; i++){
         pieData.push([influencerData[i].display_name, influencerData[i].count]);
      }
   });
}

function getTopPerson(dataDisplay, topic){
   var urls = '{{URL::to("/bridge/get/top_persons")}}?n=5&topic='+topic+'&date_start='+dateStart+'&date_stop='+dateStop;
   console.log('TopPerson',urls);
   $.get(urls).done(function(response){
      response = jQuery.parseJSON(response);
      if (dataDisplay == 1)
         setData_1('top_persons', response);
      else if (dataDisplay == 2)
         setData_2('top_persons', response);
   });
}

function getDemography(dataDisplay, topic){
   var urls = "{{URL::to('/bridge/get/demography?topic=')}}"+topic+'&date_start='+dateStart+'&date_stop='+dateStop;
   console.log('demography',urls);
   $.get(urls).done(function(response){
       geoLocation = jQuery.parseJSON(response);
       if (dataDisplay == 1)
         setData_1('demography', geoLocation);
      else if (dataDisplay == 2)
         setData_2('demography', geoLocation);
    });
}

function getSentiment(dataDisplay, topic){
   var urls = '{{URL::to("/bridge/get/sentiment?topic=")}}'+topic+'&date_start='+dateStart+'&date_stop='+dateStop;
   console.log('sentiment',urls);
   $.get(urls).done(function( response ) {
      response = jQuery.parseJSON(response);
      if (dataDisplay == 1)
         setData_1('sentiment', response);
      else if (dataDisplay == 2)
         setData_2('sentiment', response);
   });
}

function getKeywordMapping(dataDisplay, topic){
   $.get("{{URL::to('bridge/get/related_words?topic=')}}"+topic).done(function(response){
      response = jQuery.parseJSON(response);
      var words = [];
      var barData = [];
      for (var i = 0; i<response.length; i++){
         words.push({text: response[i].word, weight: response[i].score})+'&date_start='+dateStart+'&date_stop='+dateStop;
         barData.push([response[i].word, response[i].score]);
      }
      if (dataDisplay == 1)
         setData_1('mapping', barData);
      else if (dataDisplay == 2)
         setData_2('mapping', barData);
   });
}