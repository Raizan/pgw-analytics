PostsShowGet = function(){
  function validateName(name){
    var names = [];
    names = name.split(' ');
    if (names.length < 2)
      return name;
    var name = '';
    for (var i = 0; i < names.length; i++){
      name += names[i];
      if (i + 1 < names.length)
        name += '%20';
    }
    return name;
  }
  function requestURLmapper(request){
    var urls = [];
    var nFlag = false;
    for (var k in request){
      switch(k){
        case 'topic':
          if (request[k] == 'global')
            urls.push('topic='+topic);
          else if (typeof request[k] != 'undefined')
            urls.push('topic='+request[k]);
          break;
        case 'startDate':
          if (request[k] == 'global')
            urls.push('date_start='+dateStart);
          else if (typeof request[k] != 'undefined')
            urls.push('date_start='+request[k]);
          break;
        case 'endDate' : 
          if (request[k] == 'global')
            urls.push('date_stop='+dateStop);
          else if (typeof request[k] != 'undefined')
            urls.push('date_stop='+request[k]);
          break;
        case 'date':
        urls.push('date='+request[k]);
        break
        case 'person' : 
          if(typeof request[k] != 'undefined')
            urls.push('person='+validateName(request[k]));
          break;
        case 'name' : 
          if(typeof request[k] != 'undefined')
            urls.push('name='+validateName(request[k]));
          break;
        case 'n' :
          if (typeof request[k] != 'undefined'){
            nFlag = true;
            urls.push('n='+request[k]);
          }
          break;
        case 'sentiment':
          if (typeof request[k] != 'undefined'){
            sentimentNumber = 0;
            switch(request[k]){
              case 'positive':
                sentimentNumber = 1;
                break;
              case 'negative' :
                sentimentNumber = -1;
                break;
              default:
                sentimentNumber = 0;
                break;
            }
            urls.push('sentiment='+sentimentNumber);
          }
        default:
          break;
      }
    }
    if (!nFlag)
      urls.push('n=20');
    var url = '';
    for(var i = 0; i < urls.length; i++){
      if (i == 0)
        url += '?';
      url += urls[i];
      if (i+1 < urls.length)
        url += '&';
    }
    return url;
  }

  function showAdvancedModal(request){
    console.log('call');
    var url = '{{URL::to("bridge/get/posts")}}' +requestURLmapper(request);
    $('#advancedModal').modal('toggle');

    console.log('notification');
    var twitterNotification = $('#twitterPostLoader');
    twitterNotification.show();
    var facebookNotification = $('#facebookPostLoader');
    facebookNotification.show();
    var portalNotification = $('#portalPostLoader');
    portalNotification.show();

    tunnel.beritas = new Berita($("#portalPostContent"), objects, '{{URL::to("userFile")}}');
    tunnel.facebooks = new Facebook($("#facebookPostContent"));
    tunnel.twitters = new Twitter($("#twitterPostContent"));

    var postSource = ['tweet', 'fb', 'berita'];
    for (var i = 0; i< postSource.length; i++){
      var type = postSource[i];
      console.log(url+'&type='+type);
      $.get(url+'&type='+type).done(function(response){
        response = jQuery.parseJSON(response);
        dataDisplay = response;
        for(var j = 0; j<response.length; j++){
          tunnel.factory(response[j]);
        }
        $(tunnel.facebooks.container).children().remove();
        $(tunnel.twitters.container).children().remove();
        $(tunnel.beritas.container).children().remove();
        tunnel.clearView();
        tunnel.show();
        twitterNotification.hide();
        facebookNotification.hide();
        portalNotification.hide();
      });
    }
  }
}
