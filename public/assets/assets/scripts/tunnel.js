var Facebook = function(container){
	this.lists = [];
	this.container = container;
	console.log(this.container);
}
Facebook.prototype.setContainer = function(container){
	this.container = container;
}
Facebook.prototype.show = function() {
	var notification = $(this.container.selector+'-notification');
	var lists = this.lists;
	if (lists.length > 0){
		notification.children().first().text('');
		notification.children().last().text('');
		for (var i = 0; i< lists.length; i++){
			$(this.container).prepend('<li class="span4"><div class="facebook-person"><a href="'+lists[i].url+'" target="_blank">'+ lists[i].sumber +'</a></div><div class="facebook-time">'+lists[i].time+'</div><div class="facebook-content">'+ lists[i].content +'</div></li>');
		}
	} else {
		notification.children().first().text('No Data Available');
		notification.children().last().text('Data with this type is not Available for this topic because of the user is using different source. Look at the portal or twitter for the data');
	}
};
Facebook.prototype.add = function(object){
	this.lists.push(object);
}
Facebook.prototype.clearData = function(){
	this.lists = [];
}
Facebook.prototype.clearView = function(){
	var notification = $(this.container.selector+'-notification');
	notification.children().first().text('');
	notification.children().last().text('');
}

var Twitter = function(container){
	this.lists = [];
	this.container = container;
	console.log(this.container);
}
Twitter.prototype.setContainer = function(container){
	this.container = container;
}
Twitter.prototype.show = function() {
	container = this.container
	lists = this.lists;
	if (lists.length > 0){
		for (var i = 0; i < lists.length; i++){
			$.ajax({
		      method: "GET",
		      url: 'https://publish.twitter.com/oembed?url='+lists[i].url,
		      dataType: 'jsonp',
		      success: function(response){
		      	container.append('<div class="span4" style="margin-left: 0px; margin-right: 15px">'+response.html+'</div>');
		      }
		    });
		}
		container.children().addClass('span3');
	} else {
		$(container.selector+'-notification').children().first().text('No Data Available');
		$(container.selector+'-notification').children().last().text('Data with this type is not Available for this topic because of the user is using different source. Look at the portal or twitter for the data');
	}
};
Twitter.prototype.sort = function(data){
	var lists = data;
	for (var i = 0; i < lists.length-1; i++){
		var temp = [];
		for (var  j = i; j < lists.length; j++){
			// if (lists[i].time > lists[j].time)
			if (lists[i].topic_number < lists[j].topic_number)
				temp = lists[j];
		}
		lists[j] = lists[i];
		lists[i] = temp;
	}
	return lists;
}
Twitter.prototype.add = function(object){
	this.lists.push(object);
}
Twitter.prototype.clearData = function(){
	this.lists = [];
}
Twitter.prototype.clearView = function(){
	var notification = $(this.container.selector+'-notification');
	notification.children().first().text('');
	notification.children().last().text('');
	this.container.children().remove();
}

var Berita = function(container, objects, base_path){
	this.lists = [];
	this.initialize(container, objects, base_path);
}
Berita.prototype.initialize = function(container, objects, base_path){
	this.container = container;
	this.objects = objects;
	this.base_path = base_path;
	console.log(this.container);
}
function getLogo(objects, indexMatch, sumber, indexReturn){
	var newObject = objects;
	var logo = '';
	for (var i = 0; i< newObject.length; i++){
		var objectURL = newObject[i][indexMatch];
		var tempLogo = newObject[i][indexReturn];
		if (objectURL == sumber){
			logo = tempLogo;
		}
	}
	return logo;
}
Berita.prototype.show = function() {
	var lists = this.lists;
	var objects = this.objects;
	var base_path = this.base_path;
	if (lists.length > 0){
		for (var i = 0; i< lists.length; i++){
			var logo = getLogo(objects, 2, lists[i].sumber, 1);
			var wordMaxLength = 35;
			if (lists[i].title.length > wordMaxLength)
				$(this.container).append('<div class="span4" style="margin-left: 0px; margin-right: 15px"><div class="stats-count"><span class="fs1 arrow text-success hidden-tablet" aria-hidden="true"><img src="'+base_path+'/'+logo+'" width="35px"></span><a target="_blank" href="'+lists[i].url+'" > <h5 class="stat-value text-info">'+lists[i].title.substring(0, wordMaxLength)+'...</h5></a><span class="stat-name">'+lists[i].time+'  '+lists[i].sumber+' </span></div></div>');
			else 
				$(this.container).append('<div class="span4" style="margin-left: 0px; margin-right: 15px"><div class="stats-count"><span class="fs1 arrow text-success hidden-tablet" aria-hidden="true"><img src="'+base_path+'/'+logo+'" width="35px"></span><a target="_blank" href="'+lists[i].url+'" > <h5 class="stat-value text-info">'+lists[i].title+'</h5></a><span class="stat-name">'+lists[i].time+'  '+lists[i].sumber+' </span></div></div>');
		}
	}

	/*var dataTable = this.container;
	dataTable.clear();
	var lists = this.lists;
	if (lists.length > 0){
		for (var i = lists.length-1; i >= 0; i--){
			dataTable.row.add([i + 1, lists[i].title, lists[i].time, '<a href="'+lists[i].url+'" target="_blank">'+lists[i].url+'</a>' , lists[i].sumber]).draw(false);
		}
	} else {
		
	}*/
};
Berita.prototype.add = function(object){
	this.lists.push(object);
}
Berita.prototype.clearData = function(){
	this.lists = [];
}
Berita.prototype.clearView = function(dataTable){
}

var Tunnel = function(facebookContainer, twitterContainer, beritaContainer){
	console.log('new tunnel');
	this.newTunnel(facebookContainer, twitterContainer, beritaContainer);
}

Tunnel.prototype.newTunnel = function(facebookContainer, twitterContainer, beritaContainer){
	this.facebooks = new Facebook(facebookContainer);
	this.twitters = new Twitter(twitterContainer);
	this.beritas = new Berita(beritaContainer);
}

Tunnel.prototype.clearData = function() {
	var facebookContainer = this.facebooks.container;
	var twitterContainer = this.twitters.container;
	var beritaContainer = this.beritas.container;
	this.newTunnel();
}
Tunnel.prototype.clearView = function(){
	this.facebooks.clearView();
	this.twitters.clearView();
	this.beritas.clearView();
}
Tunnel.prototype.factory = function(data){
	// console.log(data);
	switch(data.type){
		case 'tweet':
			this.twitters.add(data);
			break;
		case 'fb':
			this.facebooks.add(data);
			break;
		case 'berita':
			this.beritas.add(data);
			break;
		default:
			console.log('The type is not in the list for ' + data.url);
			break;
	}
}
Tunnel.prototype.show = function(type){
	switch(type){
		case 'tweet':
			this.twitters.show();
			break;
		case 'fb':
			this.facebooks.show();
			break;
		case 'berita':
			this.beritas.show();
			break;
		default:
			console.log('The type is not in the list for ' + type);
			break;
	}
}
Tunnel.prototype.show = function() {
	this.facebooks.show();
	this.twitters.show();
	this.beritas.show();
};